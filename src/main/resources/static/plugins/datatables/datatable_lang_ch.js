/***
 Wrapper/Helper Class for datagrid based on jQuery Datatable Plugin
 ***/

var Datatable = function () {

    var tableOptions; // main options
    var dataTable; // datatable object
    var table; // actual table jquery object
    var tableContainer; // actual table container object
    var tableWrapper; // actual table wrapper jquery object
    var tableInitialized = false;
    var ajaxParams = {}; // set filter mode
    var the;
    //INFO TIMCROSSINFO 默认的请求成功数据回调
    var ajaxSuccess = function (result) {
        console.info("默认的success");
        console.info(result);
    };

    var countSelectedRecords = function () {
        var selected = $('tbody > tr > td:nth-child(1) input[type="checkbox"]:checked', table).size();
        var text = tableOptions.dataTable.language.metronicGroupActions;
        if (selected > 0 && text) {
            $('.table-group-actions > span', tableWrapper).text(text.replace("_TOTAL_", selected));
        } else {
            $('.table-group-actions > span', tableWrapper).text("");
        }
    };

    /*    let $tabFlush = $(".parent-tab-span").trigger('click');
        $tabFlush.click(function () {
            the.clearAjaxParams();
            the.setAjaxParam("action", tableOptions.filterApplyAction);
            var tParam;
            // get all typeable inputs
            $('textarea.form-filter, select.form-filter,input.form-filter[type="checkbox"]:checked,' +
                'input.form-filter[type="radio"]:checked, input.form-filter:not([type="radio"],[type="checkbox"])',
                tableContainer).each(
                function () {
                    tParam = $(this).val();
                    tParam = 'object' == typeof tParam && tParam != null ? tParam.toString() : tParam;
                    the.setAjaxParam($(this).attr("name"), tParam);
                });

            table.dataTable().fnClearTable();
            dataTable.ajax.reload();
        });*/


    var normalDataTale = {
        "pageLength": 10,
        "language": {
            // metronic spesific
            "metronicGroupActions": "_TOTAL_ records selected:  ",
            "metronicAjaxRequestGeneralError": "访问失败,请检查网络或者联系管理员.",

            "processing": "正在加载数据...",
            "lengthMenu": "每页显示 _MENU_条记录",
            "zeroRecords": "Nothing found - 没有记录",
            "info": "显示第  _START_ 条到第  _END_ 条记录,一共  _TOTAL_ 条记录",
            "infoEmpty": "显示0条记录",
            "infoFiltered": "(由 _MAX_ 项结果过滤)",
            "infoPostFix": "",
            "search": "搜索:",
            "emptyTable": "暂时没有数据...",
            "loadingRecords": "载入中...",
            "infoThousands": ",",
            "paginate": {
                "first": "首页",
                "previous": "上一页",
                "next": "下一页",
                "last": "尾页"
            },
            "aria": {
                "sortAscending": ": 以升序排列此列",
                "sortDescending": ": 以降序排列此列"
            },
        },
        "orderCellsTop": true,
        "columnDefs": [{ // define columns sorting options(by default all columns are sortable extept the first checkbox column)
        }],

        "deferRender": false,
        "bStateSave": true,
        "pagingType": "full_numbers", // pagination type(bootstrap, bootstrap_full_number or bootstrap_extended)
        "autoWidth": true, // disable fixed width and enable fluid table
        "processing": false, // enable/disable display message box on record load
        "serverSide": false, // enable/disable server side ajax loading

        "ajax": { // define ajax settings
            "url": "", // ajax URL
            "type": "POST", // request type
            "timeout": 200000,
            "data": function (data) { // add request parameters before submit
                $.each(ajaxParams, function (key, value) {
                    data[key] = value;
                });
                Metronic.blockUI({
                    message: tableOptions.loadingMessage,
                    target: tableContainer,
                    overlayColor: 'none',
                    cenrerY: true,
                    boxed: true
                });
            },
            "dataSrc": function (res) { // Manipulate the data returned from the server
                console.info(res);
                if (res.status != 200) {
                    Metronic.alert({
                        type: 'danger',
                        icon: 'warning',
                        message: res.message,
                        container: tableWrapper,
                        place: 'prepend'
                    });
                    Metronic.unblockUI(tableContainer);
                    return;
                }
                if (res.status == 200) {
                    if (tableOptions.resetGroupActionInputOnSuccess) {
                        $('.table-group-action-input', tableWrapper).val("");
                    }
                }
                if ($('.group-checkable', table).size() === 1) {
                    $('.group-checkable', table).attr("checked", false);
                    $.uniform.update($('.group-checkable', table));
                }
                if (tableOptions.onSuccess) {
                    tableOptions.onSuccess.call(undefined, the, res);
                }
                Metronic.unblockUI(tableContainer);
                ajaxSuccess(res.result);
                return res.result;
            },
            "error": function () {
                if (tableOptions.onError) {
                    tableOptions.onError.call(undefined, the);
                }
                Metronic.alert({
                    type: 'danger',
                    icon: 'warning',
                    message: tableOptions.dataTable.language.metronicAjaxRequestGeneralError,
                    container: tableWrapper,
                    place: 'prepend'
                });
                Metronic.unblockUI(tableContainer);
            }
        },

        "drawCallback": function () { // run some code on table redraw
            if (tableInitialized === false) { // check if table has been initialized
                tableInitialized = true; // set table initialized
                table.show(); // display table
            }
            Metronic.initUniform($('input[type="checkbox"]', table)); // reinitialize uniform checkboxes on each table reload
            countSelectedRecords(); // reset selected records indicator

            // callback for ajax data load
            if (tableOptions.onDataLoad) {
                tableOptions.onDataLoad.call(undefined, the);
            }

        },
    }

    var pageDataTable = {
        processing: true,
        serverSide: true,
        ordering: false,//是否允许用户排序
        filter: false,    //去掉搜索框
        paging: true,
        language: {
            "sProcessing": "处理中...",
            "sLengthMenu": "显示 _MENU_ 项结果",
            "sZeroRecords": "没有匹配结果",
            "sInfo": "显示第 _START_ 至 _END_ 项结果，共 _TOTAL_ 项",
            "sInfoEmpty": "显示第 0 至 0 项结果，共 0 项",
            "sInfoFiltered": "(由 _MAX_ 项结果过滤)",
            "sInfoPostFix": "",
            "sSearch": "搜索:",
            "sUrl": "",
            "sEmptyTable": "表中数据为空",
            "sLoadingRecords": "载入中...",
            "sInfoThousands": ",",

            "serverSide": true,
            "pageLength": 10,


            "oPaginate": {
                "sFirst": "首页",
                "sPrevious": "上页",
                "sNext": "下页",
                "sLast": "末页"
            },
            "oAria": {
                "sSortAscending": ": 以升序排列此列",
                "sSortDescending": ": 以降序排列此列"
            }
        },
        ajax: {
            "data": function (data) { // add request parameters before submit
                $.each(ajaxParams, function (key, value) {
                    data[key] = value;
                });
            }
        }
    };


    return {
        init: function (options) {
            this._init(options, normalDataTale);
        },

        initPage: function (options) {
            this._init(options, pageDataTable);
        },

        _init: function (options, _dataTable) {
            if (!$().dataTable) {
                return;
            }
            the = this;
            //INFO TIMCROSSINFO 加入数据请求成功回调
            if (options.ajaxSuccess!=undefined) {
                ajaxSuccess = function (result) {
                    options.ajaxSuccess(result);
                };
            }
            // default settings
            options = $.extend(true, {
                src: "", // actual table
                filterApplyAction: "filter",
                filterCancelAction: "filter_cancel",
                resetGroupActionInputOnSuccess: false,
                loadingMessage: '正在搜索请稍后...',
                retrieve: true,
                dataTable: _dataTable
            }, options);

            tableOptions = options;

            // create table's jquery object
            table = $(options.src);
            tableContainer = table.parents(".table-container");

            // apply the special class that used to restyle the default datatable
            var tmp = $.fn.dataTableExt.oStdClasses;

            $.fn.dataTableExt.oStdClasses.sWrapper = $.fn.dataTableExt.oStdClasses.sWrapper + " dataTables_extended_wrapper";
            $.fn.dataTableExt.oStdClasses.sFilterInput = "form-control input-small input-sm input-inline";
            $.fn.dataTableExt.oStdClasses.sLengthSelect = "form-control input-xsmall input-sm input-inline";

            // initialize a datatable
            dataTable = table.DataTable(options.dataTable);
            //table.TableName(table.id);

            // revert back to default
            $.fn.dataTableExt.oStdClasses.sWrapper = tmp.sWrapper;
            $.fn.dataTableExt.oStdClasses.sFilterInput = tmp.sFilterInput;
            $.fn.dataTableExt.oStdClasses.sLengthSelect = tmp.sLengthSelect;

            // get table wrapper
            tableWrapper = table.parents('.dataTables_wrapper');

            // build table group actions panel
            if ($('.table-actions-wrapper', tableContainer).size() === 1) {
                $('.table-group-actions', tableWrapper).html($('.table-actions-wrapper', tableContainer).html()); // place the panel inside the wrapper
                $('.table-actions-wrapper', tableContainer).remove(); // remove the template container
            }
            // handle group checkboxes check/uncheck
            $('.group-checkable', table).change(function () {
                var set = $('tbody > tr > td:nth-child(1) input[type="checkbox"]', table);
                var checked = $(this).is(":checked");
                $(set).each(function () {
                    $(this).attr("checked", checked);
                });
                $.uniform.update(set);
                countSelectedRecords();
            });

            // handle row's checkbox click
            table.on('change', 'tbody > tr > td:nth-child(1) input[type="checkbox"]', function () {
                countSelectedRecords();
            });

            // handle filter submit button click
            table.on('click', '.filter-submit', function (e) {
                e.preventDefault();
                the.submitFilter();
            });

            // handle filter cancel button click
            table.on('click', '.filter-cancel', function (e) {
                e.preventDefault();
                the.resetFilter();
            });
        },

        submitFilter: function () {
            the.clearAjaxParams();
            the.setAjaxParam("action", tableOptions.filterApplyAction);

            var tParam;
            // get all typeable inputs
            $('textarea.form-filter, select.form-filter,input.form-filter[type="checkbox"]:checked,' +
                'input.form-filter[type="radio"]:checked, input.form-filter:not([type="radio"],[type="checkbox"])',
                tableContainer).each(
                function () {
                    tParam = $(this).val();
                    tParam = 'object' == typeof tParam && tParam != null ? tParam.toString() : tParam;
                    the.setAjaxParam($(this).attr("name"), tParam);
                });

            table.dataTable().fnClearTable();
             dataTable.ajax.reload();
        },

        resetFilter: function () {
            $('textarea.form-filter, select.form-filter, input.form-filter', table).each(function () {
                $(this).val("");
            });
            $('input.form-filter[type="checkbox"]', table).each(function () {
                $(this).attr("checked", false);
            });
            the.clearAjaxParams();
            the.addAjaxParam("action", tableOptions.filterCancelAction);
            dataTable.ajax.reload();
        },

        getSelectedRowsCount: function () {
            return $('tbody > tr > td:nth-child(1) input[type="checkbox"]:checked', table).size();
        },

        getSelectedRows: function () {
            var rows = [];
            $('tbody > tr > td:nth-child(1) input[type="checkbox"]:checked', table).each(function () {
                rows.push($(this).val());
            });

            return rows;
        },

        setAjaxParam: function (name, value) {
            ajaxParams[name] = value;
        },

        addAjaxParam: function (name, value) {
            if (!ajaxParams[name]) {
                ajaxParams[name] = [];
            }

            skip = false;
            for (var i = 0; i < (ajaxParams[name]).length; i++) { // check for duplicates
                if (ajaxParams[name][i] === value) {
                    skip = true;
                }
            }

            if (skip === false) {
                ajaxParams[name].push(value);
            }
        },

        clearAjaxParams: function (name, value) {
            ajaxParams = {};
        },

        getDataTable: function () {
            return dataTable;
        },

        getTableWrapper: function () {
            return tableWrapper;
        },

        gettableContainer: function () {
            return tableContainer;
        },

        getTable: function () {
            return table;
        }

    };

};
