﻿/**
 * TIMCROSSINFO 手写板工具 在指定Div内绘制一个手写板
 * @returns {{init: init}}
 * @constructor
 */

var SignaturePad = function () {
    /**
     * 参考配置
     */
    var defaults = {
        width: 500,
        height: 200,
        border: '1px solid white',
        background: '#fff',
        lineColor: '#000',
        lineWidth: 2,
        likePen: true
    };

    var drawing = (function (callback) {
        return window.requestAnimationFrame ||
            window.webkitRequestAnimationFrame ||
            window.mozRequestAnimationFrame ||
            window.oRequestAnimationFrame ||
            window.msRequestAnimaitonFrame ||
            function (callback) {
                window.setTimeout(callback, 0);
            };
    })();

    function DrawPosition(event, rect) {
        var xPos, yPos;
        event = event.originalEvent;
        // Touch event
        if (event.type.indexOf('touch') !== -1) { // event.constructor === TouchEvent
            xPos = event.touches[0].clientX - rect.left;
            yPos = event.touches[0].clientY - rect.top;
        }
        // Mouse event
        else {
            xPos = event.clientX - rect.left;
            yPos = event.clientY - rect.top;
        }
        return {
            x: xPos,
            y: yPos
        };
    }

    function WritingSign(element, options,$finish) {
        this.element = element;
        this.$element = $(this.element);
        this.canvas = false;
        this.$canvas = false;
        this.ctx = false;
        this.drawing = false;
        this.currentPos = {x: 0, y: 0};
        this.lastPos = {x: 0, y: 0};
        this.options = options;

        this.$canvas = $('<canvas></canvas>').appendTo(this.$element);
        this.$canvas.attr({
            width: this.options.width,
            height: this.options.height
        });

        this.$canvas.css({
            boxSizing: 'border-box',
            width: this.options.width + 'px',
            height: this.options.height + 'px',
            border: this.options.border,
            background: this.options.background,
            cursor: 'crosshair'
        });

        // 调整笔锋效果
        if (this.options.likePen === true) {
            var width = this.$element.outerWidth();
            this.$canvas.attr('width', width);
            this.$canvas.css('width', width + 'px');
        }
        this.canvas = this.$canvas[0];
        this.ctx = this.canvas.getContext("2d");
        this.ctx.strokeStyle = this.options.lineColor;
        this.ctx.lineWidth = this.options.lineWidth;


        this.$canvas.on('mousedown touchstart', $.proxy(function (e) {
            this.drawing = true;
            this.lastPos = this.currentPos = new DrawPosition(e, this.canvas.getBoundingClientRect());
        }, this));
        this.$canvas.on('mousemove touchmove', $.proxy(function (e) {
            this.currentPos = new DrawPosition(e, this.canvas.getBoundingClientRect());
        }, this));
        this.$canvas.on('mouseup touchend mouseout', $.proxy(function (e) {
            this.drawing = false;
        }, this));

        $(document).on('touchstart touchmove touchend', $.proxy(function (e) {
            if (e.target === this.canvas) {
                e.preventDefault();
            }
        }, this));

        var that = this;
        (function drawLoop() {
            drawing(drawLoop);
            if (that.drawing) {
                that.ctx.moveTo(that.lastPos.x, that.lastPos.y);
                that.ctx.lineTo(that.currentPos.x, that.currentPos.y);
                that.ctx.stroke();
                that.lastPos = that.currentPos;
            }
        })();
        return this;
    }

    return {
        init: function ($padDiv, rules, $finishBtn) {
            var sign = new WritingSign($padDiv, rules,$finishBtn);
            $finishBtn.click(function () {
                sign.$canvas.unbind();
                $finishBtn.hide();
            });
            return sign;
        },
    };
}();
