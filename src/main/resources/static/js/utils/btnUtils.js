var BtnUtils = function () {

    return {

        createBtn: function (o) {
            var classString = o.style;
            var button = $("<button>");
            button.addClass(classString);
            button.html(o.name);
            o.title = o.name;
            button.click(function () {
                BtnUtils.formLoad(o);
            });
            return button;
        },
        createClickBtn: function (url, redirectUrl, name) {
            var button = $("<button>");
            button.addClass("btn btn-xs blue-madison");
            button.attr("style", "background-color:#990000;")
            button.html(name);
            button.click(function () {
                IntelCarUtils.ajax(url, "", function (result) {
                    if (result.status == "200") {
                        ToastrUtils.getSuccess("操作成功", result.message);
                    } else {
                        ToastrUtils.getError("操作失败", result.message);
                    }
                    IntelCarUtils.initAjax(redirectUrl);
                });
            });
            return button;
        },
        createFormViewBtn: function (o) {
            var classString = o.style;
            var button = $("<button>");
            button.addClass(classString);
            button.html(o.name);
            o.title = o.name;
            button.click(function () {
                BtnUtils.viewLoad(o);
            });
            return button;
        },
        createCopyDiDiVirturalNumberBtn: function (o) {
            var classString = o.style;
            var button = $("<button>");
            button.html(o.name);
            o.title = o.name;
            button.addClass(classString);
            button.click(function () {
                BtnUtils.copydidiLoad(o, button);
            });
            return button;
        },
        createViewBtn: function (data, url, name) {
            var button = $("<button>");
            button.addClass("btn btn-xs blue-madison");
            button.html(name);
            button.unbind().click(function () {
                IntelCarUtils.ajax(url, {id: data}, function (result) {
                    var contentBody = $(".page-content-body:visible").first();
                    contentBody.html(result);
                    Metronic.initAjax();
                }, null, "POST");
            });
            return button;
        },
        confirmBtnCreate: function (value, confirmUrl, name, redirectUrl, btnColor, successDelay, callbackFunction) {
            btnColor = btnColor ? btnColor : "red";
            var button = $("<button class='btn btn-xs'>");
            button.addClass(btnColor);
            button.html(name);
            button.unbind().click(function () {
                BtnUtils.dialogConfirm("你确定要" + name + "吗?", "提醒", function () {
                    IntelCarUtils.ajax(confirmUrl, {id: value}, function (result) {
                        if (result.status == 200) {
                            ToastrUtils.getSuccess("操作成功", result.message);
                            if (redirectUrl != null) {
                                setTimeout(function () {
                                    IntelCarUtils.initAjax(redirectUrl);
                                }, successDelay);
                            }
                            if (callbackFunction && callbackFunction != undefined && callbackFunction != null) {
                                callbackFunction();
                            }
                        } else if (result.status == 403) {
                            BtnUtils.dialogConfirm(result.message, "需要强制执行", function () {
                                IntelCarUtils.ajax(confirmUrl, {id: value, force: "force"}, function (result) {
                                    if (result.status == 200) {
                                        ToastrUtils.getSuccess("操作成功", result.message);
                                        if (redirectUrl != null) {
                                            setTimeout(function () {
                                                IntelCarUtils.initAjax(redirectUrl);
                                            }, successDelay);
                                        }
                                    } else {
                                        ToastrUtils.getSuccess("操作失败", result.message);
                                    }
                                });
                            });
                        } else {
                            ToastrUtils.getError("操作失败", result.message);
                        }
                    });
                });
            });
            return button;
        },

        confirmCreate: function (value, confirmUrl, name, redirectUrl) {
            BtnUtils.dialogConfirm("你确定要" + name + "吗?", "提醒", function () {
                IntelCarUtils.ajax(confirmUrl, {id: value}, function (result) {
                    if (result.status == 200) {
                        ToastrUtils.getSuccess("操作成功", result.message);
                        if (redirectUrl != null) {
                            IntelCarUtils.initAjax(redirectUrl);
                        }
                    } else if (result.status == 403) {
                        BtnUtils.dialogConfirm(result.message, "需要强制执行", function () {
                            IntelCarUtils.ajax(confirmUrl, {id: value, force: "force"}, function (result) {
                                if (result.status == 200) {
                                    ToastrUtils.getSuccess("操作成功", result.message);
                                    if (redirectUrl != null) {
                                        IntelCarUtils.initAjax(redirectUrl);
                                    }
                                } else {
                                    ToastrUtils.getSuccess("操作失败", result.message);
                                }
                            });
                        });
                    } else {
                        ToastrUtils.getSuccess("操作失败", result.message);
                    }
                });
            });
        },

        formBtnCreate: function (value, url, action, name, redirectUrl, callbackFunction) {
            return BtnUtils.createBtn({
                name: name,
                url: url,
                formaction: action,
                data: {id: value},
                aftersuccess: function () {
                    if (redirectUrl != null) {
                        IntelCarUtils.initAjax(redirectUrl);
                    }
                    if (callbackFunction && callbackFunction != undefined && callbackFunction != null) {
                        callbackFunction();
                    }
                },
                style: "btn btn-xs blue-madison"
            });
        },

        formViewBtnCreate: function (url, action, name) {
            return BtnUtils.createFormViewBtn({
                name: name,
                url: url,
                formaction: action,
                data: {},
                aftersuccess: function () {

                },
                style: "btn btn-xs blue-madison"
            });
        },
        formCopyDiDiVirturalNumberBtnCreate: function (url, action, name) {
            return BtnUtils.createCopyDiDiVirturalNumberBtn({
                name: name,
                url: url,
                formaction: action,
                data: {},
                aftersuccess: function () {
                },
                style: "btn btn-xs blue-madison incoming-number"
            });
        },
        formAddNew: function (url, action, name, redirectUrl, callbackFunction) {
            BtnUtils.formLoad({
                title: name,
                url: url,
                formaction: action,
                aftersuccess: function () {
                    if (redirectUrl != null) {
                        IntelCarUtils.initAjax(redirectUrl);
                    }
                    if (callbackFunction && callbackFunction != undefined && callbackFunction != null) {
                        callbackFunction();
                    }
                }
            });
        },

        formAddMultiRedirectUrl: function (url, action, name, redirectUrlArray, callbackFunction) {
            BtnUtils.formLoad({
                title: name,
                url: url,
                formaction: action,
                aftersuccess: function () {
                    if (redirectUrlArray != null) {
                        for (i = 0; i < redirectUrlArray.length; i++) {
                            IntelCarUtils.initAjax(redirectUrlArray[i]);
                        }

                    }
                    if (callbackFunction && callbackFunction != undefined && callbackFunction != null) {
                        callbackFunction();
                    }
                }
            });
        },

        formLoad: function (options) {
            var defaults = {
                title: "表单",
                url: options,
                data: null,
                success: null,
                error: null,
                aftersuccess: null,
                aftererror: null,
                autoclose: true,
                formaction: null,
                afterload: null,
                width: 760
            };
            options = $.extend(defaults, options);

            console.info(options);

            Metronic.startPageLoading();
            var formDialogId = "dialog_id_" + new Date().getTime();
            var formDialog = new dialog({
                id: formDialogId,
                width: options.width,
                title: options.title,
                onremove: function () {
                    BtnUtils.currentDialog = null;
                }
            });
            IntelCarUtils.ajax(options.url, options.data, function (res) {
                var formDom = $(res);
                if (options.formaction) {
                    formDom.attr("action", options.formaction);
                }
                if (options.afterload) {
                    options.afterload(formDom);
                }
                formDom.attr("data-dialogid", formDialogId);
                console.info(formDom);
                formDialog.content(formDom);
                formDialog.showModal();
                var formNode = $(formDialog.node);
                formNode.find("select").each(function () {
                    var defaultData = $(this).data("selected");
                    defaultData && $(this).val(defaultData).trigger("change");
                });

                formDialog.button([{
                    value: "关闭",
                    callback: function () {
                    }
                }, {
                    value: "提交",
                    callback: function () {
                        formDialog.statusbar("");
                        var form = formNode.find("form");
                        var $form = $(form);
                        if (!$form.valid()) exit;
                        if ($form.attr("fromIsSending") == "true") {
                            ToastrUtils.getSuccess("请等待", "上次请求执行中");
                            return false;
                            exit(0);
                        }
                        $form.attr("fromIsSending", "true");
                        BtnUtils.ajaxForm(form, function (result) {
                            $form.attr("fromIsSending", "false");
                            if (result.status == 200) {
                                if (options.success) {
                                    options.success(result, form, formDialog);
                                } else {
                                    formDialog.statusbar('<span style="color:green;">' + result.message + '</span>');
                                    BtnUtils.dialogClose(formDialog);
                                    ToastrUtils.getSuccess("操作成功", result.message);
                                }
                                options.aftersuccess && options.aftersuccess(result, form, formDialog);
                            } else if (result.status == 403) {
                                BtnUtils.dialogConfirm(result.message, "需要强制执行", function () {
                                    var data = $form.serialize() + "&force=force";
                                    IntelCarUtils.ajax($form.attr("action"), data, function (result) {
                                        $form.attr("fromIsSending", "false");
                                        if (result.status == 200) {
                                            if (options.success) {
                                                options.success(result, form, formDialog);
                                            } else {
                                                formDialog.statusbar('<span style="color:green;">' + result.message + '</span>');
                                                BtnUtils.dialogClose(formDialog);
                                                ToastrUtils.getSuccess("操作成功", result.message);
                                            }
                                            options.aftersuccess && options.aftersuccess(result, form, formDialog);
                                        } else {
                                            if (options.error) {
                                                options.error(result, form, formDialog);
                                            } else {
                                                if (result.data) {
                                                    for (var i in result.data) {
                                                        var errMsg = result.data[i];
                                                        var n = 0;
                                                        for (var name in errMsg) {
                                                            var input = form.find("[name='" + name + "']");
                                                            if (input.length > 0 && n++ == 0) {
                                                                input.focus();
                                                            } else {
                                                                formDialog.statusbar('<span class="vailderror">' + errMsg[name] + '</span>');
                                                            }
                                                            var vailderror = input.parent().find("span.vailderror");
                                                            if (vailderror.length == 0) {
                                                                vailderror = $("<span class='vailderror'>" + errMsg[name] + "</span>");
                                                                input.parent().append(vailderror);
                                                            } else {
                                                                vailderror.html(errMsg[name]);
                                                                vailderror.show();
                                                            }
                                                            input.change(function (e) {
                                                                $(this).parent().find('span.vailderror').hide();
                                                                $(this).unbind(e);
                                                            })
                                                        }
                                                    }
                                                } else {
                                                    formDialog.statusbar('<span style="color:red">' + result.message + '</span>');
                                                }
                                            }
                                            options.aftererror && options.aftererror(result, form, formDialog);
                                        }
                                    }, function (xhr, msg, e) {
                                        $form.attr("fromIsSending", "false");
                                        var errorText = xhr.status + " " + xhr.statusText + ":" + form.attr("action");
                                        console.log(errorText)
                                        var message = "操作失败,请稍后再试或联系管理员.";
                                        formDialog.statusbar('<span style="color:red">' + message + '</span>');
                                    });
                                });
                            } else {
                                if (options.error) {
                                    options.error(result, form, formDialog);
                                } else {
                                    if (result.data) {
                                        for (var i in result.data) {
                                            var errMsg = result.data[i];
                                            var n = 0;
                                            for (var name in errMsg) {
                                                var input = form.find("[name='" + name + "']");
                                                if (input.length > 0 && n++ == 0) {
                                                    input.focus();
                                                } else {
                                                    formDialog.statusbar('<span class="vailderror">' + errMsg[name] + '</span>');
                                                }
                                                var vailderror = input.parent().find("span.vailderror");
                                                if (vailderror.length == 0) {
                                                    vailderror = $("<span class='vailderror'>" + errMsg[name] + "</span>");
                                                    input.parent().append(vailderror);
                                                } else {
                                                    vailderror.html(errMsg[name]);
                                                    vailderror.show();
                                                }
                                                input.change(function (e) {
                                                    $(this).parent().find('span.vailderror').hide();
                                                    $(this).unbind(e);
                                                })
                                            }
                                        }
                                    } else {
                                        formDialog.statusbar('<span style="color:red">' + result.message + '</span>');
                                    }
                                }
                                options.aftererror && options.aftererror(result, form, formDialog);
                            }
                        }, function (xhr) {
                            $form.attr("fromIsSending", "false");
                            var errorText = xhr.status + " " + xhr.statusText + ":" + form.attr("action");
                            console.log(errorText)
                            var message = "操作失败,请稍后再试或联系管理员.";
                            formDialog.statusbar('<span style="color:red">' + message + '</span>');
                        });
                        return false;
                    },
                    autofocus: true
                }]);
                Metronic.initAjax();
            });
            return formDialog;
        },

        viewLoad: function (options) {
            var defaults = {
                title: "展示表单",
                url: options,
                data: null,
                success: null,
                error: null,
                aftersuccess: null,
                aftererror: null,
                autoclose: true,
                formaction: null,
                afterload: null,
                width: 760
            };
            options = $.extend(defaults, options);

            console.info(options);

            Metronic.startPageLoading();
            var formDialogId = "dialog_id_" + new Date().getTime();
            var formDialog = new dialog({
                id: formDialogId,
                width: options.width,
                title: options.title,
                onremove: function () {
                    BtnUtils.currentDialog = null;
                }
            });
            IntelCarUtils.ajax(options.url, options.data, function (res) {
                var formDom = $(res);
                if (options.formaction) {
                    formDom.attr("action", options.formaction);
                }
                if (options.afterload) {
                    options.afterload(formDom);
                }
                formDom.attr("data-dialogid", formDialogId);
                console.info(formDom);
                formDialog.content(formDom);
                formDialog.showModal();
                var formNode = $(formDialog.node);
                formNode.find("select").each(function () {
                    var defaultData = $(this).data("selected");
                    defaultData && $(this).val(defaultData).trigger("change");
                });
                Metronic.initAjax();
            });
            return formDialog;
        },
        copydidiLoad: function (options, btn) {
            var defaults = {
                title: "展示表单",
                url: options,
                data: null,
                success: null,
                error: null,
                aftersuccess: null,
                aftererror: null,
                autoclose: true,
                formaction: null,
                afterload: null,
                width: 760
            };
            options = $.extend(defaults, options);

            console.info(options);

            //  Metronic.startPageLoading();
            var formDialogId = "dialog_id_" + new Date().getTime();
            var formDialog = new dialog({
                id: formDialogId,
                width: options.width,
                title: options.title,
                onremove: function () {
                    BtnUtils.currentDialog = null;
                }
            });
            IntelCarUtils.ajax(options.url, options.data, function (data) {
                if (data.status == 200) {
                    console.log('获取手机号' + data.result)
                    btn.attr("copyText", data.result.toString())
                    var clipboard = new ClipboardJS(".incoming-number", {
                        text: function (trigger) {
                            return trigger.getAttribute('copyText');
                        }
                    });
                    console.info("浏览器复制功能支持:" + ClipboardJS.isSupported())
                    if (!ClipboardJS.isSupported()) {
                        ToastrUtils.getError("当前浏览器不支持复制功能！");
                    }
                    clipboard.on('success', function (e) {
                        ToastrUtils.getSuccess("复制成功" + e.text);
                        e.clearSelection();
                    });
                    clipboard.on('error', function (e) {
                        ToastrUtils.getError("复制失败！");
                    });
                } else {
                    ToastrUtils.getError("获取失败！");
                    console.log(data);
                }
            });
        },

        formLoadDialog: function (options) {
            var defaults = {
                title: "表单",
                url: options,
                data: null,
                success: null,
                error: null,
                aftersuccess: null,
                aftererror: null,
                autoclose: true,
                formaction: null,
                afterload: null,
                width: 760
            };
            options = $.extend(defaults, options);

            console.info(options);

            Metronic.startPageLoading();
            var formDialogId = "dialog_id_" + new Date().getTime();
            var formDialog = new dialog({
                id: formDialogId,
                width: options.width,
                title: options.title,
                onremove: function () {
                    BtnUtils.currentDialog = null;
                }
            });
            IntelCarUtils.ajax(options.url, options.data, function (res) {
                var formDom = $(res);
                if (options.formaction) {
                    formDom.attr("action", options.formaction);
                }
                if (options.afterload) {
                    options.afterload(formDom);
                }
                formDom.attr("data-dialogid", formDialogId);
                console.info(formDom);
                formDialog.content(formDom);
                formDialog.showModal();
                var formNode = $(formDialog.node);
                formNode.find("select").each(function () {
                    var defaultData = $(this).data("selected");
                    defaultData && $(this).val(defaultData).trigger("change");
                });

                formDialog.button([{
                    value: "关闭",
                    callback: function () {
                    }
                }, {
                    value: "提交",
                    callback: function () {
                        formDialog.statusbar("");
                        var form = formNode.find("form");
                        var $form = $(form);
                        if (!$form.valid()) exit;
                        if ($form.attr("fromIsSending") == "true") {
                            ToastrUtils.getSuccess("请等待", "上次请求执行中");
                            return false;
                            exit(0);
                        }
                        $form.attr("fromIsSending", "true");
                        BtnUtils.ajaxForm(form, function (result) {
                            $form.attr("fromIsSending", "false");
                            if (result.status == 200) {
                                if (options.success) {
                                    options.success(result, form, formDialog);
                                } else {
                                    formDialog.statusbar('<span style="color:green;">' + result.message + '</span>');
                                    BtnUtils.dialogClose(formDialog);
                                    ToastrUtils.getSuccess("操作成功", result.message);
                                }
                                options.aftersuccess && options.aftersuccess(result, form, formDialog);
                            } else {
                                if (options.success) {
                                    options.success(result, form, formDialog);
                                } else {
                                    formDialog.statusbar('<span style="color:red;">' + result.message + '</span>');
                                    BtnUtils.dialogClose(formDialog);
                                    ToastrUtils.getError("操作失败", result.message);
                                }
                                options.aftererror && options.aftererror(result, form, formDialog);
                            }
                        }, function (xhr) {
                            $form.attr("fromIsSending", "false");
                            var errorText = xhr.status + " " + xhr.statusText + ":" + form.attr("action");
                            console.log(errorText)
                            var message = "操作失败,请稍后再试或联系管理员.";
                            formDialog.statusbar('<span style="color:red">' + message + '</span>');
                        });
                        return false;
                    },
                    autofocus: true
                }]);
                Metronic.initAjax();
            });
            return formDialog;
        },

        formLoadFile: function (options) {
            var defaults = {
                title: "表单",
                url: options,
                data: null,
                success: null,
                error: null,
                aftersuccess: null,
                aftererror: null,
                autoclose: true,
                formaction: null,
                afterload: null,
                width: 760
            };
            options = $.extend(defaults, options);

            console.info(options);

            Metronic.startPageLoading();
            var formDialogId = "dialog_id_" + new Date().getTime();
            var formDialog = new dialog({
                id: formDialogId,
                width: options.width,
                title: options.title,
                onremove: function () {
                    BtnUtils.currentDialog = null;
                }
            });

            IntelCarUtils.ajax(options.url, options.data, function (res) {
                var formDom = $(res);
                if (options.formaction) {
                    formDom.attr("action", options.formaction);
                }
                if (options.afterload) {
                    options.afterload(formDom);
                }
                formDom.attr("data-dialogid", formDialogId);
                console.info(formDom);
                formDialog.content(formDom);
                formDialog.showModal();
                var formNode = $(formDialog.node);
                formNode.find("select").each(function () {
                    var defaultData = $(this).data("selected");
                    defaultData && $(this).val(defaultData).trigger("change");
                });

                formDialog.button([{
                    value: "关闭",
                    callback: function () {
                    }
                }, {
                    value: "提交",
                    callback: function () {
                        formDialog.statusbar("");
                        var form = formNode.find("form");
                        var $form = $(form);
                        if (!$form.valid()) exit;
                        if ($form.attr("fromIsSending") == "true") {
                            ToastrUtils.getSuccess("请等待", "上次请求执行中");
                            return false;
                            exit(0);
                        }
                        $form.attr("fromIsSending", "true");

                        IntelCarUtils.ajaxFile($(form).attr("action"), new FormData($form[0]), function (result) {
                            $form.attr("fromIsSending", "false");
                            if (result.status == 200) {
                                if (options.success) {
                                    options.success(result, form, formDialog);
                                } else {
                                    formDialog.statusbar('<span style="color:green;">' + result.message + '</span>');
                                    BtnUtils.dialogClose(formDialog);
                                    ToastrUtils.getSuccess("操作成功", result.message);
                                }
                                options.aftersuccess && options.aftersuccess(result, form, formDialog);
                            } else {
                                if (options.error) {
                                    options.error(result, form, formDialog);
                                } else {
                                    if (result.data) {
                                        for (var i in result.data) {
                                            var errMsg = result.data[i];
                                            var n = 0;
                                            for (var name in errMsg) {
                                                var input = form.find("[name='" + name + "']");
                                                if (input.length > 0 && n++ == 0) {
                                                    input.focus();
                                                } else {
                                                    formDialog.statusbar('<span class="vailderror">' + errMsg[name] + '</span>');
                                                }
                                                var vailderror = input.parent().find("span.vailderror");
                                                if (vailderror.length == 0) {
                                                    vailderror = $("<span class='vailderror'>" + errMsg[name] + "</span>");
                                                    input.parent().append(vailderror);
                                                } else {
                                                    vailderror.html(errMsg[name]);
                                                    vailderror.show();
                                                }
                                                input.change(function (e) {
                                                    $(this).parent().find('span.vailderror').hide();
                                                    $(this).unbind(e);
                                                })
                                            }
                                        }
                                    } else {
                                        formDialog.statusbar('<span style="color:red">' + result.message + '</span>');
                                    }
                                }
                                options.aftererror && options.aftererror(result, form, formDialog);
                            }
                        }, function (xhr) {
                            $form.attr("fromIsSending", "false");
                            var errorText = xhr.status + " " + xhr.statusText + ":" + form.attr("action");
                            console.log(errorText)
                            var message = "操作失败,请稍后再试或联系管理员.";
                            formDialog.statusbar('<span style="color:red">' + message + '</span>');
                        });
                        return false;
                    },
                    autofocus: true
                }]);
                Metronic.initAjax();
            });
            return formDialog;
        },

        ajaxForm: function (form, success, error, type) {
            IntelCarUtils.ajax($(form).attr("action"), $(form).serialize(), success, error, type || "post");
        },

        msgSimple: function (message, time) {
            var d = new dialog({content: message});
            d.show();
            time = time || 800;
            BtnUtils.dialogRemove(d, time);
            return d;
        },

        msgError: function (message, title) {
            title = title || "错误";
            var d = new dialog({content: message, title: title});
            d.show();
            return d;
        },

        msgLoad: function (title) {
            var d = new dialog({title: title});
            d.showModal();
            return d;
        },

        dialogClose: function (dlg) {
            BtnUtils.dialogRemove(dlg, 0);
        },

        dialogRemove: function (dlg, time) {
            setTimeout(function () {
                dlg.close();
                dlg.remove();
            }, time);
        },

        dataTableReload: function reloadDatatable() {
            if (currentDt) {
                currentDt.ajax.reload();
            }
        },

        dialogConfirm: function (text, title, successcbk, cancelcbk) {
            var cDialog = new dialog({
                title: title || "",
                content: text || "",
                width: 230,
                ok: function () {
                    successcbk && successcbk();
                },
                cancel: function () {
                    cancelcbk && cancelcbk()
                }
            });
            cDialog.showModal();
        },

        loading: function ($btn) {
            var $loading = $btn.children(".loading");
            if ($loading.length == 0) {
                $btn.prepend('<img src="' + baselocation + '/resources/images/waiting/hold.gif" width="16" class="loading" hidden="hidden">');
                $loading = $btn.children(".loading");
            } else if ($loading.css("display") != 'none') {
                return null;
            }
            $loading.fadeIn(100);
            return $loading;
        },

        loadingStop: function ($loading) {
            $loading.fadeOut(100);
        }
    };
}();

