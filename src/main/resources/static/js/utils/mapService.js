var MapService = (function () {

    var directionsParam = {
        startImg: baselocation + "/resources/images/map/start.png",
        endImg: baselocation + "/resources/images/map/end.png"
    };

    /**
     * 绘制地图
     * @param mapId 地图显示用div标签id
     * @param depLat 出发纬度
     * @param depLng 出发经度
     * @param arrLat 到达纬度
     * @param arrLng 到达经度
     */
    function displayRoute(mapId, depLat, depLng, arrLat, arrLng) {
        try {
            var map = new google.maps.Map(document.getElementById(mapId), {
                zoom: 12,
                center: {lat: depLat, lng: depLng},
                disableDefaultUI: true,
                zoomControl: true
            });

            var directionsService = new google.maps.DirectionsService;
            var directionsDisplay = new google.maps.DirectionsRenderer({
                draggable: false,
                map: map,
                markerOptions: {icon: directionsParam.startImg, visible: false}
            });

            var reqBody = {
                origin: {
                    lat: parseFloat(depLat), lng: parseFloat(depLng)
                },
                destination: {
                    lat: parseFloat(arrLat), lng: parseFloat(arrLng)
                },
                travelMode: google.maps.TravelMode.DRIVING,
                avoidTolls: true
            };

            directionsService.route(reqBody, function (response, status) {
                if (status === google.maps.DirectionsStatus.OK) {
                    directionsDisplay.setDirections(response);
                }
            });

            new google.maps.Marker({
                position: {"lat": depLat, "lng": depLng},
                map: map,
                icon: directionsParam.startImg
            });
            new google.maps.Marker({
                position: {"lat": arrLat, "lng": arrLng},
                map: map,
                icon: directionsParam.endImg
            });
        } catch (e) {
            console.log(e);
        }
    };
    return {
        displayRoute: displayRoute,
    };
})();
