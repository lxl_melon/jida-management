(function () {
    jQuery(document).ready(function () {
        try {
            var $list = $("#voucher-list").randomId();
            var baseUrl = "./market/voucher";
            var $addBtn = $("#voucher-add-btn").randomId();
            var grid = new Datatable();
            grid.init({
                src: $list,
                dataTable: {
                    "columns": [
                        {data: "id"}, {data: "accountName"},
                        {data: "password"}, {data: "tokenUrl"},
                        {
                            data: "id",
                            createdCell: function (td, tdData) {
                                var delBtn = BtnUtils.confirmBtnCreate(tdData, baseUrl + "/deleteData", "删除", baseUrl);
                                var editBtn = BtnUtils.formBtnCreate(tdData, baseUrl + "/edit", baseUrl + "/editData/", "编辑", baseUrl);
                                $(td).html([editBtn, delBtn]);
                            }
                        }
                    ],
                    "ajax": {
                        "url": baseUrl + "/listVoucherData",
                    },
                }
            });

            IntelCarUtils.pageNameChange($list, "对外发券列表");
            $addBtn.click(function () {
                BtnUtils.formAddNew(baseUrl + "/add",
                    baseUrl + "/addData", "新建对外发券", baseUrl);
            });

        } catch (e) {
            console.error(e);
            alert("页面初始化失败");
        }
    });
})();