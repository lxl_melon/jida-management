(function () {
    jQuery(document).ready(function () {
        try {
            var $form = $("#voucher-add-form").randomId();
            IntelCarUtils.validate($form, {
                accountName: {required: true},
            });
        } catch (e) {
            console.error(e);
            alert("页面初始化失败");
        }
    });
})();