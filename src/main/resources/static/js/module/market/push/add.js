(function () {
    jQuery(document).ready(function () {
        let $addBtn = $("#addBtn").randomId();
        let cancelBtn = $("#cancelBtn").randomId();
        let baseUrl = "./market/push";

        $addBtn.click(function () {
            var data = $('#line-push-addForm').serialize();
            Layout.ajaxify(baseUrl + "/addData",data);
        });

        cancelBtn.click(function () {
            IntelCarUtils.initAjax(baseUrl + "/list");
        });

        var $regionsList = $("#cityId").randomId();
        IntelCarUtils.select2($regionsList, "不填写默认为全国");

        // var $form = $("#line-push-addForm").randomId();
        //     IntelCarUtils.validate($form, {
        //         pushUp: {required: true, number: true, min:0},
        //         smsText:{required: true, isLegal: true}
        //     });
        //
        // jQuery.validator.addMethod("isLegal", function (value, element) {
        //     var length = value.length;
        //     return length < 61;
        // }, "短信内容不能大于61个字");

    });
})();

function checkboxAll(type,val) {
    if(val){
        $("input[name="+type+"]").parent().removeClass('checked');//or
    }
}

function checkboxTags(type,val) {
    if(val) {
        $("input[name=" + type + "]").parent().removeClass('checked');//or
    }
}