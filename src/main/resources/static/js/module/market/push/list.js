(function () {
    jQuery(document).ready(function () {
        try {
            let $list = $("#user-push-table").randomId();
            let $addBtn = $("#user-push-add-btn").randomId();
            let baseUrl = "./market/push";
            let $searchBtn = $("#btn-search").randomId();

            let grid = new Datatable();
            grid.init({
                src: $list,
                dataTable: {
                    bServerSide:true,
                    paging : true,//是否分页
                    pagingType : "full_numbers",//除首页、上一页、下一页、末页四个按钮还有页数按钮
                    lengthChange : true,//是否允许用户改变表格每页显示的记录数
                    ordering : false,//是否允许用户排序
                    processing : true,//是否显示处理状态
                    columns: [
                        {data: "id"},//推送策略ID
                        {data: "pushStrategyName"},//推送策略名称
                        {data: "createTime"},//推送时间
                        {
                            data: "pushStatus", createdCell: function (td, tdData, full) {
                            var statusSpan = '';
                            if (tdData == 0) {
                                statusSpan = '<span class="label label-sm label-success">用户计算中</span>';
                            }else if(tdData == 1){
                                statusSpan = '<span class="label label-sm label-success">推送中</span>';
                            }else {
                                statusSpan = '<span class="label label-sm label-danger">已完成</span>';
                            }
                            $(td).html(statusSpan);
                        }
                        },
                        {
                            data: "pushType", createdCell: function (td, tdData, full) {
                            var statusSpan = '';
                            if (tdData == 0) {
                                statusSpan = '<span class="label label-sm label-success">短信</span>';
                            }else {
                                statusSpan = '<span class="label label-sm label-danger">优惠券</span>';
                            }
                            $(td).html(statusSpan);
                        }
                        },
                        {data: "operator"},//操作人
                        {data: "updateTime"},//更新时间
                        {
                            data: "id",
                            createdCell: function (td, tdData) {
                                url = IntelCarUtils.ajaxUrlGenerate(baseUrl + "/getPushInfoData?", {"id": tdData});
                                $(td).html('<a href="' + url + '" class="ajaxify" uniqueTabCheck="false">策略详情</a>');
                            }
                        }
                    ],
                    "ajax": {
                        url: baseUrl + "/listData",
                    },
                }
            });

            $addBtn.click(function () {
                IntelCarUtils.initAjax(baseUrl + "/add");
                //BtnUtils.formAddNew(baseUrl + "/add", baseUrl + "/addData", "创建标签", baseUrl + "/list");
            });

            $searchBtn.click(function () {
                grid.submitFilter();
            });
            IntelCarUtils.pageNameChange($list,"推送策略列表");
        } catch (e) {
            console.error(e);
            alert("页面初始化失败");
        }
    });
})();