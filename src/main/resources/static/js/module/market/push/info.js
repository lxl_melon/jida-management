(function () {
    jQuery(document).ready(function () {
        let $addBtn = $("#addBtn").randomId();
        let cancelBtn = $("#cancelBtn").randomId();
        let baseUrl = "./market/push";
        var  pushId = $("#pushId").val();
        $addBtn.click(function () {
            $.ajax({
                type:"POST",
                url:baseUrl + "/infoData?id="+pushId,
                dataType:"json", //预期服务器返回数据的类型
                success:function(data){
                    if(data.status == 200){
                        $('.parent-tab.active').trigger('dblclick');
                        IntelCarUtils.initAjax(baseUrl + "/list");
                    }
                },
                error:function(jqXHR){

                }
            });
        });

        cancelBtn.click(function () {
            $.ajax({
                type:"POST",
                url:baseUrl + "/deleteData?id="+pushId,
                dataType:"json", //预期服务器返回数据的类型
                success:function(data){
                    if(data.status == 200){
                        $('.parent-tab.active').trigger('dblclick');
                    }
                },
                error:function(jqXHR){

                }
            });
        });

        var $regionsList = $("#cityId").randomId();
        IntelCarUtils.select2($regionsList, "不填写默认为全国");

    });
})();