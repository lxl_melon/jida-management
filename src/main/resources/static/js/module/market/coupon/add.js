(function () {
    jQuery(document).ready(function () {
        try {
            var $form = $("#coupon-add-form").randomId();
            IntelCarUtils.validate($form, {
                couponName: {required: true, maxlength: 64},
                giftCard: {required: true, maxlength: 64},
                batchNumber: {required: true, maxlength: 64},
                amount: {required: true, digits: true},
                describe: {required: true, maxlength: 15},
                couponResource: {required: true},
                cityId: {required: true},
                productId: {required: true},
                carTypeId: {required: true}
            });

            IntelCarUtils.select2($("#projectName", $form), "请选择项目名");
            IntelCarUtils.select2($("#supplierName", $form), "请选择供应商");
            IntelCarUtils.select2($("#regionsList", $form), "请选择适用城市");
            IntelCarUtils.select2($("#productList", $form), "请选择适用产品");
            IntelCarUtils.select2($("#carTypeList", $form), "请选择适用车型");
            IntelCarUtils.select2($("#supplierName", $form), "请选择供应商名称");
            IntelCarUtils.select2($("#projectName", $form), "请选择内部合作");
            IntelCarUtils.select2($("#distributionName", $form), "请选择供应商");
            IntelCarUtils.select2($("#couponResource", $form), "请选择优惠券使用方");
            IntelCarUtils.select2($("#externalProjectName", $form), "请选择外部合作");


        } catch (e) {
            console.error(e);
            alert("页面初始化失败");
        }

        var $supplierRate = $("input[name = supplierRate]").randomId();
        var $otherRate = $("input[name = otherRate]").randomId();
        var $distributionRate = $("input[name = distributionRate]").randomId();
        var $externalProjectRate = $("input[name = externalProjectRate]").randomId();

        var $supplierName = $('#supplierName').randomId();
        var $projectName = $('#projectName').randomId();
        var $distributionName = $('#distributionName').randomId();
        var $externalProjectName = $('#externalProjectName').randomId();

        changeStatus($supplierName.val(), $supplierRate);
        changeStatus($projectName.val(), $otherRate);
        changeStatus($distributionName.val(), $distributionRate);
        changeStatus($externalProjectName.val(), $externalProjectRate);
        $supplierName.change(function () {
            changeStatus(this.value, $supplierRate);
        });
        $projectName.change(function () {
            changeStatus(this.value, $otherRate);
        });
        $distributionName.change(function () {
            changeStatus(this.value, $distributionRate);
        });
        $externalProjectName.change(function () {
            changeStatus(this.value, $externalProjectRate);
        });
    });

    //改变输入框状态
    function changeStatus(value, data) {
        if (value == '') {
            data.attr("readonly", true);
            data.css('background-color', '#BBB');
            data.attr("value", 0)
        } else {
            data.attr("readonly", false);
            data.css('background-color', '#FFF');
        }
    }
})();
