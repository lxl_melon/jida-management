(function () {
    jQuery(document).ready(function () {
        try {
            var $list = $("#coupon-list").randomId();
            var baseUrl = "./market/coupon";
            var $addBtn = $("#coupon-add-btn").randomId();
            var grid = new Datatable();
            grid.init({
                src: $list,
                dataTable: {
                    "columns": [
                        {data: "id"}, {data: "couponName"},
                        {data: "giftCard"}, {data: "batchNumber"},
                        {data: "amount"}, {data: "cityName"},
                        {data: "createTime"},
                        {
                            data: "id",
                            createdCell: function (td, tdData) {
                                var delBtn = BtnUtils.confirmBtnCreate(tdData, baseUrl + "/deleteData", "删除", baseUrl);
                                var editBtn = BtnUtils.formBtnCreate(tdData, baseUrl + "/edit", baseUrl + "/editData/", "编辑", baseUrl);
                                $(td).html([editBtn, delBtn]);
                            }
                        }
                    ],
                    "ajax": {
                        "url": baseUrl + "/listCouponData",
                    },
                }
            });
            IntelCarUtils.pageNameChange($list, "建券管理");

            $addBtn.click(function () {
                BtnUtils.formAddNew(baseUrl + "/add",
                    baseUrl + "/addData", "新建优惠券", baseUrl);
            });

        } catch (e) {
            console.error(e);
            alert("页面初始化失败");
        }
    });
})();