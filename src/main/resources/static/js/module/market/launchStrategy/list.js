(function () {
    jQuery(document).ready(function () {
        try {
            var $list = $("#launchStrategy-list").randomId();
            var baseUrl = "./market/launchStrategy";
            var $addBtn = $("#launchStrategy-add-btn").randomId();

            let $searchBtn = $("#airport-search").randomId();
            let $status = $("#status-select2").randomId();
            var grid = new Datatable();
            grid.init({
                src: $list,
                dataTable: {
                    "columns": [
                        {data: "id"},
                        {
                            data: "name",
                            createdCell: function (td, tdData, full) {
                                let url = IntelCarUtils.ajaxUrlGenerate(baseUrl + "/edit?", {"id": full['id']});
                                $(td).html('<a href="' + url + '" class="ajaxify" uniqueTabCheck="false">' + tdData + '</a>');
                            }
                        },
                        {data: "effectStartTime"}, {data: "effectEndTime"}, {data: "levelString"},
                        {
                            data: "statusString", createdCell: function (td, tdData) {
                                var statusSpan = '';
                                if (tdData == "未生效") {
                                    statusSpan = '<span class="label label-sm label-' + 'success' + '">未生效</span>';
                                } else if (tdData == "生效中") {
                                    statusSpan = '<span class="label label-sm label-' + 'success' + '">生效中</span>';
                                } else if (tdData == "已结束") {
                                    statusSpan = '<span class="label label-sm label-' + 'warning' + '">已结束</span>';
                                } else {
                                    statusSpan = '<span class="label label-sm label-' + 'warning' + '">未知</span>';
                                }
                                $(td).html(statusSpan);
                            }
                        },
                        {data: "refidName"}, {data: "operator"},
                        {data: "updateTime"},
                        {
                            data: "id",
                            createdCell: function (td, tdData, full) {
                                let effect;
                                let stop;
                                switch (full['statusString']) {
                                    case "未生效": {
                                        effect = BtnUtils.confirmBtnCreate(tdData, baseUrl + "/effectStart", "立即生效", baseUrl);
                                        stop = BtnUtils.confirmBtnCreate(tdData, baseUrl + "/efffectStop", "强制结束", baseUrl);
                                        break;
                                    }
                                    case "生效中": {
                                        var button = $("<button class='btn btn-xs gray' >立即生效</button>");
                                        effect = button;
                                        stop = BtnUtils.confirmBtnCreate(tdData, baseUrl + "/efffectStop", "强制结束", baseUrl);
                                        break;
                                    }
                                    case "已结束": {
                                        var buttonEffect = $("<button class='btn btn-xs gray' >立即生效</button>");
                                        var buttonStop = $("<button class='btn btn-xs gray' >强制结束</button>");
                                        effect = buttonEffect;
                                        stop = buttonStop;
                                        break;
                                    }
                                }
                                var delBtn = BtnUtils.confirmBtnCreate(tdData, baseUrl + "/deleteData", "删除", baseUrl);
                                $(td).html([effect, stop, delBtn]);
                            }
                        }
                    ],
                    "ajax": {
                        "url": baseUrl + "/listLaunchStrategyData",
                    },
                    order: [[0, "desc"]],
                }
            });
            IntelCarUtils.pageNameChange($list, "投放策略列表");
            IntelCarUtils.select2($status, "请选择活动状态");

            $addBtn.click(function () {
                IntelCarUtils.initAjax(baseUrl + "/add");
            });
            $searchBtn.click(function () {
                grid.submitFilter();
            });
        } catch (e) {
            console.error(e);
            alert("页面初始化失败");
        }
    });
})();