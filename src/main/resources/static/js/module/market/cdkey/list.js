(function () {
    function download(data, url,name){
        var button = $("<button class='btn btn-xs'>");
        button.addClass("red");
        button.html(name);
        button.unbind().click(function () {
            url += "?id=" + data +"&";
            let tableContainer = ".table-container";
            let tParam;
            // get all typeable inputs
            $('textarea.form-filter, select.form-filter,input.form-filter[type="checkbox"]:checked,' +
                'input.form-filter[type="radio"]:checked, input.form-filter:not([type="radio"],[type="checkbox"])',
                tableContainer).each(
                function () {
                    tParam = $(this).val();
                    tParam = 'object' == typeof tParam && tParam != null ? tParam.toString() : tParam;
                    url += $(this).attr("name") + "=" + tParam + "&";
                });
            window.open(url);
        });
        return button;
    };

    jQuery(document).ready(function () {
        try {
            var $list = $("#cdkey-list").randomId();
            var baseUrl = "./market/cdkey";
            var $addBtn = $("#cdkey-add-btn").randomId();
            var grid = new Datatable();
            grid.init({
                src: $list,
                dataTable: {
                    "columns": [
                        {data: "id"},
                        {data: "channelName"},
                        {data: "batchNo"},
                        {data: "number"},
                        {data: "createTime"},
                        {
                            data: "id",
                            createdCell: function (td, tdData) {
                                var downBtn = download(tdData, baseUrl + "/download","下载兑换码");
                                $(td).html([downBtn]);
                            }
                        }
                    ],
                    "ajax": {
                        "url": baseUrl + "/listData",
                    },
                }
            });
            IntelCarUtils.pageNameChange($list, "劵码兑换");

            $addBtn.click(function () {
                BtnUtils.formAddNew(baseUrl + "/add",
                    baseUrl + "/addData", "创建兑换码", baseUrl);
            });

        } catch (e) {
            console.error(e);
            alert("页面初始化失败");
        }
    });
})();