(function () {
    //过滤特殊字符
    jQuery.validator.addMethod("specialCharFilter", function(value) {
        var pattern = new RegExp("[`~!@#$^&*()=|{}':;,.<>/?~！@#￥……&*（）——|【】‘；：”“'。，、？%+ 　\"\\\\]");
        var specialStr = "";
        for(var i=0;i<value.length;i++){
            specialStr += value.substr(i, 1).replace(pattern, '');
        }
        if( specialStr === value){
            return true;
        }
        return false;
    },"不能输入特殊字符");
    jQuery(document).ready(function () {
        try {
            var $form = $("#cdkey-add-form").randomId();
            IntelCarUtils.validate($form, {
                channelName: {required: true, maxlength: 50,specialCharFilter:true},
                batchNo: {required: true, maxlength: 30,specialCharFilter:true},
                number: {required: true,specialCharFilter:true,max:500000,min:1},
            });
        } catch (e) {
            console.error(e);
            alert("页面初始化失败");
        }
    });
})();
