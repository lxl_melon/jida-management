(function () {
    jQuery(document).ready(function () {
        try {
            var $list = $("#tips-list").randomId();
            var baseUrl = "./market/tips";
            var grid = new Datatable();
            var $addBtn = $("#tips-add-btn").randomId();
            grid.init({
                src: $list,
                dataTable: {
                    "columns": [
                        {data: "id"}, {data: "name"},
                        {
                            data: "channelType", createdCell: function (td, tdData) {
                                var statusSpan = '';
                                if (tdData == 0) {
                                    statusSpan = '<span class="label label-sm label-' + 'success' + '">APP</span>';
                                } else if (tdData == 1) {
                                    statusSpan = '<span class="label label-sm label-' + 'success' + '">WeChat</span>';
                                } else if(tdData == 3) {
                                    statusSpan = '<span class="label label-sm label-' + 'success' + '">用车小程序</span>';
                                } else {
                                    statusSpan = '<span class="label label-sm label-' + 'warning' + '">未知' + tdData + '</span>';
                                }
                                $(td).html(statusSpan);
                            }
                        },
                        {data: "productIds"},{data: "startTime"}, {data: "endTime"},
                        {
                            data: "status", createdCell: function (td, tdData) {
                                var statusSpan = '';
                                if (tdData == 1) {
                                    statusSpan = '<span class="label label-sm label-' + 'success' + '">冻结</span>';
                                } else if (tdData == 0) {
                                    statusSpan = '<span class="label label-sm label-' + 'success' + '">激活</span>';
                                } else {
                                    statusSpan = '<span class="label label-sm label-' + 'warning' + '">未知' + tdData + '</span>';
                                }
                                $(td).html(statusSpan);
                            }
                        },
                        {data: "operator"},
                        {
                            data: "id",
                            createdCell: function (td, tdData, full) {
                                var delBtn = BtnUtils.confirmBtnCreate(tdData, baseUrl + "/deleteData", "删除", baseUrl);
                                var frozenBtn;
                                // if (full["status"] == 0) {
                                //     frozenBtn = BtnUtils.confirmBtnCreate(tdData, baseUrl + "/frozenData", "冻结", baseUrl);
                                // }
                                var url = IntelCarUtils.ajaxUrlGenerate(baseUrl + "/edit?", {"id": full['id']});
                                var toHtml = "<a href='" + url + "' class='ajaxify' uniqueTabCheck='false'>编辑</a>";
                                $(td).html([delBtn, frozenBtn, ("  " + toHtml)]);
                            }
                        }
                    ],
                    "ajax": {
                        "url": baseUrl + "/listData",
                    },
                    order: [[7, "desc"]],
                }
            });

            $addBtn.click(function () {
                IntelCarUtils.initAjax(baseUrl + "/add");
            });

            IntelCarUtils.pageNameChange($list, "Tips列表");
        } catch (e) {
            console.error(e);
            alert("页面初始化失败");
        }
    });
})();