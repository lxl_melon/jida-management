(function () {
    jQuery(document).ready(function () {
        try {
            var $list = $("#banner-list").randomId();
            var baseUrl = "./market/banner";
            var grid = new Datatable();
            var $addBtn = $("#banner-add-btn").randomId();
            grid.init({
                src: $list,
                dataTable: {
                    "columns": [
                        {data: "id"}, {data: "name"},
                        {
                            data: "channelType", createdCell: function (td, tdData) {
                                var statusSpan = '';
                                if (tdData == 0) {
                                    statusSpan = '<span class="label label-sm label-' + 'success' + '">APP</span>';
                                } else if (tdData == 1) {
                                    statusSpan = '<span class="label label-sm label-' + 'success' + '">WeChat</span>';
                                } else if(tdData == 3) {
                                    statusSpan = '<span class="label label-sm label-' + 'success' + '">用车小程序</span>';
                                } else {
                                    statusSpan = '<span class="label label-sm label-' + 'warning' + '">未知' + tdData + '</span>';
                                }
                                $(td).html(statusSpan);
                            }
                        },
                        {data: "sort"}, {data: "time"}, {data: "updateTime"}, {data: "operator"},
                        {
                            data: "status", createdCell: function (td, tdData) {
                                var statusSpan = '';
                                if (tdData == 0) {
                                    statusSpan = '<span class="label label-sm label-' + 'success' + '">冻结</span>';
                                } else if (tdData == 1) {
                                    statusSpan = '<span class="label label-sm label-' + 'success' + '">激活</span>';
                                } else {
                                    statusSpan = '<span class="label label-sm label-' + 'warning' + '">未知' + tdData + '</span>';
                                }
                                $(td).html(statusSpan);
                            }
                        },
                        {
                            data: "id",
                            createdCell: function (td, tdData, full) {
                                var delBtn = BtnUtils.confirmBtnCreate(tdData, baseUrl + "/deleteData", "删除", baseUrl);
                                var frozenBtn;
                                if (full["status"] == 1) {
                                    frozenBtn = BtnUtils.confirmBtnCreate(tdData, baseUrl + "/frozenData", "冻结", baseUrl);
                                }
                                var url = IntelCarUtils.ajaxUrlGenerate(baseUrl + "/edit?", {"id": full['id']});
                                var toHtml = "<a href='" + url + "' class='ajaxify' uniqueTabCheck='false'>编辑</a>";
                                $(td).html([delBtn, frozenBtn, ("  " + toHtml)]);
                            }
                        }
                    ],
                    "ajax": {
                        "url": baseUrl + "/listBannerData",
                    },
                    order: [[7, "desc"]],
                }
            });

            $addBtn.click(function () {
                IntelCarUtils.initAjax(baseUrl + "/add");
            });

            IntelCarUtils.pageNameChange($list, "Banner列表");
        } catch (e) {
            console.error(e);
            alert("页面初始化失败");
        }
    });
})();