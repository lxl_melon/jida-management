(function () {
    jQuery(document).ready(function () {
        try {
            //INFO TIMCROSSINFO 页面最外部Html标签 定义id , 页面内部用class或其他属性
            //INFO TIMCROSSINFO 对于重复调用页面 , 使用 ramdomId()方法变更为随机id
            var $form = $("#price-basic-add-form").randomId();

            //INFO TIMCROSSINFO 页面传参采用 <input disabled="disabled" pname="productId" th:value="${productId}" type="hidden"/>
            var productId = $("input[pname='productId']", $list).val();

            //INFO TIMCROSSINFO 对于子页面内部 , 使用选择器时 加 ,$form 限制选择器范围
            var $priceInput = $(".priceInput", $form);

            console.log($priceInput.html());
        } catch (e) {
            console.error(e);
            alert("页面初始化失败");
        }
    });
})();