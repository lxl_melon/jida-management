(function () {
    jQuery(document).ready(function () {
        try {
            var grid = new Datatable();
            var baseUrl = "./order/cancel";
            var $list = $("#order-cancel-list");
            var $searchBtn = $("#btn-search");
            grid.init({
                src: $list,
                dataTable: {
                    "columns": [
                        {
                            data: "id",
                            createdCell: function (td, tdData) {
                                var url = baseUrl + "/get?id=" + tdData + "&";
                                $(td).html('<a href="' + url + '" class="ajaxify" uniqueTabCheck="false">' + tdData + '</a>');
                            }
                        }, {data: "userId"}, {data: "cityName"}, {data: "createTime"}
                    ],
                    "ajax": {
                        "url": baseUrl + "/listData",
                    },
                    order: [0, "desc"]
                }
            });

            $searchBtn.click(function () {
                grid.submitFilter();
            });
            IntelCarUtils.pageNameChange($list, "已取消订单");

        } catch (e) {
            console.error(e);
            alert("页面初始化失败");
        }
    });
})();