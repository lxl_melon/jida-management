(function () {
    jQuery(document).ready(function () {
        try {
            var $searchBtn = $("#btn-search").randomId();
            var $punishExportBtn = $("#btn-punish-export").randomId();
            var $generateBillBtn = $(".generate-bill").randomId();

            var $time1 = $("#commissionMonth").randomId();
            var $commissionList = $("#commissionList").randomId();

            IntelCarUtils.monthInput($time1);
            var merchantAccountBaseUrl = "merchant/self/account";
            var grid = new Datatable();
            grid.init({
                src: $commissionList,
                dataTable: {
                    ajax: {
                        url: merchantAccountBaseUrl + "/commission-list/data",
                        type: 'POST',
                    },
                    columns: [
                        {
                            data: "merchantName",
                            createdCell: function (td, tdData) {
                                $(td).html(tdData);
                            }
                        },
                        {data: "collectionMerchantCompany"}, {data: "commissionMonth"},
                        {data: "totalCount"},
                        {data: "commissionAmount"},
                        {data: "punishAmount"},
                        {data: "awardAmount"},
                        {
                            data: "totalAmount",
                            createdCell: function (td, tdData, full) {
                                var url = merchantAccountBaseUrl + "/commission-detail/list?merchantCode=" + full["merchantCode"] + "&month=" + full["commissionMonth"] + "&billStatus=0&"
                                var statusBtn = $('<a href="' + url + '" class="ajaxify" uniqueTabCheck="false">' + tdData + '</a>');
                                $(td).html(statusBtn);
                            }
                        },
                        {
                            data: "merchantCode",
                            createdCell: function (td, tdData, full) {
                                var operatorBtn = $('<button class="btn btn-xs red export-detail" data-merchant-code="' + full["merchantCode"] + '" data-commission-month="' + full["commissionMonth"] + '" data-commission-page="' + full["exportPages"] + '" >明细导出</button>');
                                $(td).html(operatorBtn);
                            }
                        }
                    ]
                }
            });

            $searchBtn.click(function () {
                grid.submitFilter();
            });

            $generateBillBtn.click(function () {
                BtnUtils.dialogConfirm("你确定要生成账单吗?", "提醒", function () {
                    var merchantName = $(".commission-list-container input.form-filter[name=merchantName]").val();
                    var commissionMonth = $(".commission-list-container input.form-filter[name=month]").val();
                    IntelCarUtils.ajax(merchantAccountBaseUrl + "/generate-bill", {
                        merchantName: merchantName,
                        month: commissionMonth
                    }, function (result) {
                        if (result.status == 200) {
                            ToastrUtils.getSuccess("操作成功", result.message);
                            IntelCarUtils.initAjax(merchantAccountBaseUrl + "/commission-bill");
                        } else {
                            ToastrUtils.getError("操作失败", result.message);
                        }
                    });
                });
            });

            $(".commission-list-container").on('click', ".export-detail", function () {
                console.log("export....")
                var _this = $(this);
                var merchantCode = _this.attr("data-merchant-code");
                var commissionMonth = _this.attr("data-commission-month");
                var pages = _this.attr("data-commission-page");
                IntelCarUtils.ajax(merchantAccountBaseUrl + "/commission-detail/export?merchantCode=" + merchantCode + "&month=" + commissionMonth + "&page=" + pages, {}, function (result) {
                    if (result.status == 200) {
                        ToastrUtils.getSuccess("导出成功，请查看邮件");
                    } else {
                        ToastrUtils.getError("操作失败，请重试");
                    }
                });
            });

            $punishExportBtn.click(function () {
                var month = $time1.val();
                IntelCarUtils.ajax(merchantAccountBaseUrl + "/punish-detail-export?month=" + month, {}, function (result) {
                    if (result.status == 200) {
                        ToastrUtils.getSuccess("导出成功，请稍后邮件查收");
                    } else {
                        ToastrUtils.getError("操作失败，请重试");
                    }
                });
            });

            IntelCarUtils.pageNameChange($commissionList, "供应商佣金账单列表");

        } catch (e) {
            console.error(e);
            alert("页面初始化失败");
        }
    });
})();