var CapitalAccountList = function () {
    return {
        init: function ($list, baseUrl) {
            var code = $("#hidCode").val();
            var grid = new Datatable();
            grid.init({
                src: $list,
                dataTable: {
                    "columns": [
                        {data: "code"},
                        {data: "amount"},
                        {data: "balanceAmount"},
                        {data: "finishStat"},
                        {data: "createTime"}],
                    "ajax": {
                        "url": baseUrl + "/official/getCapitalAcctData?code=" + code,
                    },
                }
            });
            IntelCarUtils.pageNameChange($list, "资金账户列表");
        },
    };
}();