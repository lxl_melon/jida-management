(function () {
    jQuery(document).ready(function () {
        try {
            var baseUrl = "./merchant/self";
            var $pageSign = $("#merchantTrackList").randomId();
            var merchantCode = $("#merchantCode").val();
            var grid = new Datatable();
            grid.init({
                src: $pageSign,
                dataTable: {
                    "columns": [{data: "content"}, {
                        data: "type",
                        createdCell: function (td, tdData) {
                            var statusSpan = '';
                            if (tdData == 10) {
                                statusSpan = '<span class="label label-sm label-' + 'success' + '">账户启用</span>';
                            } else if (tdData == 10000) {
                                statusSpan = '<span class="label label-sm label-' + 'warning' + '">账户注销</span>';
                            } else if (tdData == 20){
                                statusSpan = '<span class="label label-sm label-' + 'warning' + '">修改信息</span>';
                            }
                            $(td).html(statusSpan);
                        }
                    }, {data: "operatorName"}, {data: "createTime"}],
                    "ajax": {
                        "url": baseUrl + "/official/track-list?merchantCode=" + merchantCode,
                    },
                    order: [[3, "desc"]],
                }
            });
        } catch (e) {
            console.error(e);
            alert("页面初始化失败");
        }
    });
})();