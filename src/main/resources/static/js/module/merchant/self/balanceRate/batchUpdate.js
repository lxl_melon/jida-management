(function () {
    jQuery(document).ready(function () {
        try {
            var baseUrl = "./merchant/self/balanceRate";
            var $form = $("#balance-rate-batch-update").randomId();
            var $btnTemplateDownload = $("#btn-template-download").randomId();
            var $btnBatchUpdate = $("#btn-batch-update").randomId();

            //下载模板
            $btnTemplateDownload.click(function () {
                window.open(baseUrl + "/templateDownload");
            });

            //批量调整
            $btnBatchUpdate.click(function () {
                if ($form.valid()) {
                    var data = new FormData($form[0]);
                    IntelCarUtils.ajaxFile(baseUrl + "/submitBatchUpdate",
                        data, function (result) {
                            if (result.status == "200") {
                                ToastrUtils.getSuccess("操作成功", result.message);
                                IntelCarUtils.initAjax(baseUrl + "/batchUpdate");
                            } else {
                                ToastrUtils.getError("操作失败", result.message);
                            }
                        }
                    );
                }
            });
            IntelCarUtils.pageNameChange($form, "供应商批量调佣金比例");
        } catch (e) {
            console.error(e);
            alert("页面初始化失败");
        }
    });
})();