(function () {
    jQuery(document).ready(function () {
        try {
            var $searchBtn = $("#btn-search").randomId();

            var $time1 = $("#awardAndPunishMonth").randomId();
            IntelCarUtils.monthInput($time1);
            var startTime = $("#award-and-punish-datetime-1").randomId();
            IntelCarUtils.dateTimeInput(startTime);
            var endTime = $("#award-and-punish-datetime-2").randomId();
            IntelCarUtils.dateTimeInput(endTime);
            var $billStatus = $("#billStatus").randomId();
            IntelCarUtils.select2($billStatus, "请选择账单状态");

            var $awardAndPunishBatchInvalid = $("#awardAndPunishBatchInvalid").randomId();
            var $awardAndPunishBatchImport = $("#awardAndPunishBatchImport").randomId();
            var $awardAndPunishImportRecord = $("#awardAndPunishImportRecord").randomId();

            var $awardAndPunishList = $("#awardAndPunishList").randomId();
            var merchantAccountBaseUrl = "merchant/self/account";

            var grid = new Datatable();
            grid.initPage({
                src: $awardAndPunishList,
                dataTable: {
                    ajax: {
                        url: merchantAccountBaseUrl + "/award-and-punish/list/data",
                        type: 'POST'
                    },
                    columns: [
                        {
                            data: "id",
                            createdCell: function (td, tdData) {
                                $(td).html('<input type="checkbox" class="award-and-punish-id-checkbox" data-value="' + tdData + '" />');
                            }
                        },
                        {data: "orderNo"},
                        {
                            data: "merchantName",
                            createdCell: function (td, tdData) {
                                $(td).html(tdData);
                            }
                        },
                        {data: "parentMerchantName"},
                        {data: "awardAndPunishMonth"},
                        {data: "amount"},
                        {
                            data: "billStatus",
                            createdCell: function (td, tdData, full) {
                                var statusBtn = tdData ? $('<span class="label label-success">是</span>')
                                    : $('<span class="label label-default">否</span>');
                                $(td).html(statusBtn);
                            }
                        },
                        {data: "operatorName"},
                        {data: "operatorTime"},
                        {data: "remark"},
                        {
                            data: "id",
                            createdCell: function (td, tdData, full) {
                                var statusBtn = (full["status"] == -1)
                                    ? $('<span class="label label-default">已作废</span>') :
                                    ((full["status"] >= 2) ? $('<span class="label label-default">不可编辑</span>') : BtnUtils.formBtnCreate(tdData, merchantAccountBaseUrl + "/award-and-punish/edit?id=" + tdData, merchantAccountBaseUrl + "/award-and-punish/edit/data?id=" + tdData, "编辑", merchantAccountBaseUrl + "/award-and-punish/list"));
                                $(td).html(statusBtn);
                            }
                        }
                    ]
                }
            });

            $searchBtn.click(function () {
                grid.submitFilter();
            });
            IntelCarUtils.pageNameChange($awardAndPunishList, "供应商奖罚管理");

            $(".page-container").on("click", "#rewardAndPunishSelectAll", function () {
                if ($("input[name=awardAndPunishSelectAll]:checked").length > 0) {
                    $(".award-and-punish-id-checkbox").attr("checked", "checked");
                    $(".award-and-punish-id-checkbox").parent().addClass("checked");
                } else {
                    $(".award-and-punish-id-checkbox").removeAttr("checked");
                    $(".award-and-punish-id-checkbox").parent().removeClass("checked");
                }
            });

            /**
             * 批量导入
             */
            $awardAndPunishBatchImport.click(function () {
                var option = {
                    data: {},
                    url: merchantAccountBaseUrl + "/award-and-punish/batch-import",
                    formaction: merchantAccountBaseUrl + "/award-and-punish/batch-import/data",
                    title: "奖罚批量导入",
                    aftersuccess: function () {
                        setTimeout(function () {
                            IntelCarUtils.initAjax(merchantAccountBaseUrl + "/award-and-punish/list");
                        }, 2000);
                    },
                }
                BtnUtils.formLoad(option);
            });

            /**
             * 批量作废
             */
            $awardAndPunishBatchInvalid.click(function () {
                BtnUtils.dialogConfirm("你确定要批量作废吗?", "提醒", function () {
                    var url = merchantAccountBaseUrl + "/award-and-punish/batch-invalid";
                    var checkedNodes = $(".award-and-punish-id-checkbox:checked");
                    var checkedIds = "";
                    for (var i = 0; i < checkedNodes.length; i++) {
                        checkedIds += $(checkedNodes[i]).attr("data-value");
                        if (i < checkedNodes.length - 1) {
                            checkedIds += ",";
                        }
                    }
                    if (checkedIds.length <= 0) {
                        ToastrUtils.getError("操作失败", "请选择需要作废的记录");
                    } else {
                        IntelCarUtils.ajax(url, {merchantAccountDetailIds: checkedIds}, function (result) {
                            if (result.status == 200) {
                                ToastrUtils.getSuccess("操作成功", result.message);
                            } else {
                                ToastrUtils.getError("操作失败", result.message);
                            }
                            IntelCarUtils.initAjax(merchantAccountBaseUrl + "/award-and-punish/list");
                        });
                    }
                });
            });

            /**
             * 导入记录
             */
            $awardAndPunishImportRecord.click(function () {
                IntelCarUtils.initAjax(merchantAccountBaseUrl + "/award-and-punish/import-list");
            });

        } catch (e) {
            console.error(e);
            alert("页面初始化失败");
        }
    });
})();