(function () {
    jQuery(document).ready(function () {
        try {
            var baseUrl = "./merchant/self/balanceRate";
            var $pageSign = $("#trackList").randomId();
            var grid = new Datatable();
            grid.init({
                src: $pageSign,
                dataTable: {
                    "columns": [{data: "id"}, {data: "fileUrl"},
                        {data: "quantity"}, {data: "createTime"}, {data: "operatorName"}],
                    "ajax": {
                        "url": baseUrl + "/getTrackListData",
                    },
                    order: [[2, "desc"]],
                }
            });
        } catch (e) {
            console.error(e);
            alert("页面初始化失败");
        }
    });
})();