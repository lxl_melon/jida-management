(function () {
    jQuery(document).ready(function () {
        try {

            var merchantAccountBaseUrl = "merchant/self/account";
            var $awardAndPunishEditSubmit = $("#awardAndPunishEditSubmit").randomId();
            var $awardAndPunishEditInvalid = $("#awardAndPunishEditInvalid").randomId();
            var $awardAndPunishEditCancel = $("#awardAndPunishEditCancel").randomId();

            var $awardAndPunishMonth = $("#awardAndPunishMonth").randomId();
            IntelCarUtils.monthInput($awardAndPunishMonth);


            var $awardAndPunishImportMonth = $("#awardAndPunishImportMonth").randomId();
            IntelCarUtils.monthInput($awardAndPunishImportMonth);

            setTimeout(function () {
                $("#awardAndPunishEditForm").parents(".modal-content").find("#提交").hide();
                $("#awardAndPunishEditForm").parents(".modal-content").find("#关闭").hide();
            }, 0);

            $awardAndPunishEditSubmit.click(function () {
                $.ajax({
                    url: merchantAccountBaseUrl + "/award-and-punish/edit/data",
                    type: 'POST',
                    cache: false,
                    data: new FormData($('#awardAndPunishEditForm')[0]),
                    processData: false,
                    contentType: false,
                    dataType: "json",
                    success: function (res) {
                        if (res.message == 'success') {
                            ToastrUtils.getSuccess("操作成功");
                        } else {
                            ToastrUtils.getError(res.message);
                        }
                        $("#awardAndPunishEditForm").parents(".modal-content").find("button[i=close]").click();
                        IntelCarUtils.initAjax(merchantAccountBaseUrl + "/award-and-punish/list");
                    }
                });
            });

            $awardAndPunishEditInvalid.click(function () {
                $.ajax({
                    url: merchantAccountBaseUrl + "/award-and-punish/invalid",
                    type: 'POST',
                    cache: false,
                    data: new FormData($('#awardAndPunishEditForm')[0]),
                    processData: false,
                    contentType: false,
                    dataType: "json",
                    success: function (res) {
                        if (res.message == 'success') {
                            ToastrUtils.getSuccess("操作成功");
                        } else {
                            ToastrUtils.getError(res.message);
                        }
                        $("#awardAndPunishEditForm").parents(".modal-content").find("button[i=close]").click();
                        IntelCarUtils.initAjax(merchantAccountBaseUrl + "/award-and-punish/list");
                    }
                });
            });

            $awardAndPunishEditCancel.click(function () {
                $("#awardAndPunishEditForm").parents(".modal-content").find("button[i=close]").click();
            })

        } catch (e) {
            console.error(e);
            alert("页面初始化失败");
        }
    });
})();