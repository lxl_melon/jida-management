(function () {
    jQuery(document).ready(function () {
        try {
            var grid = new Datatable();
            var baseUrl = "./merchant/self/account";
            var $list = $("#merchantAccountDetail");
            var merchantCode = $("#merchantAccountDetail #merchantAccountDetailCode").val();
            grid.init({
                src: $list,
                dataTable: {

                    "columns": [
                        {
                            data: "id"
                        },
                        {data: "orderId"}, {data: "amount"},
                        {data: "riskStatus"},
                        {
                            data: "type", createdCell: function (td, tdData) {
                            var statusSpan = '';
                            if (tdData == 0) {
                                statusSpan = '<span class="label label-sm label-' + 'danger' + '">错误数据</span>';
                            } else if (tdData == 1) {
                                statusSpan = '<span class="label label-sm label-' + 'success' + '">订单收入</span>';
                            } else if (tdData == 2) {
                                statusSpan = '<span class="label label-sm label-' + 'warning' + '">处罚</span>';
                            } else if (tdData == 3) {
                                statusSpan = '<span class="label label-sm label-' + 'success' + '">奖励</span>';
                            } else if (tdData == 4) {
                                statusSpan = '<span class="label label-sm label-' + 'success' + '">提现</span>';
                            } else if (tdData == 14) {
                                statusSpan = '<span class="label label-sm label-' + 'warning' + '">平台免单扣除</span>';
                            }
                            $(td).html(statusSpan);
                        }
                        },
                        {data: "reason"}, {data: "createTime"}
                    ],
                    "ajax": {
                        "url": baseUrl + "/detailData?merchantCode=" + merchantCode,
                    },
                    "order": [[0, "desc"]]
                }
            });

            $(".merchant-account-detail-btn").click(function () {
                var accountDetailStatus = $(this).attr("data-status");
                $("#merchantAccountDetailStatus").val(accountDetailStatus);
                $(".merchant-account-detail-btn").removeClass("green-meadow");
                $(this).addClass("green-meadow");
                grid.submitFilter();
            });

            IntelCarUtils.pageNameChange($list, "供应商账户明细");
        } catch (e) {
            console.error(e);
            alert("页面初始化失败");
        }
    });
})();