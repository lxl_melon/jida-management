(function () {
    jQuery(document).ready(function () {
        try {
            var $awardAndPunishImportList = $("#awardAndPunishImportList").randomId();
            var merchantAccountBaseUrl = "merchant/self/account";

            var grid = new Datatable();
            grid.init({
                src: $awardAndPunishImportList,
                dataTable: {
                    ajax: {
                        url: merchantAccountBaseUrl + "/award-and-punish/import-list/data",
                        type: 'POST'
                    },
                    columns: [
                        {
                            data: "operator"
                        },
                        {data: "operateTime"},
                        {
                            data: "originDataUrl",
                            createdCell: function (td, tdData) {
                                var linkBtn = $('<a class="label label-success" href="' + tdData + '">下载</a>');
                                $(td).html(linkBtn);
                            }
                        },
                        {
                            data: "failDataUrl",
                            createdCell: function (td, tdData) {
                                var linkBtn = $('<a class="label label-warning" href="' + tdData + '">下载</a>');
                                $(td).html(linkBtn);
                            }
                        }
                    ]
                }
            });

            IntelCarUtils.pageNameChange($awardAndPunishImportList, "供应商奖罚导入记录");

        } catch (e) {
            console.error(e);
            alert("页面初始化失败");
        }
    });
})();