(function () {
    jQuery(document).ready(function () {
        try {

            var merchantAccountBaseUrl = "merchant/self/account";
            var $awardAndPunishSubmit = $("#awardAndPunishSubmit").randomId();

            $("#awardAndPunishTemplate").click(function () {
                window.open(merchantAccountBaseUrl + "/award-and-punish/template-export");
            });

            var $awardAndPunishImportMonth = $("#awardAndPunishImportMonth").randomId();
            IntelCarUtils.monthInput($awardAndPunishImportMonth);

            setTimeout(function () {
                $(".award-and-punish-import-container").parents(".modal-content").find("#提交").hide();
                $(".award-and-punish-import-container").parents(".modal-content").find("#关闭").hide();
            }, 0);

            $awardAndPunishSubmit.click(function () {
                if (!$("#awardAndPunishExcel").val()) {
                    ToastrUtils.getError("请选择需要导入的文件");
                    return;
                }
                if (!$awardAndPunishImportMonth.val()) {
                    ToastrUtils.getError("请选择奖励月份");
                    return;
                }
                $.ajax({
                    url: merchantAccountBaseUrl + "/award-and-punish/batch-import/data",
                    type: 'POST',
                    cache: false,
                    data: new FormData($('#awardAndPunishImportForm')[0]),
                    processData: false,
                    contentType: false,
                    dataType: "json",
                    success: function (res) {
                        if (res.message = 'success') {
                            ToastrUtils.getSuccess("导入完毕！");
                        } else {
                            ToastrUtils.getError("操作失败请稍后重试");
                        }
                        $(".award-and-punish-import-container").parents(".modal-content").find("button[i=close]").click();
                        IntelCarUtils.initAjax(merchantAccountBaseUrl + "/award-and-punish/list");
                    }
                });
            });

        } catch (e) {
            console.error(e);
            alert("页面初始化失败");
        }
    });
})();