(function () {
    jQuery(document).ready(function () {
        try {
            var baseUrl = "./merchant/self";

            var $driverBatchMove = $(".driver-batch-move").randomId();

            var $list = $("#invalidList").randomId();
            var $searchBtn = $("#btn-search").randomId();

            var grid = new Datatable();
            grid.init({
                src: $list,
                dataTable: {
                    "columns": [
                        {
                            data: "id",
                            createdCell: function (td, tdData) {
                                var url = baseUrl + "/official/get?id=" + tdData + "&";
                                $(td).html('<a href="' + url + '" class="ajaxify" uniqueTabCheck="false">' + tdData + '</a>');
                            }
                        },
                        {data: "code"}, {data: "companyName"}, {data: "parentMerchantCompany"},
                        {data: "regionName"}, {data: "drivers"},
                        {data: "balanceRates"},
                        {data: "invitationCode"}, {data: "abutName"},
                        {
                            data: "abutStatus", createdCell: function (td, tdData) {
                                var statusSpan = '';
                                if (tdData == 0) {
                                    statusSpan = '<span class="label label-sm label-' + 'warning' + '">已注销</span>';
                                } else if (tdData == 1) {
                                    statusSpan = '<span class="label label-sm label-' + 'success' + '">谈判中</span>';
                                } else if (tdData == 2) {
                                    statusSpan = '<span class="label label-sm label-' + 'success' + '">已签约</span>';
                                } else if (tdData == 3) {
                                    statusSpan = '<span class="label label-sm label-' + 'success' + '">服务中</span>';
                                } else if (tdData == 4) {
                                    statusSpan = '<span class="label label-sm label-' + 'success' + '">暂停服务</span>';
                                }
                                $(td).html(statusSpan);
                            }
                        },
                        {data: "invalidTime"}, {data: "invalidOperator"}
                    ],
                    "ajax": {
                        "url": baseUrl + "/official/service-invalid-list/data",
                    },
                }
            });

            $searchBtn.click(function () {
                grid.submitFilter();
            });
            IntelCarUtils.pageNameChange($list, "已注销供应商列表");
        } catch (e) {
            console.error(e);
            alert("页面初始化失败");
        }
    });
})();