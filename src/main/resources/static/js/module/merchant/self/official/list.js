(function () {
    jQuery(document).ready(function () {
        try {
            var baseUrl = "./merchant/self";
            var $list = $("#officialList").randomId();
            var $searchBtn = $("#btn-search").randomId();
            var grid = new Datatable();
            grid.init({
                src: $list,
                dataTable: {
                    "columns": [
                        {
                            data: "id",
                            createdCell: function (td, tdData) {
                                var url = baseUrl + "/official/get?id=" + tdData + "&";
                                $(td).html('<a href="' + url + '" class="ajaxify" uniqueTabCheck="false">' + tdData + '</a>');
                            }
                        },
                        {data: "code"}, {data: "companyName"}, {data: "parentMerchantCompany"}, {data: "regionName"}, {data: "drivers"}, {data: "balanceRates"}, {data: "code"}, {data: "invitationCode"}, {data: "abutName"},
                        {
                            data: "abutStatus", createdCell: function (td, tdData) {
                                var statusSpan = '';
                                if (tdData == 0) {
                                    statusSpan = '<span class="label label-sm label-' + 'warning' + '">已注销</span>';
                                } else if (tdData == 1) {
                                    statusSpan = '<span class="label label-sm label-' + 'success' + '">谈判中</span>';
                                } else if (tdData == 2) {
                                    statusSpan = '<span class="label label-sm label-' + 'success' + '">已签约</span>';
                                } else if (tdData == 3) {
                                    statusSpan = '<span class="label label-sm label-' + 'success' + '">服务中</span>';
                                } else if (tdData == 4) {
                                    statusSpan = '<span class="label label-sm label-' + 'success' + '">暂停服务</span>';
                                }
                                $(td).html(statusSpan);
                            }
                        },
                        {data: "createTime"}],
                    "ajax": {
                        "url": baseUrl + "/officialData",
                    },
                }
            });

            $(".descAdd").click(function () {
                IntelCarUtils.initAjax(baseUrl + "/add");

            });

            //导出表格按钮监听
            $("#merchant-export").click(function () {
                let url = baseUrl + "/merchantExport";
                window.open(url);
            });

            $searchBtn.click(function () {
                grid.submitFilter();
            });
            IntelCarUtils.pageNameChange($list, "正式供应商列表");
        } catch (e) {
            console.error(e);
            alert("页面初始化失败");
        }
    });
})();