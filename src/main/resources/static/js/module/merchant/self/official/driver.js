(function () {
    jQuery(document).ready(function () {
        try {
            var baseUrl = "./merchant/driver";
            var $list = $("#driverList").randomId();

            var $driverBatchMoveBtn = $(".driver-batch-move").randomId();

            var merchantOfficialBaseUrl = "./merchant/self/official";
            var code = $("#hidMerchatCode").val();

            var grid = new Datatable();
            grid.init({
                src: $list,
                dataTable: {
                    "columns": [
                        /*{
                            data: "id",
                            createdCell: function (td, tdData) {
                                url = IntelCarUtils.ajaxUrlGenerate(baseUrl + "/official/get?", {"id": tdData});
                                $(td).html('<a href="' + url + '" class="ajaxify" uniqueTabCheck="false">' + tdData + '</a>');
                            }
                        },*/
                        {data: "id"},
                        {data: "name"}, {data: "cellphone"},
                        {data: "cityName"}, {data: "carBrand"},
                        {data: "carType"}, {data: "merchant"}
                    ],
                    "ajax": {
                        "url": "./merchant/self/getDriverByCode?code=" + code,
                    },
                }
            });

            /**
             * 司机批量转移
             */
            $driverBatchMoveBtn.click(function () {
                var option = {
                    data: {"code": code},
                    url: merchantOfficialBaseUrl + "/driver/batch-move",
                    formaction: merchantOfficialBaseUrl + "/driver/batch-move/data",
                    title: "司机批量转移",
                    aftersuccess: function () {
                        setTimeout(function () {
                            IntelCarUtils.initAjax(merchantOfficialBaseUrl + "/driver");
                        }, 2000);
                    },
                }
                BtnUtils.formLoad(option);
            });

            IntelCarUtils.pageNameChange($list, "司机列表");
        } catch (e) {
            console.error(e);
            alert("页面初始化失败");
        }
    });
})();