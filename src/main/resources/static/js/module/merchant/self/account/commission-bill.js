(function () {
    jQuery(document).ready(function () {
        try {
            var $searchBtn = $("#btn-search").randomId();
            var $commissionBillExportBtn = $("#btn-commission-bill-export").randomId();
            var $commissionBillDetailExportBtn = $("#btn-commission-bill-detail-export").randomId();

            var $time1 = $("#commissionMonth").randomId();
            var $commissionBillList = $("#commissionBillList").randomId();
            IntelCarUtils.monthInput($time1);
            var merchantAccountBaseUrl = "merchant/self/account";

            var grid = new Datatable();
            grid.initPage({
                src: $commissionBillList,
                dataTable: {
                    ajax: {
                        type: 'POST',
                        url: merchantAccountBaseUrl + "/commission-bill/data"
                    },
                    columns: [
                        {data: "transactionNo"},
                        {
                            data: "merchantName",
                            createdCell: function (td, tdData) {
                                $(td).html(tdData);
                            }
                        },
                        {data: "collectionMerchantCompany"},
                        {data: "commissionMonth"},
                        {data: "commissionAmount"},
                        {data: "punishAmount"},
                        {data: "awardAmount"},
                        {
                            data: "totalAmount",
                            createdCell: function (td, tdData, full) {
                                var url = merchantAccountBaseUrl + "/commission-detail/list?merchantCode=" + full["merchantCode"] + "&month=" + full["commissionMonth"] + "&billStatus=1&transactionNo=" + full["transactionNo"]
                                var statusBtn = $('<a href="' + url + '" class="ajaxify" uniqueTabCheck="false">' + tdData + '</a>');
                                $(td).html(statusBtn);
                            }
                        },
                        {data: "operatorName"},
                        {data: "operatorTime"}
                    ]
                }
            });

            $searchBtn.click(function () {
                grid.submitFilter();
            });

            $commissionBillExportBtn.click(function () {
                var url = merchantAccountBaseUrl + "/commission-bill/export?";
                var tableContainer = $commissionBillList.parents(".table-container");
                var tParam;
                // get all typeable inputs
                $('textarea.form-filter, select.form-filter,input.form-filter[type="checkbox"]:checked,' +
                    'input.form-filter[type="radio"]:checked, input.form-filter:not([type="radio"],[type="checkbox"])',
                    tableContainer).each(
                    function () {
                        tParam = $(this).val();
                        tParam = 'object' == typeof tParam && tParam != null ? tParam.toString() : tParam;
                        url += $(this).attr("name") + "=" + tParam + "&";
                    });
                IntelCarUtils.ajax(url, {}, function (result) {
                    if (result.status == 200) {
                        ToastrUtils.getSuccess("导出成功，请查看邮件");
                    } else {
                        ToastrUtils.getError(result.message);
                    }
                });
            });

            $commissionBillDetailExportBtn.click(function () {
                var url = merchantAccountBaseUrl + "/commission-bill/detail/export?";
                var tableContainer = $commissionBillList.parents(".table-container");
                var tParam;
                // get all typeable inputs
                $('textarea.form-filter, select.form-filter,input.form-filter[type="checkbox"]:checked,' +
                    'input.form-filter[type="radio"]:checked, input.form-filter:not([type="radio"],[type="checkbox"])',
                    tableContainer).each(
                    function () {
                        tParam = $(this).val();
                        tParam = 'object' == typeof tParam && tParam != null ? tParam.toString() : tParam;
                        url += $(this).attr("name") + "=" + tParam + "&";
                    });
                IntelCarUtils.ajax(url, {}, function (result) {
                    if (result.status == 200) {
                        ToastrUtils.getSuccess("导出成功，请查看邮件");
                    } else {
                        ToastrUtils.getError(result.message);
                    }
                });
            });

            IntelCarUtils.pageNameChange($commissionBillList, "供应商佣金账单列表");

        } catch (e) {
            console.error(e);
            alert("页面初始化失败");
        }
    });
})();