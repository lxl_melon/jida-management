(function () {
    jQuery(document).ready(function () {
        try {
            var baseUrl = "./merchant/self";
            var $pageSign = $("#merchant-self-official-get").randomId();
            var id = $("input[pname='merchantId']", $pageSign).val();
            var code = $("input[pname='merchantCode']", $pageSign).val();
            var $pageName = $("input[pname='pageName']", $pageSign).val();
            var qrCodeUrl = $("input[pname='qrCodeUrl']", $pageSign).val();

            var $editInfo = $("#edit_info").randomId();
            var $editPwd = $("#edit_pwd").randomId();
            var $findDriver = $("#find_driver").randomId();
            var $findAccount = $("#find_account").randomId();
            var $invalidAccount = $("#invalidAccount").randomId();
            var $validAccount = $("#validAccount").randomId();
            var $changeParentMerchant = $("#changeParentMerchant").randomId();
            var $addTransportManagementAccount = $("#addTransportManagementAccount").randomId();
            var $acceptOrdinaryBtn = $("#acceptOrdinaryBtn").randomId();
            var $noAcceptOrdinaryBtn = $("#noAcceptOrdinaryBtn").randomId();
            var $acceptDiscountBtn = $("#acceptDiscountBtn").randomId();
            var $noAcceptDiscountBtn = $("#noAcceptDiscountBtn").randomId();
            
            //调整是否开通接送机订单
            var changeAssignAirport = $("#changeAssignAirport").randomId();
            changeAssignAirport.click(function () {
                BtnUtils.formAddNew(baseUrl + "/changeAssignAirport", baseUrl + "/changeAssignAirportData?id=" + id, "调整是否开通接送机订单", baseUrl + "/official/get?id=" + id + "&");
            });

            //调整派单权重
            var changeWeight = $("#changeWeight").randomId();
            changeWeight.click(function () {
                BtnUtils.formAddNew(baseUrl + "/changeWeight?id=" + id, baseUrl + "/changeWeightData?id=" + id, "调整派单权重", baseUrl + "/official/get?id=" + id + "&");
            });

            //修改信息
            $editInfo.click(function () {
                IntelCarUtils.initAjax(baseUrl + "/edit?id=" + id);
                IntelCarUtils.pageNameChange($pageSign, "编辑-" + $pageName);
            });

            //修改密码
            $editPwd.click(function () {
                IntelCarUtils.ajax(baseUrl + "/resetPwd?id=" + id, {}, function (result) {
                    ToastrUtils.getSuccess("操作成功", result.message);
                });
            });

            $addTransportManagementAccount.click(function () {
                IntelCarUtils.ajax(baseUrl + "/addTransportManagementAccount?id=" + id, {}, function (result) {
                    if (result.status == "200") {
                        ToastrUtils.getSuccess("添加成功", result.message);
                        IntelCarUtils.initAjax(baseUrl + "/official/get?id=" + id);
                    } else {
                        ToastrUtils.getError("添加失败", result.message);
                    }
                });
            });

            //查看司机
            $findDriver.click(function () {
                IntelCarUtils.initAjax(baseUrl + "/official/driver?code=" + code);
            });

            //查看资金账户
            $findAccount.click(function () {
                IntelCarUtils.initAjax(baseUrl + "/official/capitalAccount?code=" + code);
            });

            // 注销账户
            $invalidAccount.click(function () {
                BtnUtils.dialogConfirm("你确定要注销账户吗?", "提醒", function () {
                    IntelCarUtils.ajax(baseUrl + "/official/invalidAccount", {code: code}, function (result) {
                        if (result.status == "200") {
                            ToastrUtils.getSuccess("操作成功", result.message);
                        } else {
                            ToastrUtils.getError("操作失败", result.message);
                        }
                        IntelCarUtils.initAjax(baseUrl + "/official/get?id=" + id);
                    });
                });
            });

            // 启用账户
            $validAccount.click(function () {
                BtnUtils.dialogConfirm("你确定要启用账户吗?", "提醒", function () {
                    IntelCarUtils.ajax(baseUrl + "/official/validAccount", {code: code}, function (result) {
                        if (result.status == "200") {
                            ToastrUtils.getSuccess("操作成功", result.message);
                        } else {
                            ToastrUtils.getError("操作失败", result.message);
                        }
                        IntelCarUtils.initAjax(baseUrl + "/official/get?id=" + id);
                    });
                });
            });

            // 修改母租赁公司
            $changeParentMerchant.click(function () {
                BtnUtils.formAddNew(baseUrl + "/change-parent-merchant?code=" + code, baseUrl + "/change-parent-merchant/data?code=" + code, "调整租赁公司", baseUrl + "/official/get?id=" + id + "&");
            });

            //更改显示在 标签页 的文字
            IntelCarUtils.pageNameChange($pageSign, $pageName);
            // 司机拉新二维码
            new QRCode(document.getElementById("qrcodeDriverRegister"), {
                text: qrCodeUrl,
                width: 150,
                height: 150,
                correctLevel: QRCode.CorrectLevel.H,
            });
            
            $acceptOrdinaryBtn.click(function () {
                BtnUtils.dialogConfirm("你确定要接普通订单吗?", "提醒", function () {
                    IntelCarUtils.ajax(baseUrl + "/official/isOrdinary", {code: code,isOrdinary : 1}, function (result) {
                        if (result.status == "200") {
                            ToastrUtils.getSuccess("操作成功", result.message);
                        } else {
                            ToastrUtils.getError("操作失败", result.message);
                        }
                        IntelCarUtils.initAjax(baseUrl + "/official/get?id=" + id);
                    });
                });
            });

            $noAcceptOrdinaryBtn.click(function () {
                BtnUtils.dialogConfirm("你确定不接普通订单吗?", "提醒", function () {
                    IntelCarUtils.ajax(baseUrl + "/official/isOrdinary", {code: code,isOrdinary : 0}, function (result) {
                        if (result.status == "200") {
                            ToastrUtils.getSuccess("操作成功", result.message);
                        } else {
                            ToastrUtils.getError("操作失败", result.message);
                        }
                        IntelCarUtils.initAjax(baseUrl + "/official/get?id=" + id);
                    });
                });
            });
            
            $acceptDiscountBtn.click(function () {
                BtnUtils.dialogConfirm("你确定要接滴滴特惠订单吗?", "提醒", function () {
                    IntelCarUtils.ajax(baseUrl + "/official/isDiscount", {code: code,isDiscount : 1}, function (result) {
                        if (result.status == "200") {
                            ToastrUtils.getSuccess("操作成功", result.message);
                        } else {
                            ToastrUtils.getError("操作失败", result.message);
                        }
                        IntelCarUtils.initAjax(baseUrl + "/official/get?id=" + id);
                    });
                });
            });

            $noAcceptDiscountBtn.click(function () {
                BtnUtils.dialogConfirm("你确定不接滴滴特惠订单吗?", "提醒", function () {
                    IntelCarUtils.ajax(baseUrl + "/official/isDiscount", {code: code,isDiscount : 0}, function (result) {
                        if (result.status == "200") {
                            ToastrUtils.getSuccess("操作成功", result.message);
                        } else {
                            ToastrUtils.getError("操作失败", result.message);
                        }
                        IntelCarUtils.initAjax(baseUrl + "/official/get?id=" + id);
                    });
                });
            });
        } catch (e) {
            console.error(e);
            alert("页面初始化失败");
        }
    });
})();