(function () {
    jQuery(document).ready(function () {
        try {
            var $searchBtn = $("#btn-search").randomId();

            var $commissionDetailList = $("#commissionDetailList").randomId();
            var $time1 = $("#commission-detail-datetime-1").randomId();
            var $time2 = $("#commission-detail-datetime-2").randomId();
            IntelCarUtils.dateTimeInput($time1);
            IntelCarUtils.dateTimeInput($time2);

            var $hiddenMonth = $("#hiddenMonth").randomId();
            var $hiddenMerchantCode = $("#hiddenMerchantCode").randomId();
            var $billStatus = $("#billStatus").randomId();
            var $defaultTransactionNo = $("#defaultTransactionNo").randomId();

            var merchantAccountBaseUrl = "merchant/self/account";

            var grid = new Datatable();
            grid.initPage({
                src: $commissionDetailList,
                dataTable: {
                    ajax: {
                        url: merchantAccountBaseUrl + "/commission-detail-list/data?merchantCode=" + $hiddenMerchantCode.val() + "&month=" + $hiddenMonth.val() + "&billStatus=" + $billStatus.val() + "&defaultTransactionNo" + $defaultTransactionNo.val(),
                        type: 'POST'
                    },
                    columns: [
                        {data: "orderId"},
                        {
                            data: "orderTcId"
                        },
                        {data: "merchantCompanyName"}, {data: "parentMerchantCompanyName"},
                        {
                            data: "productType",
                            createdCell: function (td, tdData, full) {
                                var statusSpan = '';
                                var statusSpanText = "";
                                if (tdData == 11) {
                                    statusSpanText = "预约专车";
                                } else if (tdData == 12) {
                                    statusSpanText = "接机";
                                } else if (tdData == 13) {
                                    statusSpanText = "送机";
                                } else if (tdData == 14) {
                                    statusSpanText = "接站";
                                } else if (tdData == 15) {
                                    statusSpanText = "送站";
                                } else if (tdData == 19) {
                                    statusSpanText = "即时专车";
                                } else if (tdData == 24) {
                                    statusSpanText = "即时出租车";
                                } else if (tdData == 65) {
                                    statusSpanText = "预约出租车";
                                }
                                statusSpan = '<span class="label label-sm label-' + 'success' + '">' + statusSpanText + '</span>';
                                $(td).html(statusSpan);
                            }
                        },
                        {data: "driverName"}, {data: "driverCellphone"},
                        {data: "createTime"}, {data: "typeDesc"}, {data: "amount"},
                        {
                            data: "status",
                            createdCell: function (td, tdData, full) {
                                var statusBtn = tdData >= 2 ? $('<span class="label label-success">是</span>')
                                    : $('<span class="label label-default">否</span>');
                                $(td).html(statusBtn);
                            }
                        }
                    ]
                }
            });

            $searchBtn.click(function () {
                grid.submitFilter();
            });
            IntelCarUtils.pageNameChange($commissionDetailList, "佣金明细");

        } catch (e) {
            console.error(e);
            alert("页面初始化失败");
        }
    });
})();