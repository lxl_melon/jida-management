(function () {
    jQuery(document).ready(function () {
        try {
            var baseUrl = "./merchant/self";
            var $list = $("#auditList").randomId();
            var $searchBtn = $("#btn-search").randomId();
            var grid = new Datatable();
            grid.init({
                src: $list,
                dataTable: {
                    "columns": [
                        {
                            data: "id",
                            createdCell: function (td, tdData) {
                                var url = baseUrl + "/audit/get?id=" + tdData + "&"
                                $(td).html('<a href="' + url + '" class="ajaxify" uniqueTabCheck="false">' + tdData + '</a>');
                            }
                        },
                        {data: "code"}, {data: "companyName"}, {data: "parentMerchantCompany"}, {data: "drivers"}, {data: "balanceRates"}, {data: "code"}, {data: "createTime"}],
                    "ajax": {
                        "url": baseUrl + "/auditData",
                    },
                }
            });

            $searchBtn.click(function () {
                grid.submitFilter();
            });

            IntelCarUtils.pageNameChange($list, "待审核供应商列表");
        } catch (e) {
            console.error(e);
            alert("页面初始化失败");
        }
    });
})();