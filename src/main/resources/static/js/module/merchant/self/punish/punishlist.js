(function () {
    jQuery(document).ready(function () {
        try {
            var baseUrl = "./merchant/self";
            var $punishInfoList = $("#punishInfoList").randomId();
            var id =  $("#merchantId").val();
            var merchantCode = $("#merchantCode").val();
            var grid = new Datatable();
            grid.init({
                src: $punishInfoList,
                dataTable: {
                    "columns": [{data: "fineFee"}, {data: "conent"},
                        {data: "operator"}, {data: "createTime"},
                        {
                            data: "id",
                            createdCell: function (td, tdData, full) {
                                var statusBtn = (full["status"]);
                                if ((full["status"]) == 0) {
                                    statusBtn = BtnUtils.formBtnCreate(tdData, baseUrl + "/cancelPunish?punishId=" + full["id"],
                                        baseUrl + "/cancelPunishData?punishId=" + full["id"], "撤销", baseUrl + "/official/get?id=" + id, "green");
                                } else {
                                    statusBtn = '<span class="label label-default">已撤销</span>';
                                }
                                $(td).html(statusBtn);
                            }
                        }, {data: "cancelReason"}],
                    "ajax": {
                        "url": baseUrl + "/getPunishInfoListData?merchantCode=" + merchantCode,
                    },
                    order: [[4, "desc"]]
                }
            });
        } catch (e) {
            console.error(e);
            alert("页面初始化失败");
        }
    });
})();