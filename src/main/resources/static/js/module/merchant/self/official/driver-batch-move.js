(function () {
    jQuery(document).ready(function () {
        try {

            var merchantOfficialBaseUrl = "merchant/self/official";
            var $driverBatchMoveSubmit = $("#driverBatchMoveSubmit").randomId();

            var merchantCode = $(".driver-batch-move-container input[name=merchantCode]").val();

            var $targetMerchant = $("#targetMerchant").randomId();
            var $merchantRegionName = $("#merchantRegionName").randomId();
            var $moveCount = $("#moveCount").randomId();
            var $moveDriverStatus = $("#moveDriverStatus").randomId();
            var $moveNetStatus = $("#moveNetStatus").randomId();
            var $moveRoadStatus = $("#moveRoadStatus").randomId();
            var $moveDriverSilenceStatus = $("#moveDriverSilenceStatus").randomId();

            IntelCarUtils.select2($targetMerchant, "请选择目标供应商");
            IntelCarUtils.select2($merchantRegionName, "请选择转移城市");
            IntelCarUtils.select2($moveCount, "请选择转移数量");
            IntelCarUtils.select2($moveDriverStatus, "请选择转移司机状态");
            IntelCarUtils.select2($moveNetStatus, "请选择转移司机网约车驾驶证状态");
            IntelCarUtils.select2($moveRoadStatus, "请选择转移司机道路许可证状态");
            IntelCarUtils.select2($moveDriverSilenceStatus, "请选择转移司机停止接单状态");

            setTimeout(function () {
                $(".driver-batch-move-container").parents(".modal-content").find("#提交").hide();
                $(".driver-batch-move-container").parents(".modal-content").find("#关闭").hide();
            }, 0);

            $driverBatchMoveSubmit.click(function () {
                $.ajax({
                    url: merchantOfficialBaseUrl + "/driver/batch-move/data",
                    type: 'POST',
                    cache: false,
                    data: new FormData($('#driverBatchMoveForm')[0]),
                    processData: false,
                    contentType: false,
                    dataType: "json",
                    success: function (res) {
                        if (res.status == 200) {
                            ToastrUtils.getSuccess("批量转移成功");
                            $(".driver-batch-move-container").parents(".modal-content").find("button[i=close]").click();
                            IntelCarUtils.initAjax(merchantOfficialBaseUrl + "/driver?code=" + merchantCode);
                        } else {
                            ToastrUtils.getError(res.message);
                            return;
                        }
                    }
                });
            });

        } catch (e) {
            console.error(e);
            alert("页面初始化失败");
        }
    });
})();