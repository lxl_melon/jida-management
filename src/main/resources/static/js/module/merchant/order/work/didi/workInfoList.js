jQuery(document).ready(function () {
    init();


    function initWorkTable($WorkListTable, workBaseUrl, redirectUrl, orderId) {
        try {
            var grid = new Datatable();
            grid.init({
                src: $WorkListTable,
                dataTable: {
                    "columns": [{
                        data: "workId"
                    }, {
                        data: "type",
                        createdCell: function (td, tdData) {
                            if (tdData == '1') {
                                $(td).html('<span class="label label-sm label-warning" style="background-color: #89C4F4">安全</span>');
                            } else {
                                $(td).html('<span class="label label-sm label-warning" style="background-color: #adadad">普通</span>');
                            }
                        }
                    },
                        {
                            data: "level",
                            createdCell: function (td, tdData, full) {
                                if (tdData > 0) {
                                    if (full["urgentReply"] == 0) {
                                        editBtn = BtnUtils.createClickBtn(workBaseUrl + "/handlerWork?action=urgent_reply&description=回复加急&orderId=" + orderId + "&workId=" + full["workId"] + "&didiWorkId=" + full["distributorWorkId"], redirectUrl, "回复加急");
                                        $(td).html([editBtn]);
                                    } else {
                                        $(td).html('<span class="label label-sm label-warning" style="background-color: #437cae">加急已回复</span>');
                                    }
                                } else {
                                    $(td).html('<span class="label label-sm label-warning" style="background-color: #adadad">未加急</span>');
                                }
                            }
                        }, {data: "title"}, {
                            data: "source",
                            createdCell: function (td, tdData) {
                                if (tdData == 'ark') {
                                    $(td).html('<span class="label label-sm label-warning" style="background-color: #89C4F4">滴滴</span>');
                                } else {
                                    $(td).html('<span class="label label-sm label-warning" style="background-color: #89C4F4">秒走</span>');
                                }
                            }
                        }, {data: "createTime"}, {
                            data: "distributorWorkId",
                            createdCell: function (td, tdData,full) {
                                queryBtn = BtnUtils.formCopyDiDiVirturalNumberBtnCreate(workBaseUrl + "/didiVirtualNumber?type=1&didiId=" + tdData + "&orderId=" + full["orderTcId"]
                                    , "get", "复制进线人电话");
                                $(td).html([queryBtn]);
                            }
                        },
                        {
                            data: "workId",
                            createdCell: function (td, tdData, full) {
                                console.info(full)
                                if (full["status"] == 1) {
                                    queryBtn = BtnUtils.formViewBtnCreate(workBaseUrl + "/workList/HandleHistory/show?workId=" + tdData, "get", "处理记录");
                                    editBtn = BtnUtils.formBtnCreate(orderId, workBaseUrl + "/handlerWork/show?orderId=" + orderId
                                        + "&workId=" + tdData + "&orderTcId=" + full["orderTcId"] + "&source=" + full["source"] + "&dictId=" + full["dictId"] + "&didiWorkId=" + full["distributorWorkId"]
                                        + "&didiSafeWorkId=" + full["didiSafeWorkId"], workBaseUrl + "/handlerWork", "处理", redirectUrl);
                                    $(td).html([queryBtn, editBtn]);
                                } else {
                                    queryBtn = BtnUtils.formViewBtnCreate(workBaseUrl + "/workList/HandleHistory/show?workId=" + tdData, "get", "处理记录");
                                    $(td).html([queryBtn]);
                                }
                            }
                        }],
                    "ajax": {
                        "url": workBaseUrl + "/getWorkInfoList?orderId=" + orderId,
                        "async": false,
                    },
                    order: [[5, "desc"]],
                },
                ajaxSuccess: function (result) {
                    console.info(result)
                }
            });
            $("img", $WorkListTable).click(function () {
                window.open($(this).attr("src"), "_blank");
            });
        } catch (e) {
            console.error(e);
            alert("滴滴工单表初始化失败");
        }
    }


    function init() {
        try {
            var workBaseUrl = "./merchant/order/didi/work";
            var workOrderId = $("#work-list-orderId").val();
            var redirectUrl = "./merchant/order/airport/get?id=" + workOrderId;
            var $WorkListTable = $("#didiWorkInfoList").randomId();
            initWorkTable($WorkListTable, workBaseUrl, redirectUrl, workOrderId);
        } catch (e) {
            console.error(e);
            alert("页面初始化失败,请尝试 Ctrl键 + F5键 刷新浏览器页面");
        }
    };

});
