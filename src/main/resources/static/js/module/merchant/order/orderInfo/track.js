(function () {
    jQuery(document).ready(function () {
        try {
            var baseUrl = "./merchant/order";
            var $pageSign = $("#trackList").randomId();
            var id = $("#hiddOrderID").randomId().val();
            var grid = new Datatable();
            grid.init({
                src: $pageSign,
                dataTable: {
                    "columns": [{data: "id"}, {data: "content"}, {data: "operator"}, {data: "createTime"}],
                    "ajax": {
                        "url": baseUrl + "/getOrderTrackListData?orderId=" + id,
                    },
                    order: [[0, "desc"]],
                }
            });
        } catch (e) {
            console.error(e);
            alert("页面初始化失败");
        }
    });
})();