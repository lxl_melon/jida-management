jQuery(document).ready(function () {
    init();

    function initRefundInfo($orderRefundInfoList, baseUrl) {
        try {
            var grid = new Datatable();
            grid.init({
                src: $orderRefundInfoList,
                dataTable: {
                    "columns": [{data: "refundSerialId"},
                        {data: "refundFee"},{
                            data: "refundStatus"
                            ,
                            createdCell: function (td, tdData) {
                                if (tdData == '0') {
                                    $(td).html('<span class="label label-sm label-warning" style="background-color: #437cae">初始状态</span>');
                                } else if (tdData == '1') {
                                    $(td).html('<span class="label label-sm label-warning" style="background-color: #437cae">退款成功</span>');
                                } else if (tdData == '2') {
                                    $(td).html('<span class="label label-sm label-warning" style="background-color: #437cae">退款失败</span>');
                                }
                            }
                        }, {data: "createDate"}, {data: "refundReason"}],
                    "ajax": {
                        "url": baseUrl,
                        "async": false,
                    },
                    order: [[4, "desc"]],
                },
                ajaxSuccess: function (result) {
                    console.info(result)
                }
            });
        } catch (e) {
            console.error(e);
            alert("查询退款初始化失败");
        }
    }

    function init() {
        try {
            var $pageSign = $("#work_refund_query").randomId();
            var orderId = $("input[name='orderId']", $pageSign).val();
            var $orderRefundInfoList = $("#orderRefundInfoList", $pageSign).randomId();
            var csBaseUrl = "./merchant/order/didi/work/billRefundQueryData?orderId=" + orderId;

            initRefundInfo($orderRefundInfoList, csBaseUrl);
            //更改显示在 标签页 的文字
            IntelCarUtils.pageNameChange($pageSign, "退款记录");
        } catch (e) {
            console.error(e);
            alert("页面初始化失败,请尝试 Ctrl键 + F5键 刷新浏览器页面");
        }
    };

});
