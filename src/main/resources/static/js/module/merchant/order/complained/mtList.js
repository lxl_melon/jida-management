(function () {
    jQuery(document).ready(function () {
        try {

            var baseUrl = "./merchant/order";
            var $list = $("#mtOrderComplained").randomId();
            var $container = $("#mtOrderComplainedListContainer").randomId();

            var $complainedStatus = $("#complainedStatus").randomId();
            var $sourceType = $("#sourceType").randomId();
            var $complainedDatetimeChanger = $("#complained-datetime-changer").randomId();
            var $complainedDatetime1 = $("#complained-datetime-1").randomId();
            var $complainedDatetime2 = $("#complained-datetime-2").randomId();
            let $exportBtn = $("#meituan-export").randomId();

            var $complainedSearch = $("#mtcomplained-search").randomId();

            IntelCarUtils.select2($complainedStatus);
            IntelCarUtils.select2($sourceType);
            IntelCarUtils.select2($complainedDatetimeChanger);
            IntelCarUtils.dateTimeInput($complainedDatetime1);
            IntelCarUtils.dateTimeInput($complainedDatetime2);


            var grid = new Datatable();
            grid.initPage({
                src: $list,
                dataTable: {
                    'serverSide':true,
                    'server':true,
                    ordering: false,//是否允许用户排序
                    filter: false,    //去掉搜索框
                    paging: true,

                    "columns": [
                        {
                            data: "orderId",
                            createdCell: function (td, tdData) {
                                var url = IntelCarUtils.ajaxUrlGenerate(baseUrl + "/airport/get?", {"id": tdData});
                                $(td).html('<a href="' + url + '" class="ajaxify" uniqueTabCheck="false">' + tdData + '</a>');
                            }
                        },
                        {data: "transactionNo"},
                       {data: "typeTcTopic"},
                        {
                            data: "sourceType",
                            createdCell: function (td, tdData) {
                                if (tdData == 3) {
                                    $(td).html('<span class="label label-sm label-warning" style="background-color: #437cae">美团</span>');

                                } else {
                                    $(td).html('<span class="label label-sm label-warning" style="background-color: #f1c40f">秒走</span>');

                                }
                            }
                        },
                        {data: "createTime"},
                        {data: "status",
                            createdCell: function (td, tdData) {
                                if (tdData == 0) {
                                    $(td).html('<span class="label label-sm label-warning" style="background-color: #0c91e5">未处理</span>');
                                } else {
                                    $(td).html('<span class="label label-sm label-warning" style="background-color: #8e9bae">已处理</span>');
                                }
                            }}],
                    order: [[4, "desc"]],

                    "ajax": {
                        "url": baseUrl + "/work/mt/complainedData"
                    },
                }
            });
            $complainedSearch.click(function () {
                grid.submitFilter();
            });
            IntelCarUtils.pageNameChange($list, "美团问题单列表");

            //导出表格按钮监听
            $exportBtn.click(function () {
                let url = baseUrl + "/work/mt/exportFileData?";
                let tParam;
                // get all typeable inputs
                $('textarea.form-filter, select.form-filter,input.form-filter[type="checkbox"]:checked,' +
                    'input.form-filter[type="radio"]:checked, input.form-filter:not([type="radio"],[type="checkbox"])',
                    $container).each(
                    function () {
                        tParam = $(this).val();
                        tParam = 'object' == typeof tParam && tParam != null ? tParam.toString() : tParam;
                        tParam = tParam == null || tParam == "null" ? '':tParam;
                        if (tParam!='')
                            url += $(this).attr("name") + "=" + tParam + "&";
                    });
                window.open(url);
            });
        } catch (e) {
            console.error(e);
            alert("页面初始化失败");
        }
    });
})();