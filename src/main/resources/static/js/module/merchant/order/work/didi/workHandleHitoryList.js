jQuery(document).ready(function () {
    init();

    function initWorkHandleHistoryTable($orderWorkInfoListTable, baseUrl, workId) {
        try {
            var grid = new Datatable();
            grid.init({
                src: $orderWorkInfoListTable,
                dataTable: {
                    "columns": [{data: "workId"}, {data: "createTime"},
                        {
                            data: "action",
                            createdCell: function (td, tdData) {
                                if (tdData == 'close') {
                                    $(td).html('<span class="label label-sm label-warning" style="background-color: #437cae">关单</span>');
                                } else if (tdData == 'end') {
                                    $(td).html('<span class="label label-sm label-warning" style="background-color: #437cae">关单</span>');
                                } else if (tdData == 'update_order') {
                                    $(td).html('<span class="label label-sm label-warning" style="background-color: #437cae">升级到订单组</span>');
                                } else if (tdData == 'update_project') {
                                    $(td).html('<span class="label label-sm label-warning" style="background-color: #437cae">升级到项目组</span>');
                                } else if (tdData == 'update_complain') {
                                    $(td).html('<span class="label label-sm label-warning" style="background-color: #437cae">升级到投诉组</span>');
                                } else if (tdData == 'outbound') {
                                    $(td).html('<span class="label label-sm label-warning" style="background-color: #437cae">外呼</span>');
                                } else if (tdData == 'circulation') {
                                    $(td).html('<span class="label label-sm label-warning" style="background-color: #437cae">流转工单</span>');
                                } else if (tdData == 'process') {
                                    $(td).html('<span class="label label-sm label-warning" style="background-color: #437cae">继续处理</span>');
                                } else if (tdData == 'reply') {
                                    $(td).html('<span class="label label-sm label-warning" style="background-color: #437cae">回复</span>');
                                } else if (tdData == 'urgent_reply') {
                                    $(td).html('<span class="label label-sm label-warning" style="background-color: #437cae">回复加急</span>');
                                } else if (tdData == 'pre_confirm') {
                                    $(td).html('<span class="label label-sm label-warning" style="background-color: #437cae">工单录入</span>');
                                } else if (tdData == 'temp_store') {
                                    $(td).html('<span class="label label-sm label-warning" style="background-color: #437cae">工单暂存</span>');
                                } else {
                                    $(td).html('<span class="label label-sm label-warning" style="background-color: #437cae">处理中</span>');
                                }
                            }
                        }, {data: "content"}, {data: "operator"}],
                    "ajax": {
                        "url": baseUrl + "/workList/HandleHistory?workId=" + workId,
                        "async": false,
                    },
                    order: [[1, "desc"]],
                },
                ajaxSuccess: function (result) {
                    console.info(result)
                }
            });
        } catch (e) {
            console.error(e);
            alert("滴滴工单记录初始化失败");
        }
    }

    function init() {
        try {
            var csBaseUrl = "./merchant/order/didi/work";
            var $pageSign = $("#work_OrderWorkInfo").randomId();
            var workId = $("input[name='workId']", $pageSign).val();
            var $orderWorkInfoListTable = $("#orderWorkInfoList", $pageSign).randomId();
            initWorkHandleHistoryTable($orderWorkInfoListTable, csBaseUrl, workId);
            //更改显示在 标签页 的文字
            IntelCarUtils.pageNameChange($pageSign, "工单处理记录");
        } catch (e) {
            console.error(e);
            alert("页面初始化失败,请尝试 Ctrl键 + F5键 刷新浏览器页面");
        }
    };

});
