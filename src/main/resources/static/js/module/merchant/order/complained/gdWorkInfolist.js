
jQuery(document).ready(function () {
    init();

    function initRefundInfo($orderRefundInfoList, baseUrl) {
        try {
            var grid = new Datatable();
            grid.init({
                src: $orderRefundInfoList,
                dataTable: {
                    "columns": [
                        {data: "sourceType",
                            createdCell: function (td, tdData) {
                                if (tdData == 2) {
                                    sourceTypeSpan = '<span>高德</span>';
                                } else  {
                                    sourceTypeSpan = '<span>用户</span>';
                                }
                                $(td).html(sourceTypeSpan);
                            }
                        },
                        {data: "promptTime"},
                        {
                            data: "typeGd",
                            createdCell: function (td, tdData) {
                                var statusSpan = '';
                                if (tdData == 1) {
                                    statusSpan = '<span class="label label-sm label-' + 'warning' + '">支付失败</span>';
                                } else if (tdData == 2) {
                                    statusSpan = '<span class="label label-sm label-' + 'warning' + '">未乘车扣费</span>';
                                } else if (tdData == 3) {
                                    statusSpan = '<span class="label label-sm label-' + 'warning' + '">其他方式已付费</span>';
                                } else if (tdData == 4) {
                                    statusSpan = '<span class="label label-sm label-' + 'warning' + '">费用不认可</span>';
                                } else if (tdData == 5) {
                                    statusSpan = '<span class="label label-sm label-' + 'warning' + '">重复扣费</span>';
                                } else if (tdData == 6) {
                                    statusSpan = '<span class="label label-sm label-' + 'warning' + '">其他费用问题</span>';
                                } else if (tdData == 7) {
                                    statusSpan = '<span class="label label-sm label-' + 'warning' + '">投诉司机</span>';
                                } else if (tdData == 8) {
                                    statusSpan = '<span class="label label-sm label-' + 'warning' + '">物品丢失</span>';
                                } else if (tdData == 9) {
                                    statusSpan = '<span class="label label-sm label-' + 'warning' + '">取消订单</span>';
                                } else if (tdData == 10) {
                                    statusSpan = '<span class="label label-sm label-' + 'warning' + '">发票问题</span>';
                                } else if (tdData == 11) {
                                    statusSpan = '<span class="label label-sm label-' + 'warning' + '">其他投诉问题</span>';
                                } else if (tdData == 12) {
                                    statusSpan = '<span class="label label-sm label-' + 'warning' + '">客服置入</span>';
                                } else {
                                    statusSpan = '<span class="label label-sm label-' + 'danger' + '">未知状态' + tdData + '</span>';
                                }
                                $(td).html(statusSpan);
                            }
                        }, {data: "passengerFeedback"}, {data: "processResult"}
                    ],
                    "ajax": {
                        "url": baseUrl
                    },
                    order: [[2, "desc"]],
                },
                ajaxSuccess: function (result) {
                    console.info(result)
                }
            });
        } catch (e) {
            console.error(e);
            alert("查询退款初始化失败");
        }
    }

    function init() {
        try {
            var $pageSign = $("#work_gd_query").randomId();
            var orderId = $("input[name='orderId']", $pageSign).val();
            var $orderRefundInfoList = $("#gdWorkInfoList", $pageSign).randomId();
            var csBaseUrl = "./merchant/order/work/gd/getOrderWorkList?orderId=" + orderId;
            initRefundInfo($orderRefundInfoList, csBaseUrl);
            //更改显示在 标签页 的文字
            IntelCarUtils.pageNameChange($pageSign, "处理记录");
        } catch (e) {
            console.error(e);
            alert("页面初始化失败,请尝试 Ctrl键 + F5键 刷新浏览器页面");
        }
    };

});

