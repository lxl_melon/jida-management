(function () {
    jQuery(document).ready(function () {
        try {
            var baseUrl = "./merchant/order";
            var $pageSign = $("#dispatchList").randomId();
            var id = $("#hiddOrderID").randomId().val();
            var grid = new Datatable();
            grid.init({
                src: $pageSign,
                dataTable: {
                    "columns": [{data: "id"}, {data: "times"}, {data: "createTime"}, {data: "name"}
                    , {data: "alias"}
                    ,{ 
                    	data: "available" , createdCell: function (td, tdData) {
                        var statusSpan = '';
                        if (tdData == 0) {
                            statusSpan = '<span class="label label-sm label-' + 'warning' + '">否</span>';
                        } else if (tdData == 1) {
                            statusSpan = '<span class="label label-sm label-' + 'success' + '">是</span>';
                        }
                        $(td).html(statusSpan);
                    	}
                     },{data: "accept" , createdCell: function (td, tdData) {
                         var statusSpan = '';
                         if (tdData == 0) {
                             statusSpan = '<span class="label label-sm label-' + 'warning' + '">否</span>';
                         } else if (tdData == 1) {
                             statusSpan = '<span class="label label-sm label-' + 'success' + '">是</span>';
                         }
                         $(td).html(statusSpan);
                     	}
                      }, {data: "distance"}],
                    "ajax": {
                        "url": baseUrl + "/getOrderDispatchListData?orderId=" + id,
                    },
                    order: [[0, "desc"]],
                }
            });
        } catch (e) {
            console.error(e);
            alert("页面初始化失败");
        }
    });
})();