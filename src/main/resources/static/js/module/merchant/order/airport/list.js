(function () {
    jQuery(document).ready(function () {
        try {

            var baseUrl = "./merchant/order";
            var $list = $("#merchantOrderAirport").randomId();

            var $idInput = $("#order-airport-id-input").randomId();
            var $idRemind = $("#order-airport-id-code").randomId();
            
            var $distributionIdInput = $("#distribution_order_id-input").randomId();

            var $cellphoneInput = $("#order-airport-cellphone-input").randomId();
            var $cellphoneRemind = $("#order-airport-cellphone-code").randomId();

            var $productIdSelector = $("#airport-productId-select2").randomId();
            var $statusSelector = $("#airport-status-select2").randomId();
            var $allStatusBtn = $("#airport-status-selectAll").randomId();

            var $time1 = $("#airport-datetime-1").randomId();
            var $time2 = $("#airport-datetime-2").randomId();
            var $timeChangerSelector = $("#airport-datetime-changer").randomId();

            var $cellphoneChange = $("#cellphoneChange").randomId();

            var $searchBtn = $("#airport-search").randomId();

            var actionCode = $("input[pname='actionCode']", $list).val();
            var actionName = $("input[pname='actionName']", $list).val();
            var serviceCity = $("#serviceCity").randomId();
            var riskStatus = $("#riskStatus-select2").randomId();

            var $airportExport = $("#airport-export").randomId();
            var $source = $("#source").randomId();

            IntelCarUtils.select2(riskStatus, "请选择风控状态");

            IntelCarUtils.select2(serviceCity, "");

            var grid = new Datatable();
            grid.initPage({
                src: $list,
                dataTable: {
                    "columns": [
                        {
                            data: "orderId",
                            createdCell: function (td, tdData) {
                                var url = IntelCarUtils.ajaxUrlGenerate(baseUrl + "/airport/get?", {"id": tdData});
                                $(td).html('<a href="' + url + '" class="ajaxify" uniqueTabCheck="false">' + tdData + '</a>');
                            }
                        }, {data: "orderTcId"}, {data: "productName"}, {data: "passengerName"},
                        {data: "passengerCellphone"}, {data: "driverCellphone"}, {data: "startCityName"}, {data: "carType"}, {data: "merchant"},
                        {data: "createTime"}, {data: "useTime"},
                        {
                            data: "amountType", createdCell: function (td, tdData) {
                                var statusSpan = '';
                                if (tdData == 0) {
                                    statusSpan = '<span class="label label-sm label-' + 'warning' + '">一口价</span>';
                                } else if (tdData == 1) {
                                    statusSpan = '<span class="label label-sm label-' + 'success' + '">实时报价</span>';
                                } else {
                                    statusSpan = '<span class="label label-sm label-' + 'danger' + '">未知状态' + tdData + '</span>';
                                }
                                $(td).html(statusSpan);
                            }
                        },
                        {
                            data: "status", createdCell: function (td, tdData) {
                                var statusSpan = '';
                                if (tdData == 0) {
                                    statusSpan = '<span class="label label-sm label-' + 'warning' + '">创建订单</span>';
                                } else if (tdData == 10) {
                                    statusSpan = '<span class="label label-sm label-' + 'success' + '">订单推送成功</span>';
                                } else if (tdData == 15) {
                                    statusSpan = '<span class="label label-sm label-' + 'success' + '">供应商接单成功</span>';
                                } else if (tdData == 20) {
                                    statusSpan = '<span class="label label-sm label-' + 'success' + '">接单成功</span>';
                                } else if (tdData == 50) {
                                    statusSpan = '<span class="label label-sm label-' + 'success' + '">司机出发</span>';
                                } else if (tdData == 100) {
                                    statusSpan = '<span class="label label-sm label-' + 'success' + '">司机到达</span>';
                                } else if (tdData == 200) {
                                    statusSpan = '<span class="label label-sm label-' + 'success' + '">开始服务</span>';
                                } else if (tdData == 300) {
                                    statusSpan = '<span class="label label-sm label-' + 'success' + '">订单完成</span>';
                                } else if (tdData == 400) {
                                    statusSpan = '<span class="label label-sm label-' + 'success' + '">已结算</span>';
                                } else if (tdData == 500) {
                                    statusSpan = '<span class="label label-sm label-' + 'success' + '">用户已支付</span>';
                                } else if (tdData == 1000) {
                                    statusSpan = '<span class="label label-sm label-' + 'success' + '">订单取消</span>';
                                } else {
                                    statusSpan = '<span class="label label-sm label-' + 'danger' + '">未知状态' + tdData + '</span>';
                                }

                                $(td).html(statusSpan);
                            }
                        }],
                    "ajax": {
                        "url": baseUrl + "/airportData?actionCode=" + actionCode,
                    },
                    order: [[9, "desc"]],
                }
            });
            $idInput.focus(function () {
                $idRemind.show(200);
            });
            $idInput.blur(function () {
                $idRemind.hide(200);
            });

            $distributionIdInput.focus(function () {
                $idRemind.show(200);
            });
            $distributionIdInput.blur(function () {
                $idRemind.hide(200);
            });
            
            $cellphoneInput.focus(function () {
                $cellphoneRemind.show(200);
            });
            $cellphoneInput.blur(function () {
                $cellphoneRemind.hide(200);
            });

            IntelCarUtils.select2($productIdSelector, "请选择产品类型");
            IntelCarUtils.select2($statusSelector, "请选择订单状态");
            IntelCarUtils.select2($source, "请选择渠道来源");

            IntelCarUtils.dateTimeInput($time1);
            IntelCarUtils.dateTimeInput($time2);

            IntelCarUtils.select2NoQuery($timeChangerSelector, "查询时间条件更改");
            $timeChangerSelector.change(function () {
                var prefix = $(this).val();
                $time1.attr("name", prefix + $time1.attr("base"));
                $time2.attr("name", prefix + $time2.attr("base"));
            });

            IntelCarUtils.select2NoQuery($cellphoneChange, "查询时间条件更改");
            $cellphoneChange.change(function () {
                var prefix = $(this).val();
                $cellphoneInput.attr("name", prefix + $cellphoneInput.attr("data-base"));
            });

            $allStatusBtn.click(function () {
                if ($(this).attr("select") == "true") {
                    $("option", $statusSelector).attr("selected", "selected");
                    $statusSelector.trigger("change");
                    $(this).attr("select", "false");
                    $(this).html("全不选");
                    $(this).trigger("change");
                } else {
                    $("option", $statusSelector).removeAttr("selected");
                    $statusSelector.trigger("change");
                    $(this).attr("select", "true");
                    $(this).html("全 选");
                    $(this).trigger("change");
                }
            });

            $airportExport.click(function () {
                var currentPage = $(".paginate_button.active>a").text() - 1;
                var currentStart = currentPage * 10;
                var url = baseUrl + "/export?actionCode=" + actionCode + "&start=" + currentStart + "&";
                var tableContainer = ".table-container";
                var tParam;
                // get all typeable inputs
                $('textarea.form-filter, select.form-filter,input.form-filter[type="checkbox"]:checked,' +
                    'input.form-filter[type="radio"]:checked, input.form-filter:not([type="radio"],[type="checkbox"])',
                    tableContainer).each(
                    function () {
                        tParam = $(this).val();
                        tParam = 'object' == typeof tParam && tParam != null ? tParam.toString() : tParam;
                        url += $(this).attr("name") + "=" + tParam + "&";
                    });
                window.open(url);
            });

            $searchBtn.click(function () {
                grid.submitFilter();
            });
            IntelCarUtils.pageNameChange($list, actionName);
        } catch (e) {
            console.error(e);
            alert("页面初始化失败");
        }
    });
})();