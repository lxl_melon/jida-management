(function () {
    jQuery(document).ready(function () {
        try {

            var baseUrl = "./merchant/order";
            var $list = $("#merchantOrderComplained").randomId();

            var $complainedStatus = $("#complainedStatus").randomId();
            var $complainedStatusSelect2 = $("#complained-status-select2").randomId();
            var $sourceType = $("#sourceType").randomId();
            var $complainedDatetimeChanger = $("#complained-datetime-changer").randomId();
            var $complainedDatetime1 = $("#complained-datetime-1").randomId();
            var $complainedDatetime2 = $("#complained-datetime-2").randomId();

            var $complainedSearch = $("#complained-search").randomId();

            IntelCarUtils.select2($complainedStatus);
            IntelCarUtils.select2($complainedStatusSelect2, "请选择问题类型");
            IntelCarUtils.select2($sourceType);
            IntelCarUtils.select2($complainedDatetimeChanger);
            IntelCarUtils.dateTimeInput($complainedDatetime1);
            IntelCarUtils.dateTimeInput($complainedDatetime2);


            var grid = new Datatable();
            grid.initPage({
                src: $list,
                dataTable: {
                    "columns": [
                        {
                            data: "orderId",
                            createdCell: function (td, tdData) {
                                var url = IntelCarUtils.ajaxUrlGenerate(baseUrl + "/airport/get?", {"id": tdData});
                                $(td).html('<a href="' + url + '" class="ajaxify" uniqueTabCheck="false">' + tdData + '</a>');
                            }
                        },
                        {data: "passengerCellphone"},
                        {data: "sourceType",
                            createdCell: function (td, tdData,all) {
                                queryBtn = BtnUtils.formViewBtnCreate(baseUrl + "/work/gd/toWorkInfoList?orderId=" + all["orderId"], "get", "查看");
                                $(td).html([queryBtn]);
                            }
                        },
                        {
                            data: "typeGd",
                            createdCell: function (td, tdData) {
                                var statusSpan = '';
                                if (tdData == 1) {
                                    statusSpan = '<span class="label label-sm label-' + 'warning' + '">支付失败</span>';
                                } else if (tdData == 2) {
                                    statusSpan = '<span class="label label-sm label-' + 'warning' + '">未乘车扣费</span>';
                                } else if (tdData == 3) {
                                    statusSpan = '<span class="label label-sm label-' + 'warning' + '">其他方式已付费</span>';
                                } else if (tdData == 4) {
                                    statusSpan = '<span class="label label-sm label-' + 'warning' + '">费用不认可</span>';
                                } else if (tdData == 5) {
                                    statusSpan = '<span class="label label-sm label-' + 'warning' + '">重复扣费</span>';
                                } else if (tdData == 6) {
                                    statusSpan = '<span class="label label-sm label-' + 'warning' + '">其他费用问题</span>';
                                } else if (tdData == 7) {
                                    statusSpan = '<span class="label label-sm label-' + 'warning' + '">投诉司机</span>';
                                } else if (tdData == 8) {
                                    statusSpan = '<span class="label label-sm label-' + 'warning' + '">物品丢失</span>';
                                } else if (tdData == 9) {
                                    statusSpan = '<span class="label label-sm label-' + 'warning' + '">取消订单</span>';
                                } else if (tdData == 10) {
                                    statusSpan = '<span class="label label-sm label-' + 'warning' + '">发票问题</span>';
                                } else if (tdData == 11) {
                                    statusSpan = '<span class="label label-sm label-' + 'warning' + '">其他投诉问题</span>';
                                } else if (tdData == 12) {
                                    statusSpan = '<span class="label label-sm label-' + 'warning' + '">客服置入</span>';
                                } else {
                                    statusSpan = '<span class="label label-sm label-' + 'danger' + '">未知状态' + tdData + '</span>';
                                }
                                $(td).html(statusSpan);
                            }
                        },
                        {data: "promptTime"}, {data: "timeRemaining"},
                        {data: "status"}
                    ],
                    "ajax": {
                        "url": baseUrl + "/complainedData"
                    },
                    order: [[4, "desc"]],
                }
            });

            $complainedSearch.click(function () {
                grid.submitFilter();
            });
            IntelCarUtils.pageNameChange($list, "投诉订单列表");
        } catch (e) {
            console.error(e);
            alert("页面初始化失败");
        }
    });
})();
// {
//     data: "orderStatus",
//         createdCell: function (td, tdData) {
//     var statusSpan = '';
//     if (tdData == 0) {
//         statusSpan = '<span class="label label-sm label-' + 'warning' + '">创建订单</span>';
//     } else if (tdData == 10) {
//         statusSpan = '<span class="label label-sm label-' + 'success' + '">订单推送成功</span>';
//     } else if (tdData == 15) {
//         statusSpan = '<span class="label label-sm label-' + 'success' + '">供应商接单成功</span>';
//     } else if (tdData == 20) {
//         statusSpan = '<span class="label label-sm label-' + 'success' + '">接单成功</span>';
//     } else if (tdData == 50) {
//         statusSpan = '<span class="label label-sm label-' + 'success' + '">司机出发</span>';
//     } else if (tdData == 100) {
//         statusSpan = '<span class="label label-sm label-' + 'success' + '">司机到达</span>';
//     } else if (tdData == 200) {
//         statusSpan = '<span class="label label-sm label-' + 'success' + '">开始服务</span>';
//     } else if (tdData == 300) {
//         statusSpan = '<span class="label label-sm label-' + 'success' + '">订单完成</span>';
//     } else if (tdData == 400) {
//         statusSpan = '<span class="label label-sm label-' + 'success' + '">已结算</span>';
//     } else if (tdData == 500) {
//         statusSpan = '<span class="label label-sm label-' + 'success' + '">用户已支付</span>';
//     } else if (tdData == 1000) {
//         statusSpan = '<span class="label label-sm label-' + 'success' + '">订单取消</span>';
//     } else {
//         statusSpan = '<span class="label label-sm label-' + 'danger' + '">未知状态' + tdData + '</span>';
//     }
//     $(td).html(statusSpan);
// }
// },
