/**
 * 车型库
 */
(function () {
    jQuery(document).ready(function () {
        try {
        	var resultId = $("#resultList").val();
        	if(resultId != 0){
        		$(".ui-dialog-button").hide();
        		$("#resultList").attr("disabled",true);
        		$("#content").attr("disabled",true);
        	}
        	if(resultId == 2){
        		$("#contentDiv").show();
        	}else{
        		$("#contentDiv").hide();
        	}
        	
        	$("#resultList").click(function(){        		
        		if($(this).val() == 2){
            		$("#contentDiv").show();
            	}else{
            		$("#contentDiv").hide();
            	}
        	});
        	
            var $form = $("#editNoCarArrivedStatus-form").randomId();
            IntelCarUtils.validate($form, {
            	content: {required: true,minlength:5, maxlength:32}
            });
            IntelCarUtils.select2($("#resultList", $form), "请选择处理结果");
        } catch (e) {
            console.error(e);
            alert("页面初始化失败");
        }
    });
})();