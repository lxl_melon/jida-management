/**
 * 行程录音
 */
(function () {
    jQuery(document).ready(function () {
        try {
            var grid = new Datatable();
            var baseUrl = "./merchant/order";
            var $list = $("#recording-list").randomId();
            var $searchBtn = $("#btn-search").randomId();

            grid.initPage({
                src: $list,
                dataTable: {
                    "columns": [
                        {data: "id"}, {data: "orderId"}, {data: "name"},  {data: "createTime"},  {data: "size"},
                        {
                            data: "url", createdCell: function (td, tdData) {
                                var statusSpan = '';
                                statusSpan = '<span class="label label-sm"><a href="'+tdData+'">下载</a></span>';
                                $(td).html(statusSpan);
                            }
                        }
                    ],
                    "ajax": {
                        "url": baseUrl + "/recordingData",
                    },
                    order: [0, "desc"],
                }
            });

            $searchBtn.click(function () {
                grid.submitFilter();
            });
            
            IntelCarUtils.pageNameChange($list, "行程录音列表");

        } catch (e) {
            console.error(e);
            alert("页面初始化失败");
        }
    });
})();