(function () {
    jQuery(document).ready(function () {
        try {
            var baseUrl = "./merchant/order";
            var $pageSign = $("#punishInfoList").randomId();
            var orderId = $("#hiddOrderID").randomId().val();
            var grid = new Datatable();
            grid.init({
                src: $pageSign,
                dataTable: {
                    "columns": [{data: "feeYuan"}, {data: "reason"}, {data: "operator"}, {data: "createTime"},
                        {
                            data: "id",
                            createdCell: function (td, tdData, full) {
                                var statusBtn = (full["status"]);
                                if ((full["status"]) == 0) {
                                    statusBtn = BtnUtils.formBtnCreate(tdData, baseUrl + "/cancelPunish?orderId=" + full["orderId"] + "&id=" + full["id"], baseUrl + "/cancelPunishData?orderId=" + full["orderId"] + "&id=" + full["id"], "撤销", baseUrl + "/airport/get?id=" + full["orderId"], "green");
                                } else {
                                    statusBtn = '<span class="label label-default">已撤销</span>';
                                }
                                $(td).html(statusBtn);
                            }
                        }, {data: "cancelReason"}],
                    "ajax": {
                        "url": baseUrl + "/getPunishInfoListData?orderId=" + orderId,
                    },
                    order: [[3, "desc"]],
                }
            });
        } catch (e) {
            console.error(e);
            alert("页面初始化失败");
        }
    });
})();