(function () {
    jQuery(document).ready(function () {
        try {

            var baseUrl = "./merchant/order";
            var $list = $("#merchantOrderNoCarArrived").randomId();

            var $noCarArrivedStatus = $("#noCarArrivedStatus").randomId();
            var $noCarArrivedDatetimeChanger = $("#noCarArrived-datetime-changer").randomId();
            var $noCarArrivedDatetime1 = $("#noCarArrived-datetime-1").randomId();
            var $noCarArrivedDatetime2 = $("#noCarArrived-datetime-2").randomId();

            var $noCarArrivedSearch = $("#noCarArrived-search").randomId();

            IntelCarUtils.select2($noCarArrivedStatus);         
            IntelCarUtils.select2($noCarArrivedDatetimeChanger);
            IntelCarUtils.dateTimeInput($noCarArrivedDatetime1);
            IntelCarUtils.dateTimeInput($noCarArrivedDatetime2);


            var grid = new Datatable();
            grid.initPage({
                src: $list,
                dataTable: {
                    "columns": [
                        {data: "id"},
                        {
                            data: "orderId",
                            createdCell: function (td, tdData) {
                                var url = IntelCarUtils.ajaxUrlGenerate(baseUrl + "/airport/get?", {"id": tdData});
                                $(td).html('<a href="' + url + '" class="ajaxify" uniqueTabCheck="false">' + tdData + '</a>');
                            }
                        }, 
                        {data: "problemType", createdCell: function (td, tdData) {
	                            var statusSpan = '';
	                            if (tdData == 1) {
	                                statusSpan = '<span class="label label-sm label-' + 'warning' + '">高德接送机到场无车</span>';
	                            }
	                            $(td).html(statusSpan);
                        	}
                        },
                        {data: "productType",
                            createdCell: function (td, tdData, full) {
                                var statusSpan = '';
                                var statusSpanText = "";
                                if (tdData == 11) {
                                    statusSpanText = "预约专车";
                                } else if (tdData == 12) {
                                    statusSpanText = "接机";
                                } else if (tdData == 13) {
                                    statusSpanText = "送机";
                                } else if (tdData == 14) {
                                    statusSpanText = "接站";
                                } else if (tdData == 15) {
                                    statusSpanText = "送站";
                                } else if (tdData == 19) {
                                    statusSpanText = "即时专车";
                                } else if (tdData == 24) {
                                    statusSpanText = "即时出租车";
                                } else if (tdData == 65) {
                                    statusSpanText = "预约出租车";
                                }
                                statusSpan = '<span class="label label-sm label-' + 'success' + '">' + statusSpanText + '</span>';
                                $(td).html(statusSpan);
                            }
                        },
                        {data: "passengerCellphone"}, {data: "useTime"},
                        {
                            data: "orderStatus",
                            createdCell: function (td, tdData) {
                                var statusSpan = '';
                                if (tdData == 0) {
                                    statusSpan = '<span class="label label-sm label-' + 'warning' + '">创建订单</span>';
                                } else if (tdData == 10) {
                                    statusSpan = '<span class="label label-sm label-' + 'success' + '">订单推送成功</span>';
                                } else if (tdData == 15) {
                                    statusSpan = '<span class="label label-sm label-' + 'success' + '">供应商接单成功</span>';
                                } else if (tdData == 20) {
                                    statusSpan = '<span class="label label-sm label-' + 'success' + '">接单成功</span>';
                                } else if (tdData == 50) {
                                    statusSpan = '<span class="label label-sm label-' + 'success' + '">司机出发</span>';
                                } else if (tdData == 100) {
                                    statusSpan = '<span class="label label-sm label-' + 'success' + '">司机到达</span>';
                                } else if (tdData == 200) {
                                    statusSpan = '<span class="label label-sm label-' + 'success' + '">开始服务</span>';
                                } else if (tdData == 300) {
                                    statusSpan = '<span class="label label-sm label-' + 'success' + '">订单完成</span>';
                                } else if (tdData == 400) {
                                    statusSpan = '<span class="label label-sm label-' + 'success' + '">已结算</span>';
                                } else if (tdData == 500) {
                                    statusSpan = '<span class="label label-sm label-' + 'success' + '">用户已支付</span>';
                                } else if (tdData == 1000) {
                                    statusSpan = '<span class="label label-sm label-' + 'success' + '">订单取消</span>';
                                } else {
                                    statusSpan = '<span class="label label-sm label-' + 'danger' + '">未知状态' + tdData + '</span>';
                                }
                                $(td).html(statusSpan);
                            }
                        },                       
                        {data: "sourceType",
                        	createdCell: function (td, tdData, full) {                        
	                            var statusSpan = '';
	                            var statusSpanText = "";
	                            if (tdData == 1) {
	                                statusSpanText = "司机";
	                            } else if (tdData == 2) {
	                                statusSpanText = "平台";
	                            }
	                            statusSpan = '<span class="label label-sm label-' + 'success' + '">' + statusSpanText + '</span>';
	                            $(td).html(statusSpan);
	                        }
                        }, {data: "promptTime"}, {data: "processedTime"},
                        {
                            data: "status",
                        	createdCell: function (td, tdData, full) {                        
	                            var statusSpan = '';
	                            if (tdData == 0) {
                                    statusSpan = '<span class="label label-sm label-' + 'warning' + '">未处理</span>';
                                } else if (tdData == 1) {
                                    statusSpan = '<span class="label label-sm label-' + 'success' + '">已处理</span>';
                                }
	                            $(td).html(statusSpan);
	                        }
                        },
                        {
                            data: "id", createdCell: function (td, tdData, full) {
                            	var editBtn = (full["status"] != 1)
                            	? BtnUtils.formBtnCreate(tdData, baseUrl + "/editNoCarArrivedStatus",
                                                baseUrl + "/editNoCarArrivedStatusData?id="+tdData, "编辑", baseUrl + "/noCarArrived")
                            	:  BtnUtils.formBtnCreate(tdData, baseUrl + "/editNoCarArrivedStatus",
                                        baseUrl + "/editNoCarArrivedStatusData?id="+tdData, "查看", baseUrl + "/noCarArrived");                            	
                                $(td).html([editBtn]);
                            }
                        }],
                    "ajax": {
                        "url": baseUrl + "/noCarArrivedData"
                    },
                    order: [[1, "desc"]],
                }
            });

            $noCarArrivedSearch.click(function () {
                grid.submitFilter();
            });
            IntelCarUtils.pageNameChange($list, "到场无车订单列表");
        } catch (e) {
            console.error(e);
            alert("页面初始化失败");
        }
    });
})();