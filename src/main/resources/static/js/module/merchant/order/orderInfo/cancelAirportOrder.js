/**
 * 车型库
 */
(function () {
    jQuery(document).ready(function () {
        try {        	
            var $form = $("#cancelAirportOrder-form").randomId();
            IntelCarUtils.validate($form, {
            	reassign: {required: true}
            });
            IntelCarUtils.select2($("#resultList", $form), "请选择处理结果");
        } catch (e) {
            console.error(e);
            alert("页面初始化失败");
        }
    });
})();