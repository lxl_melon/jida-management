(function () {
    jQuery(document).ready(function () {
        try {
            var baseUrl = "./merchant/order";
            var $pageSign = $("#mtWorkInfoList").randomId();
            var orderId = $("#hiddOrderID").randomId().val();
            var grid = new Datatable();
            grid.init({
                src: $pageSign,
                dataTable: {
                    "columns": [{data: "id"}, {
                        data: "refundType",
                        createdCell: function (td, tdData, all) {
                            if (tdData == '1') {
                                $(td).html('<span class="label label-sm label-warning" style="background-color: #89C4F4">部分退款</span>');
                            } else if (tdData == '2') {
                                $(td).html('<span class="label label-sm label-warning" style="background-color: #89C4F4">全额退款</span>');
                            } else {
                                $(td).html('<span class="label label-sm label-warning" style="background-color: #adadad">不涉及退款</span>');
                            }
                        }
                    }, {
                        data: "refundAmount",
                        createdCell: function (td, tdData) {
                            $(td).html('<span>'+parseFloat(tdData)/100+'</span>');
                        }
                    }, {data: "record"}, {data: "operator"}, {data: "createTime"}],
                    "ajax": {
                        "url": baseUrl + "/getMtWorkInfoListData?orderId=" + orderId,
                    },
                    order: [[5, "desc"]],
                }
            });
        } catch (e) {
            console.error(e);
            alert("页面初始化失败");
        }
    });
})();