(function () {
    jQuery(document).ready(function () {
        try {
            var baseUrl = "./merchant/order";
            var $pageSign = $("#merchant-order-get-ab").randomId();
            var id = $("input[pname='orderId']", $pageSign).val();
            var priceLoad = $("input[pname='priceLoad']", $pageSign).val();
            var orderTcId = $("input[pname='orderTcId']", $pageSign).val();
            var distributionOrderId = $("input[pname='distributionOrderId']", $pageSign).val();
            var cityId = $("input[pname='cityId']", $pageSign).val();
            var $pageName = $("input[pname='pageName']", $pageSign).val();
            var $abnormalBtn = $("#abnormalBtn").randomId();
            var $invoiceBtn = $("#invoiceBtn").randomId();
            var $trackBtn = $("#trackBtn").randomId();
            var $arriveBtn = $("#arriveBtn").randomId();
            var $actualBtn = $("#actualBtn").randomId();
            var $optimizationBtn = $("#optimizationBtn").randomId();
            var $cancelBtn = $("#cancelBtn").randomId();
            var $cancelAirportBtn = $("#cancelAirportBtn").randomId();
            var $stopBtn = $("#stopBtn").randomId();
            var redirectUrl = baseUrl + "/airport/get?id=" + id;
            var $orderPunishBtn = $("#orderPunishBtn").randomId();
            var $nonAbnormalBtn = $("#nonAbnormalBtn").randomId();
            var $finishOkBtn = $("#finishOkBtn").randomId();
            var $forceBalanceBtn = $("#forceBalanceBtn").randomId();
            var $forceBalanceBtn1 = $("#forceBalanceBtn1").randomId();
            var $riskBtn = $("#riskBtn").randomId();
            var $makeComplainBtn = $("#makeComplainBtn").randomId();
            var $processComplainedBtn = $("#processComplainedBtn").randomId();
            var $makeMtComplainBtn = $("#makeMtComplainBtn").randomId();
            var $processMtComplainedBtn = $("#processMtComplainedBtn").randomId();
            var $orderRiskDeduct = $("#orderRiskDeduct").randomId();
            var $orderRiskCouponDeduct = $("#orderRiskCouponDeduct").randomId();
            var $cancelOrderRiskDeduct = $("#cancelOrderRiskDeduct").randomId();
            var $btnPolicyDownload = $("#btnPolicyDownload").randomId();
            var $policyDownloadUrl = $("#policyDownloadUrl").val();
            //滴滴分销单
            //获取滴滴虚拟号，根据乘客手机号
            var $didiPassengerPhoneBtn = $("#didi-passenger-phone-copy-btn").randomId();
            //置为滴滴问题单
            var $distributorDiDiproblem = $("#distributorDiDiBtn").randomId();
            //滴滴取消订单
            var $didiCancelBtn = $("#didiCancelBtn", $pageSign).randomId();
            //滴滴改价
            var $changePriceBtn = $("#changePriceBtn", $pageSign).randomId();
            //滴滴退款
            var $didiRefundPriceBtn = $("#didiRefundPriceBtn", $pageSign).randomId();
            //滴滴退款查询
            var $didiRefundQueryBtn = $("#didiRefundQueryBtn", $pageSign).randomId();
            //滴滴推送订单结算状态
            var $pushOrderStatusToDiDi = $("#pushOrderStatusToDiDi", $pageSign).randomId();
            //滴滴获取用户支付状态
            var $pullPayStatusFromDiDi = $("#pullPayStatusFromDiDi", $pageSign).randomId();

            //置为异常
            $abnormalBtn.click(function () {
                BtnUtils.dialogConfirm("你要将改订单置为异常订单吗?", "提醒", function () {
                    IntelCarUtils.ajax(baseUrl + "/setAbnormalOrder", {id: id}, function (result) {
                        if (result.status == "200") {
                            ToastrUtils.getSuccess("操作成功", result.message);
                        } else {
                            ToastrUtils.getError("操作失败", result.message);
                        }
                        IntelCarUtils.initAjax(redirectUrl);
                    });
                });
            });


            //置为非异常订单
            $nonAbnormalBtn.click(function () {
                BtnUtils.dialogConfirm("你要将改订单置为非异常订单吗?", "提醒", function () {
                    IntelCarUtils.ajax(baseUrl + "/setNonAbnormalOrder", {id: id}, function (result) {
                        if (result.status == "200") {
                            ToastrUtils.getSuccess("操作成功", result.message);
                        } else {
                            ToastrUtils.getSuccess("操作失败", result.message);
                        }
                        IntelCarUtils.initAjax(redirectUrl);
                    });
                });
            });

            //异常订单惩罚
            $orderPunishBtn.click(function () {
                var option = {
                    data: {id: id},
                    url: baseUrl + "/orderPunish?orderId="+id,
                    formaction: baseUrl + "/punishData?orderId=" + id,
                    title: "异常处罚",
                    aftersuccess: function (data) {
                        IntelCarUtils.initAjax(redirectUrl);
                    },
                }
                BtnUtils.formLoad(option);
            });

            //补开发票
            $invoiceBtn.click(function () {
                var option = {
                    data: {id: id},
                    url: baseUrl + "/invoice?orderId="+id,
                    formaction: baseUrl + "/newInvoice?orderId=" + id,
                    title: "开发票",
                    aftersuccess: function (data) {
                        IntelCarUtils.initAjax(redirectUrl);
                    },
                }
                BtnUtils.formLoad(option);
            });


            //一口价直接结束订单
            $finishOkBtn.click(function () {
                BtnUtils.dialogConfirm("你要确定要结束该订单吗?", "提醒", function () {
                    IntelCarUtils.ajax(baseUrl + "/finishOkOrder", {orderId: id}, function (result) {
                        if (result.status == "200") {
                            ToastrUtils.getSuccess("操作成功", result.message);
                        } else {
                            ToastrUtils.getError("操作失败", result.message);
                        }
                        IntelCarUtils.initAjax(redirectUrl);
                    });
                });
                BtnUtils.formLoad(option);
            });

            //实时结束订单
            $stopBtn.click(function () {
                var option = {
                    data: {orderId: id},
                    url: baseUrl + "/finishOrder",
                    formaction: baseUrl + "/finishOrderData?orderId=" + id,
                    title: "结束订单",
                    aftersuccess: function () {
                        IntelCarUtils.initAjax(redirectUrl);
                    },
                }
                BtnUtils.formLoad(option);
            });

            //确认订单
            $forceBalanceBtn.click(function () {
                var option = {
                    data: {orderId: id},
                    url: baseUrl + "/confirmOrder",
                    formaction: baseUrl + "/confirmOrderData?orderId=" + id,
                    title: "确认订单",
                    aftersuccess: function () {
                        IntelCarUtils.initAjax(redirectUrl);
                    },
                }
                BtnUtils.formLoad(option);
            });

            //管理员确认订单
            $forceBalanceBtn1.click(function () {
                var option = {
                    data: {orderId: id},
                    url: baseUrl + "/adminConfirmOrder",
                    formaction: baseUrl + "/adminConfirmOrderData?orderId=" + id,
                    title: "管理员确认订单",
                    aftersuccess: function () {
                        IntelCarUtils.initAjax(redirectUrl);
                    },
                }
                BtnUtils.formLoad(option);
            });

            //取消订单
            $cancelBtn.click(function () {
                BtnUtils.dialogConfirm("你要取消该订单吗?", "提醒", function () {
                    IntelCarUtils.ajax(baseUrl + "/cancelOrderData", {id: id}, function (result) {
                        if (result.status == "200") {
                            ToastrUtils.getSuccess("操作成功", result.message);
                        } else {
                            ToastrUtils.getSuccess("操作失败", result.message);
                        }
                        IntelCarUtils.initAjax(redirectUrl);
                    });
                });
            });

            //接送机取消订单
            $cancelAirportBtn.click(function () {
                var option = {
                    data: {id: id},
                    url: baseUrl + "/cancelAirportOrder",
                    formaction: baseUrl + "/cancelAirportOrderData?id=" + id,
                    title: "取消订单",
                    aftersuccess: function () {
                        IntelCarUtils.initAjax(redirectUrl);
                    },
                }
                BtnUtils.formLoad(option);
            });

            //跟进记录
            $trackBtn.click(function () {
                var option = {
                    data: {id: id},
                    url: baseUrl + "/trackSubmit",
                    formaction: baseUrl + "/trackData?id=" + id,
                    title: "跟进记录",
                    aftersuccess: function () {
                        IntelCarUtils.initAjax(redirectUrl);
                    },
                }
                BtnUtils.formLoad(option);
            });
            
            //到达上车地点
            $arriveBtn.click(function () {
                var option = {
                    data: {id: id},
                    url: baseUrl + "/arriveSubmit",
                    formaction: baseUrl + "/arriveData?id=" + id,
                    title: "到达上车地点",
                    aftersuccess: function () {
                        IntelCarUtils.initAjax(redirectUrl);
                    },
                }
                BtnUtils.formLoad(option);
            });

            //实际行驶路线
            $actualBtn.click(function () {
                window.open(baseUrl + "/router/actual?orderId=" + id, "_blank");
            });

            //优化行驶路线
            $optimizationBtn.click(function () {
                window.open(baseUrl + "/router/optimization?orderId=" + id, "_blank");
            });

            //消除系统自动风控
            $riskBtn.click(function () {
                var option = {
                    data: {id: id},
                    url: baseUrl + "/riskSubmit",
                    formaction: baseUrl + "/riskData?id=" + id,
                    title: "消除系统自动风控",
                    aftersuccess: function () {
                        IntelCarUtils.initAjax(redirectUrl);
                    },
                }
                BtnUtils.formLoad(option);
            });

            // 置为高德问题单
            $makeComplainBtn.click(function () {
                BtnUtils.dialogConfirm("你要将订单置为问题单吗?", "提醒", function () {
                    IntelCarUtils.ajax(baseUrl + "/make-complained", {id: id}, function (result) {
                        if (result.status == "200") {
                            ToastrUtils.getSuccess("操作成功", result.message);
                        } else {
                            ToastrUtils.getError("操作失败", result.message);
                        }
                        IntelCarUtils.initAjax(redirectUrl);
                    });
                });
            });

            // 处理高德问题单
            $processComplainedBtn.click(function () {
                var option = {
                    data: {orderId: id},
                    url: baseUrl + "/complained-process",
                    formaction: baseUrl + "/complained-process/data?orderId=" + id,
                    title: "确认订单",
                    aftersuccess: function () {
                        var option = {
                            data: {id: id},
                            url: baseUrl + "/orderPunish",
                            formaction: baseUrl + "/punishData?orderId=" + id,
                            title: "异常处罚",
                            aftersuccess: function (data) {
                                IntelCarUtils.initAjax(redirectUrl);
                            },
                        }
                        BtnUtils.formLoad(option);
                    },
                }
                BtnUtils.formLoad(option);
            });
            $btnPolicyDownload.click(function () {
                window.open($policyDownloadUrl);
            });

            // 置为美团问题单
            $makeMtComplainBtn.click(function () {
                var option = {
                    data: {id: id},
                    url: baseUrl + "/make-Mtcomplained",
                    formaction: baseUrl + "/push-Mtcomplained",
                    title: "置为美团问题单",
                    aftersuccess: function (data) {
                        IntelCarUtils.initAjax(redirectUrl);
                    },
                };
                BtnUtils.formLoad(option);
            });
            // 处理美团问题单
            $processMtComplainedBtn.click(function () {
                var option = {
                    data: {id: id},
                    url: baseUrl + "/make-Mtcomplained",
                    formaction: baseUrl + "/push-Mtcomplained",
                    title: "处理美团问题单",
                    aftersuccess: function (data) {
                        IntelCarUtils.initAjax(redirectUrl);
                    },
                };
                BtnUtils.formLoad(option);
            });

            //风控佣金扣除
            $orderRiskDeduct.click(function () {
                BtnUtils.dialogConfirm("确认进行风控佣金扣除吗?", "提醒", function () {
                    IntelCarUtils.ajax(baseUrl + "/risk-deduct/data", {orderId: id, type: 2}, function (result) {
                        if (result.status == "200") {
                            ToastrUtils.getSuccess("操作成功", result.message);
                        } else {
                            ToastrUtils.getError("操作失败", result.message);
                        }
                        IntelCarUtils.initAjax(redirectUrl);
                    });
                });
            });

            //风控优惠扣除
            $orderRiskCouponDeduct.click(function () {
                BtnUtils.dialogConfirm("确认进行风控优惠扣除吗?", "提醒", function () {
                    IntelCarUtils.ajax(baseUrl + "/risk-deduct/data", {orderId: id, type: 1}, function (result) {
                        if (result.status == "200") {
                            ToastrUtils.getSuccess("操作成功", result.message);
                        } else {
                            ToastrUtils.getError("操作失败", result.message);
                        }
                        IntelCarUtils.initAjax(redirectUrl);
                    });
                });
            });

            //取消风控扣除
            $cancelOrderRiskDeduct.click(function () {
                BtnUtils.dialogConfirm("确认取消风控扣除吗?", "提醒", function () {
                    IntelCarUtils.ajax(baseUrl + "/risk-deduct-cancel/data", {orderId: id}, function (result) {
                        if (result.status == "200") {
                            ToastrUtils.getSuccess("操作成功", result.message);
                        } else {
                            ToastrUtils.getError("操作失败", result.message);
                        }
                        IntelCarUtils.initAjax(redirectUrl);
                    });
                });
            });

            //获取滴滴虚拟号，根据乘客手机号查询
            $didiPassengerPhoneBtn.click(function () {
                IntelCarUtils.ajax(baseUrl + "/didi/work/didiVirtualNumber", {
                    type: 3,
                    orderId: orderTcId,
                    didiId: distributionOrderId
                }, function (data) {
                    if (data.status == 200) {
                        console.log('获取手机号' + data.result)
                        $didiPassengerPhoneBtn.attr("copyText", data.result)
                        var clipboard = new ClipboardJS('.didi-copy', {
                            text: function (trigger) {
                                return trigger.getAttribute('copyText');
                            }
                        });
                        console.info("浏览器复制功能支持:" + ClipboardJS.isSupported())
                        if (!ClipboardJS.isSupported()) {
                            ToastrUtils.getError("当前浏览器不支持复制功能！");
                        }
                        clipboard.on('success', function (e) {
                            ToastrUtils.getSuccess("复制成功" + e.text);
                            e.clearSelection();
                        });
                        clipboard.on('error', function (e) {
                            ToastrUtils.getError("复制失败！");
                        });
                    } else {
                        ToastrUtils.getError("获取失败！");
                        console.log(data);
                    }
                });
            });
            //滴滴问题单改价
            $changePriceBtn.click(function () {
                IntelCarUtils.ajax(baseUrl + "/didi/work/queryDiDiStatus", {
                    orderTcId: orderTcId,
                    distributionOrderId: distributionOrderId
                }, function (result) {
                    if (result.status == "200") {
                        if (result.result != 104) {
                            ToastrUtils.getError("订单状态异常，刷新重试");
                            IntelCarUtils.initAjax(redirectUrl);
                            return;
                        }
                        var option = {
                            data: {
                                orderId: id,
                                distributionOrderId: distributionOrderId,
                                priceLoad: priceLoad,
                                orderTcId: orderTcId
                            },
                            url: baseUrl + "/didi/work/ChangePrice/show",
                            formaction: baseUrl + "/didi/work/ChangePrice",
                            title: "更改金额",
                            aftersuccess: function (data) {
                                IntelCarUtils.initAjax(redirectUrl);
                            },
                        };
                        BtnUtils.formLoad(option);
                    } else {
                        ToastrUtils.getError("请求订单状态失败，刷新重试", result.message);
                        IntelCarUtils.initAjax(redirectUrl);
                    }
                });
            });
            //滴滴分销取消订单
            $didiCancelBtn.click(function () {
                var option = {
                    data: {
                        orderId: id,
                    },
                    url: baseUrl + "/didi/work/cancelOrder/show",
                    formaction: baseUrl + "/didi/work/cancelOrder",
                    title: "申请退款",
                    aftersuccess: function (data) {
                        IntelCarUtils.initAjax(redirectUrl);
                    },
                };
                BtnUtils.formLoad(option);
            });
            //滴滴问题单推送结算状态
            $pushOrderStatusToDiDi.click(function () {
                BtnUtils.dialogConfirm("确定同步滴滴已结算吗？", "提醒", function () {
                    IntelCarUtils.ajax(baseUrl + "/didi/work/pushBalanceToDiDi", {
                            orderId: id,
                            distributionOrderId: distributionOrderId,
                            orderTcId: orderTcId,
                            cityId: cityId
                        },
                        function (result) {
                            if (result.status == "200") {
                                ToastrUtils.getSuccess("操作成功", result.message);
                            } else {
                                ToastrUtils.getError("操作失败", result.message);
                            }
                            IntelCarUtils.initAjax(redirectUrl);
                        });
                });
            });
            //滴滴同步用户支付状态
            $pullPayStatusFromDiDi.click(function () {
                BtnUtils.dialogConfirm("确定同步滴滴支付信息嘛？", "提醒", function () {
                    IntelCarUtils.ajax(baseUrl + "/didi/work/pullPayStatusFromDiDi", {
                            orderId: id,
                            distributionOrderId: distributionOrderId,
                            orderTcId: orderTcId
                        },
                        function (result) {
                            if (result.status == "200") {
                                ToastrUtils.getSuccess("操作成功", result.message);
                            } else {
                                ToastrUtils.getError("操作失败", result.message);
                            }
                            IntelCarUtils.initAjax(redirectUrl);
                        });
                });
            });
            //滴滴问题单退款
            $didiRefundPriceBtn.click(function () {
                IntelCarUtils.ajax(baseUrl + "/didi/work/queryDiDiStatus", {
                    orderTcId: orderTcId,
                    distributionOrderId: distributionOrderId
                }, function (result) {
                    if (result.status == "200") {
                        if (result.result != 105 && result.result != 108) {
                            ToastrUtils.getError("订单状态异常，刷新重试", result.message);
                            IntelCarUtils.initAjax(redirectUrl);
                            return;
                        }
                        var option = {
                            data: {
                                orderId: id,
                                orderTcId: orderTcId,
                                distributionOrderId: distributionOrderId
                            },
                            url: baseUrl + "/didi/work/billRefund/show",
                            formaction: baseUrl + "/didi/work/billRefund",
                            title: "申请退款",
                            aftersuccess: function (data) {
                                IntelCarUtils.initAjax(redirectUrl);
                            },
                        };
                        BtnUtils.formLoad(option);
                    } else {
                        ToastrUtils.getError("请求订单状态失败，刷新重试", result.message);
                        IntelCarUtils.initAjax(redirectUrl);
                    }
                });
            });
            //滴滴问题单退款查询
            $didiRefundQueryBtn.click(function () {
                console.info(encodeURIComponent(distributionOrderId))
                var option = {
                    data: {
                        orderId: id,
                        distributionOrderId: encodeURIComponent(distributionOrderId),
                    },
                    url: baseUrl + "/didi/work/billRefundQuery/show",
                    title: "退款查询",
                    aftersuccess: function (data) {
                        IntelCarUtils.initAjax(redirectUrl);
                    },
                };
                BtnUtils.viewLoad(option);
            });

            //滴滴问题单按当前公里数结算
            // $didiCloseOrderBtn.click(function () {
            //     BtnUtils.dialogConfirm("确认要按当前公里分钟结算订单吗？如需改价，请操作结算后改价哦~", "提醒", function () {
            //         IntelCarUtils.ajax(baseUrl + "/cs/closeOrder", {orderId: id}, function (result) {
            //             if (result.status == "200") {
            //                 ToastrUtils.getSuccess("操作成功", result.message);
            //             } else {
            //                 ToastrUtils.getError("操作失败", result.message);
            //             }
            //             IntelCarUtils.initAjax(redirectUrl);
            //         });
            //     });
            // });

            //更改显示在 标签页 的文字
            IntelCarUtils.pageNameChange($pageSign, $pageName);
        } catch (e) {
            console.error(e);
            alert("页面初始化失败");
        }
    });
})();