(function () {
    jQuery(document).ready(function () {
        try {
            var baseUrl = "./merchant/order";
            var $pageSign = $("#mtWorkInfoList").randomId();
            var orderId = $("#hiddOrderID").randomId().val();
            var grid = new Datatable();
            grid.init({
                src: $pageSign,
                dataTable: {
                    "columns": [{data: "transactionNo"}, {
                        data: "sourceType",
                        createdCell: function (td, tdData) {
                            if (tdData == 2) {
                                $(td).html('<span class="label label-sm label-warning" style="background-color: #89C4F4">高德</span>');
                            } else {
                                $(td).html('<span class="label label-sm label-warning" style="background-color: #43ad4f">秒走</span>');
                            }
                        }
                    }, {
                        data: "typeGd",
                        createdCell: function (td, tdData) {
                            if (tdData == '1') {
                                $(td).html('<span class="label label-sm label-warning" style="background-color: #89C4F4">支付失败</span>');
                            } else if (tdData == '2') {
                                $(td).html('<span class="label label-sm label-warning" style="background-color: #89C4F4">未乘车扣费</span>');
                            } else if (tdData == '3') {
                                $(td).html('<span class="label label-sm label-warning" style="background-color: #89C4F4">其他方式已付费</span>');
                            } else if (tdData == '4') {
                                $(td).html('<span class="label label-sm label-warning" style="background-color: #89C4F4">费用不认可</span>');
                            } else if (tdData == '5') {
                                $(td).html('<span class="label label-sm label-warning" style="background-color: #89C4F4">重复扣费</span>');
                            } else if (tdData == '6') {
                                $(td).html('<span class="label label-sm label-warning" style="background-color: #89C4F4">其他费用问题</span>');
                            } else if (tdData == '7') {
                                $(td).html('<span class="label label-sm label-warning" style="background-color: #89C4F4">投诉司机</span>');
                            } else if (tdData == '8') {
                                $(td).html('<span class="label label-sm label-warning" style="background-color: #89C4F4">物品丢失</span>');
                            } else if (tdData == '9') {
                                $(td).html('<span class="label label-sm label-warning" style="background-color: #89C4F4">取消订单</span>');
                            } else if (tdData == '10') {
                                $(td).html('<span class="label label-sm label-warning" style="background-color: #89C4F4">发票问题</span>');
                            } else if (tdData == '11') {
                                $(td).html('<span class="label label-sm label-warning" style="background-color: #89C4F4">其他投诉问题</span>');
                            } else if (tdData == '12') {
                                $(td).html('<span class="label label-sm label-warning" style="background-color: #89C4F4">客服置入</span>');
                            } else {
                                $(td).html('<span class="label label-sm label-warning" style="background-color: #89C4F4">其他问题</span>');
                            }
                        }
                    }, {data: "passengerFeedback"}, {data: "typeTc"},
                        // {
                        //     data: "refundType",
                        //     createdCell: function (td, tdData) {
                        //         if (tdData == '1') {
                        //             $(td).html('<span class="label label-sm label-warning" style="background-color: #89C4F4">全额退款</span>');
                        //         } else if (tdData == '2') {
                        //             $(td).html('<span class="label label-sm label-warning" style="background-color: #89C4F4">部分退款</span>');
                        //         } else {
                        //             $(td).html('<span class="label label-sm label-warning" style="background-color: #adadad">不涉及退款</span>');
                        //         }
                        //     }
                        // },
                        {
                            data: "refundAmount",
                            createdCell: function (td, tdData) {
                                $(td).html('<span>' + parseFloat(tdData) / 100 + '</span>');
                            }
                        }, {data: "processResult"}, {data: "operatorName"}, {data: "createTime"}, {
                            data: "status",
                            createdCell: function (td, tdData, full) {
                                console.info(full)
                                if (full["status"] == 0) {
                                    editBtn = BtnUtils.formBtnCreate(orderId, baseUrl + "/complained-process?orderId=" + orderId
                                        , baseUrl + "/complained-process/data?orderId=" + orderId + "&transactionNo=" + full["transactionNo"], "处理", baseUrl + "/airport/get?id=" + orderId);
                                    $(td).html([editBtn]);
                                } else {
                                    $(td).html('<span class="label label-sm label-warning" style="background-color: #adadad">处理完成</span>');
                                }
                            }
                        }],
                    "ajax": {
                        "url": baseUrl + "/getGdWorkInfoListData?orderId=" + orderId,
                    },
                    order: [[8, "desc"]],
                }
            });
        } catch (e) {
            console.error(e);
            alert("页面初始化失败");
        }
    });
})();