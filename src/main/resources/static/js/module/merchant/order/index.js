var MerchantTIndexPageSearchTable;
jQuery(document).ready(function () {
    try {

        var baseUrl = "./merchant/order";
        var $list = $("#platform-order-list").randomId();
        var $didisearchBtn = $("#airport-didiSearch").randomId();
        var $idInput = $("#order-airport-id-input").randomId();

        $didisearchBtn.click(function () {
            didiindexSearch(MerchantTIndexPageSearchTable, $list, baseUrl, $idInput);
        });

        function didiindexSearch(grid, $list, baseUrl, $idInput) {
            if (grid != undefined) {
                grid.getDataTable().destroy();
            }
            $list.show();
            MerchantTIndexPageSearchTable = new Datatable();
            MerchantTIndexPageSearchTable.setAjaxParam("orderId", $idInput.val());
            MerchantTIndexPageSearchTable.init({
                src: $list,
                dataTable: {
                    "columns": [
                        {
                            data: "orderId",
                            createdCell: function (td, tdData) {
                                var url = IntelCarUtils.ajaxUrlGenerate(baseUrl + "/airport/get?", {"id": tdData});
                                $(td).html('<a href="' + url + '" class="ajaxify" uniqueTabCheck="false">' + tdData + '</a>');
                            }
                        },
                        {data: "orderTcId"},
                        {data: "passengerName"},
                        {data: "passengerCellphone"},
                        {data: "startCityName"},
                        {data: "createTime"},
                        {data: "useTime"},
                        {
                            data: "amountType", createdCell: function (td, tdData) {
                                var statusSpan = '';
                                if (tdData == 0) {
                                    statusSpan = '<span class="label label-sm label-' + 'warning' + '">一口价</span>';
                                } else if (tdData == 1) {
                                    statusSpan = '<span class="label label-sm label-' + 'success' + '">实时报价</span>';
                                } else {
                                    statusSpan = '<span class="label label-sm label-' + 'danger' + '">未知状态' + tdData + '</span>';
                                }
                                $(td).html(statusSpan);
                            }
                        },
                        {
                            data: "status", createdCell: function (td, tdData) {
                                var statusSpan = '';
                                if (tdData == 0) {
                                    statusSpan = '<span class="label label-sm label-' + 'warning' + '">创建订单</span>';
                                } else if (tdData == 10) {
                                    statusSpan = '<span class="label label-sm label-' + 'success' + '">订单推送成功</span>';
                                } else if (tdData == 15) {
                                    statusSpan = '<span class="label label-sm label-' + 'success' + '">供应商接单成功</span>';
                                } else if (tdData == 20) {
                                    statusSpan = '<span class="label label-sm label-' + 'success' + '">接单成功</span>';
                                } else if (tdData == 50) {
                                    statusSpan = '<span class="label label-sm label-' + 'success' + '">司机出发</span>';
                                } else if (tdData == 100) {
                                    statusSpan = '<span class="label label-sm label-' + 'success' + '">司机到达</span>';
                                } else if (tdData == 200) {
                                    statusSpan = '<span class="label label-sm label-' + 'success' + '">开始服务</span>';
                                } else if (tdData == 300) {
                                    statusSpan = '<span class="label label-sm label-' + 'success' + '">订单完成</span>';
                                } else if (tdData == 400) {
                                    statusSpan = '<span class="label label-sm label-' + 'success' + '">已结算</span>';
                                } else if (tdData == 500) {
                                    statusSpan = '<span class="label label-sm label-' + 'success' + '">用户已支付</span>';
                                } else if (tdData == 1000) {
                                    statusSpan = '<span class="label label-sm label-' + 'success' + '">订单取消</span>';
                                } else {
                                    statusSpan = '<span class="label label-sm label-' + 'danger' + '">未知状态' + tdData + '</span>';
                                }

                                $(td).html(statusSpan);
                            }
                        }],
                    "ajax": {
                        "url": baseUrl + "/didi/work/listData?",
                    },
                    order: [[6, "desc"]],
                }
            });
        }

    } catch (e) {
        console.error(e);
        alert("页面初始化失败");
    }
});