var IntelCarUtils = function () {

    return {

        ajax: function (url, data, successcallback, errorcallback, type) {
            if (!successcallback) {
                successcallback = function (result) {
                    console.log("默认成功处理方式", result);
                }
            }
            if (!errorcallback) {
                errorcallback = function (XMLHttpRequest, textStatus, errorThrown) {
                    console.error("ajax:" + XMLHttpRequest.responseJSON.status);
                    ToastrUtils.getError("操作失败", XMLHttpRequest.responseJSON.message);
                }
            }

            $.ajax({
                url: url,
                data: data,
                type: type || "post",
                success: successcallback,
                error: errorcallback,
                complete: function () {
                    Metronic.stopPageLoading();
                }
            });
        },

        ajaxFile: function (url, data, successcallback, errorcallback, type) {
            if (!successcallback) {
                successcallback = function (result) {
                    console.log("默认成功处理方式", result);
                }
            }
            if (!errorcallback) {
                errorcallback = function (XMLHttpRequest, textStatus, errorThrown) {
                    console.error("ajaxFile:" + XMLHttpRequest.responseJSON.status);
                    ToastrUtils.getError("操作失败", XMLHttpRequest.responseJSON.message);
                }
            }

            $.ajax({
                url: url,
                data: data,
                type: type || "post",
                processData: false,
                contentType: false,
                success: successcallback,
                error: errorcallback,
                complete: function () {
                    Metronic.stopPageLoading();
                }
            });
        },

        initAjax: function (url) {
            Layout.ajaxify(url);
            // let contentBody = $(".page-content-body:visible").first();
            // IntelCarUtils.ajax(url, null, function (result) {
            //     contentBody.html(result);
            //     Metronic.initAjax();
            // }, null, "POST");
        },

        ajaxConfirm: function (url, name, redirectUrl) {
            BtnUtils.dialogConfirm("你确定要" + name + "吗?", "提醒", function () {
                IntelCarUtils.ajax(url, {}, function () {
                    IntelCarUtils.initAjax(redirectUrl);
                });
            });
        },

        ajaxUrlGenerate: function (url, params) {
            if (params != undefined) {
                for (let key in params) {
                    url = url + key.toString() + "=" + params[key].toString() + "&";
                }
            }
            return url;
        },

        statusLabel: function (status) {
            if (status == "激活") {
                return '<span class="label label-sm label-' + 'success' + '">' + (status) + '</span>';
            } else if (status == "冻结") {
                return '<span class="label label-sm label-' + 'danger' + '">' + (status) + '</span>';
            } else {
                return '<span class="label label-sm label-' + 'warning' + '">' + (status) + '</span>';
            }
        },

        tagHtml: function (tagList) {
            let result = "";
            $.each(tagList, function (n, value) {
                result += '<span class="label label-sm label-info">' + value + '</span>&nbsp;&nbsp;';
            });
            return result;
        },

        validate: function ($form, rules) {
            $form.validate({
                errorElement: 'span',
                errorClass: 'help-block help-block-error',
                focusInvalid: false,
                rules: rules,
                errorPlacement: function (error, element) {
                    if (element.parent(".input-group").size() > 0) {
                        error.insertAfter(element.parent(".input-group"));
                    } else if (element.attr("data-error-container")) {
                        error.appendTo(element.attr("data-error-container"));
                    } else {
                        error.insertAfter(element);
                    }
                },
                highlight: function (element) {
                    $(element).closest('.form-group').removeClass('has-success').addClass('has-error');
                },
                unhighlight: function (element) {
                    $(element).closest('.form-group').removeClass('has-error');
                },
                success: function (label) {
                    label.closest('.form-group').removeClass('has-error').addClass("has-success");
                },
            });
        },

        cityPicker: function ($select) {
            if ($select == undefined || $select == null || $select.length == 0) {
                return false;
                exit(0);
            }
            $select.citypicker({
                simple: true,
                responsive: true,
            });
        },

        select2: function ($select, name) {
            if ($select == undefined || $select == null || $select.length == 0) {
                return false;
                exit(0);
            }
            $select.select2("destroy").select2({
                placeholder: name,
                allowClear: true,
                escapeMarkup: function (m) {
                    return m;
                },
                width: '100%',
                locales: 'zh-CN',
                formatMatches: function (term) {
                },
                formatNoMatches: function (term) {
                },
            });
        },

        select2NoQuery: function ($select, name) {
            if ($select == undefined || $select == null || $select.length == 0) {
                return false;
                exit(0);
            }
            $select.select2("destroy").select2({
                placeholder: name,
                allowClear: true,
                escapeMarkup: function (m) {
                    return m;
                },
                width: '100%',
                locales: 'zh-CN',
                minimumResultsForSearch: -1,
                formatMatches: function (term) {
                },
                formatNoMatches: function (term) {
                },
            });
        },

        dataTable: function ($table) {
            if ($table == undefined || $table == null || $table.length == 0) {
                return false;
                exit(0);
            }
            $table.DataTable({
                language: {
                    "sProcessing": "处理中...",
                    "sLengthMenu": "显示 _MENU_ 项结果",
                    "sZeroRecords": "没有匹配结果",
                    "sInfo": "显示第 _START_ 至 _END_ 项结果，共 _TOTAL_ 项",
                    "sInfoEmpty": "显示第 0 至 0 项结果，共 0 项",
                    "sInfoFiltered": "(由 _MAX_ 项结果过滤)",
                    "sInfoPostFix": "",
                    "sSearch": "搜索:",
                    "sUrl": "",
                    "sEmptyTable": "表中数据为空",
                    "sLoadingRecords": "载入中...",
                    "sInfoThousands": ",",

                    "serverSide": true,
                    "pageLength": 10,


                    "oPaginate": {
                        "sFirst": "首页",
                        "sPrevious": "上页",
                        "sNext": "下页",
                        "sLast": "末页"
                    },
                    "oAria": {
                        "sSortAscending": ": 以升序排列此列",
                        "sSortDescending": ": 以降序排列此列"
                    }
                }
            });
        },

        pageNameChange: function ($pageSign, pageName) {
            let spanId = "#" + $pageSign.parents(".page-content-body").first().attr("id") + "-span";
            $(spanId).html(pageName);
        },


        /*赔款工单*/
        workUrlCreateReparations: function (id, orderTCId, payFlowNumber, productId, merchantCode, originAmount) {
            let url = 'http://tcwlservice.17usoft.com/workorder/manager/main.html#/fill_temp?id=35&autocompletedata={"ordersource":"12","poid":"' +
                id + '","rows":[{"controlId":"7304","dv":"9750"}, ' +
                '{"controlId":"7305","dv":"' + orderTCId + '"},' +
                '{"controlId":"7306","dv":"' + payFlowNumber + '"},' +
                '{"controlId":"10796","dv":"' + productId + '"},' +
                '{"controlId":"10797","dv":"' + merchantCode + '"},' +
                '{"controlId":"10798","dv":"' + originAmount + '"}' +
                ']}';
            return url;
        },

        /*退款工单*/
        workUrlCreateRefund: function (id, orderTCId, payFlowNumber, refundAmount, productId, merchantCode, originAmount, actualAccout) {
            let url = 'http://tcwlservice.17usoft.com/workorder/manager/main.html#/fill_temp?id=33&autocompletedata={"ordersource":"12","poid":"' +
                id + '","rows":[{"controlId":"7286","dv":"9735"},' +
                '{"controlId":"7287","dv":"' + orderTCId + '"},' +
                '{"controlId":"7288","dv":"' + payFlowNumber + '"},' +
                '{"controlId":"7289","dv":"' + refundAmount + '"},' +
                '{"controlId":"10804","dv":"' + productId + '"},' +
                '{"controlId":"10805","dv":"' + merchantCode + '"},' +
                '{"controlId":"10806","dv":"' + originAmount + '"},' +
                '{"controlId":"10807","dv":"' + actualAccout + '"}' +
                ']}';
            return url;
        },

        /*交接工单*/
        workUrlCreate: function (id, orderTCId, merchantCode, productId, passengerName, useTime, callBackPhone, cellphone) {
            let url = 'http://tcwlservice.17usoft.com/workorder/manager/main.html#/fill_temp?id=6&autocompletedata={"ordersource":"12","poid":"' +
                orderTCId + '","rows":[{"controlId":"11898","dv":"' + orderTCId + '"},' +
                '{"controlId":"10306","dv":"' + id + '"},' +
                '{"controlId":"10792","dv":"' + merchantCode + '"},' +
                '{"controlId":"10793","dv":"' + productId + '"},' +
                '{"controlId":"10795","dv":"' + passengerName + '"},' +
                '{"controlId":"10307","dv":"' + callBackPhone + '"},' +
                '{"controlId":"10794","dv":"' + useTime + '"},' +
                '{"controlId":"11600","dv":"' + cellphone + '"}' +
                '' +
                ']}';
            return url;
        },

        /*添加投诉*/
        complain: function (passengerName, mobile, orderno, amount) {
            let strUrl = "";
            strUrl = "http://tcwlservice.17usoft.com/feedback/feedbackadmin/#/Customer?type=1&projectId=180010&name=" + passengerName + "&mobile=" + mobile + "&order_no=" + orderno + "&amount=" + amount + "";
            return strUrl;
        },

        dateTimeInput: function ($dateInput) {
            if ($dateInput == undefined || $dateInput == null || $dateInput.length == 0) {
                return false;
                exit(0);
            }
            $dateInput.datetimepicker({
                format: "yyyy-mm-dd hh:ii",
                autoclose: true,
                todayBtn: true,
                todayHighlight: true,
                keyboardNavigation: true,
                language: "zh-CN"
            });
            $dateInput.inputmask("y-m-d h:s", {"placeholder": "yyyy-MM-dd HH:mm"});
        },

        dateInput: function ($dateInput) {
            if ($dateInput == undefined || $dateInput == null || $dateInput.length == 0) {
                return false;
                exit(0);
            }
            $dateInput.datetimepicker({
                format: "yyyy-mm-dd",
                minView:"month",
                autoclose: true,
                todayBtn: true,
                todayHighlight: true,
                keyboardNavigation: true,
                language: "zh-CN"
            });
            $dateInput.inputmask("y-m-d", {"placeholder": "yyyy-MM-dd"});
        },

        monthInput: function ($dateInput) {
            if ($dateInput == undefined || $dateInput == null || $dateInput.length == 0) {
                return false;
                exit(0);
            }
            $dateInput.datetimepicker({
                format: "yyyy-mm",
                minView:"year",
                autoclose: true,
                todayBtn: true,
                todayHighlight: true,
                keyboardNavigation: true,
                language: "zh-CN"
            });
            $dateInput.inputmask("y-m", {"placeholder": "yyyy-MM"});
        },

        placeSearch: function ($querySelect, $cityName, $placeName, $placeLat, $placeLng, $formattedAddress) {
            $querySelect.select2("destroy").select2({
                ajax: {
                    url: function (query) {
                        return baselocation + "/platform/order/searchPlace?cityName=" + encodeURI($cityName.val()) + "&queryKey=" + encodeURI(query);
                    },
                    quietMillis: 500,
                    results: function (data) {
                        if (data && data.result && jQuery.isArray(data.result)) {
                            return {
                                results: data.result || [],
                                more: false,
                            };
                        } else {
                            return {
                                results: [],
                                more: false,
                                hasError: true,
                                jqXHR: "未搜到结果",
                                textStatus: "未搜到结果",
                                errorThrown: "未搜到结果",
                            };
                        }
                    },
                },
                formatMatches: function (term) {
                },
                formatNoMatches: function (term) {
                },
                escapeMarkup: function (markup) {
                    return markup;
                },
                minimumInputLength: 2,
                placeholder: "搜索地址",
                formatResult: function formatRepo(repo) {
                    return "<strong>" + repo.placeName + "</strong><p>" + repo.formattedAddress + "</p>";
                },
                formatSelection: function formatRepoSelection(repo) {
                    return "<strong>" + repo.placeName + "</strong>" + repo.formattedAddress;
                },
            });
            $querySelect.change(function () {
                let select = $querySelect.select2('data');
               //alert(select.placeLng);
                if ($placeName || false)
                    $placeName.val(select.placeName);
                if ($placeLat || false)
                    $placeLat.val(select.placeLat);
                if ($placeLng || false)
                    $placeLng.val(select.placeLng);
                if ($formattedAddress || false)
                    $formattedAddress.val(select.formattedAddress);
            });
        },

    };
}();
(function (factory) {
    if (typeof define === 'function' && define.amd) {
        define(['jquery'], factory);
    } else if (typeof exports === 'object') {
        factory(require('jquery'));
    } else {
        factory(jQuery);
    }
}(function ($) {
    function encode(s) {
        return config.raw ? s : encodeURIComponent(s);
    }

    function decode(s) {
        return config.raw ? s : decodeURIComponent(s);
    }

    function stringifyCookieValue(value) {
        return encode(config.json ? JSON.stringify(value) : String(value));
    }

    function parseCookieValue(s) {
        if (s.indexOf('"') === 0) {
            s = s.slice(1, -1).replace(/\\"/g, '"').replace(/\\\\/g, '\\');
        }
        try {
            s = decodeURIComponent(s.replace(/\+/g, ' '));
            return config.json ? JSON.parse(s) : s;
        } catch (e) {
        }
    }

    function read(s, converter) {
        let value = config.raw ? s : parseCookieValue(s);
        return $.isFunction(converter) ? converter(value) : value;
    }

    var config = $.cookie = function (key, value, options) {
        if (value !== undefined && !$.isFunction(value)) {
            options = $.extend({}, config.defaults, options);

            if (typeof options.expires === 'number') {
                let days = options.expires, t = options.expires = new Date();
                t.setTime(+t + days * 864e+5);
            }
            return (document.cookie = [
                encode(key), '=', stringifyCookieValue(value),
                options.expires ? '; expires=' + options.expires.toUTCString() : '', // use expires attribute, max-age is not supported by IE
                options.path ? '; path=' + options.path : '',
                options.domain ? '; domain=' + options.domain : '',
                options.secure ? '; secure' : ''
            ].join(''));
        }
        let result = key ? undefined : {};
        let cookies = document.cookie ? document.cookie.split('; ') : [];

        for (let i = 0, l = cookies.length; i < l; i++) {
            let parts = cookies[i].split('=');
            let name = decode(parts.shift());
            let cookie = parts.join('=');
            if (key && key === name) {
                result = read(cookie, value);
                break;
            }
            if (!key && (cookie = read(cookie)) !== undefined) {
                result[name] = cookie;
            }
        }
        return result;
    };
    config.defaults = {};
    //INFO TIMCROSSINFO session失效则刷新页面,防止session过期导致页面假死
    $.ajaxSetup({
        complete: function (XMLHttpRequest) {
            if (XMLHttpRequest.getResponseHeader("sessionstatus") == "timeout") {
                /* window.location.reload(); */
                BtnUtils.dialogConfirm("您的登陆已过期，是否需要刷新登陆状态？", "登陆过期提醒", function () {
                    window.open(window.location.href, "_blank");
                }, function () {
                    ToastrUtils.getError("登陆过期提醒", "您的登陆已过期");
                })
            }
        }
    });
    //INFO TIMCROSSINFO 生成随机id , 防止相同页面引用 , 造成重复id
    $.fn.randomId = function (salt) {
        let $id = $(this);
        if (salt == undefined) {
            salt = $("input[pname='salt']:first", $id).val();
            if (salt == undefined) {
                salt = new Date().getTime();
            }
        }
        $id.attr("id", $id.attr("id") + '_' + salt);
        return $id;
    };
    let route = $.cookie("route");
    if (route == undefined || route == null) {
    }
    //INFO TIMCROSSINFO 负载均衡路由
    $.cookie("route", route, {
        path: '/jidaManagement',
    });
    // console.info("route is :" + route);
}));
