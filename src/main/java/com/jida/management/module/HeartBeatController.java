package com.jida.management.module;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
public class HeartBeatController {

    @RequestMapping(value = "/heartBeat")
    @ResponseBody
    public String heartBeat() {
        return "success";
    }
}
