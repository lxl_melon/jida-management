package com.jida.management.module;

import com.jida.management.exception.BaseException;
import com.jida.management.exception.ValidateException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.web.HttpRequestMethodNotSupportedException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MaxUploadSizeExceededException;

import javax.servlet.http.HttpServletResponse;
import javax.validation.ValidationException;

@ControllerAdvice
@Slf4j
public class GlobalExceptionHandler {

    @ExceptionHandler(HttpRequestMethodNotSupportedException.class)
    @ResponseBody
    public AjaxRsp handleRequestMethodExp(HttpRequestMethodNotSupportedException ex, HttpServletResponse response) {

        return new AjaxRsp(HttpStatus.METHOD_NOT_ALLOWED.value(), "请求方法不支持");
    }

    @ExceptionHandler(ValidationException.class)
    @ResponseBody
    public AjaxRsp handleDefaultValidationException(ValidationException ex, HttpServletResponse response) {
        return new AjaxRsp(HttpStatus.BAD_REQUEST.value(), "请求参数有误");
    }

    @ExceptionHandler(BaseException.class)
    @ResponseBody
    public AjaxRsp handleBaseException(BaseException ex, HttpServletResponse response) {
        return new AjaxRsp(ex.getCode(), ex.getMessage());
    }

    @ExceptionHandler(ValidateException.class)
    @ResponseBody
    public AjaxRsp handleValidateException(ValidateException ex, HttpServletResponse response) {
        return new AjaxRsp(ex.getCode(), ex.getMessage());
    }

    @ExceptionHandler(Exception.class)
    @ResponseBody
    public AjaxRsp handleExp(Exception ex, HttpServletResponse response) {
        return new AjaxRsp(500, "系统未知错误");
    }

    @ExceptionHandler(MaxUploadSizeExceededException.class)
    @ResponseBody
    public AjaxRsp handleExp(MaxUploadSizeExceededException ex, HttpServletResponse response) {
        //response.setStatus(HttpStatus.NOT_ACCEPTABLE.value());
        return new AjaxRsp(HttpStatus.INTERNAL_SERVER_ERROR.value(), "上传图片过大,最大不超过3MB");
    }
}