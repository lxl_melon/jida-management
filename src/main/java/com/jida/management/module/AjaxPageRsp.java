package com.jida.management.module;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * Created by wugn on 2018/10/24.
 */
@Data
@NoArgsConstructor
@Builder
@AllArgsConstructor
public class AjaxPageRsp<T> {

    public static final int SUCCESS_CODE = 200;
    public static final int ERROR_CODE = 403;
    public static final String SUCCESS_MSG = "success";
    public static final String ERROR_MSG = "error";

    private int draw; // Client request times
    private int recordsTotal; // Total records number without conditions
    private int recordsFiltered; // Total records number with conditions
    private List<T> result; // The data we should display on the page
    private int status;
    private String message;


    public AjaxPageRsp<T> success(){
        return this.success(SUCCESS_CODE,SUCCESS_MSG);
    }

    public AjaxPageRsp<T> success(int status, String code){
        this.setStatus(status);
        this.setMessage(code);
        return this;
    }

    public AjaxPageRsp<T> error(){
        return this.error(ERROR_CODE,ERROR_MSG);
    }

    public AjaxPageRsp<T> error(int status, String code){
        this.setStatus(status);
        this.setMessage(code);
        return this;
    }


}
