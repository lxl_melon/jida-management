package com.jida.management.module;

import lombok.Data;

@Data
public class AjaxRsp<T> {
    public static final int SUCCESS = 200;
    public static final int NEED_FORCE = 403;

    private int status;
    private String message;
    private T result;

    public AjaxRsp() {
        this(null);
    }

    public AjaxRsp(int status,String msg) {
        this.status = status;
        this.message = msg;
    }

    public AjaxRsp(T result) {
        this(SUCCESS,"success");
        this.result = result;
    }
}