package com.jida.management.module.main;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.jida.management.module.AjaxRsp;
import com.jida.management.mybatis.entity.ErpUserInfo;
import com.jida.management.mybatis.service.ErpUserInfoService;
import org.apache.catalina.servlet4preview.http.HttpServletRequest;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.Cookie;
import java.time.LocalDateTime;
import java.util.Date;
import java.util.UUID;

@Controller
@RequestMapping(value = "")
public class MainController {

    @Autowired
    ErpUserInfoService erpUserInfoService;

    @RequestMapping(value = "login")
    public String login() {
        return "main/login.html";
    }

    @RequestMapping(value = "loginData")
    @ResponseBody
    public AjaxRsp loginData(Model model, String username, String password) {
        if (StringUtils.isBlank(username) || StringUtils.isBlank(password)) {
            return new AjaxRsp(-1, "用户名或密码为空");
        }
        QueryWrapper queryWrapper = new QueryWrapper();
        queryWrapper.eq("name", username.trim());
        queryWrapper.eq("password", password.trim());
        queryWrapper.eq("status", 1);
        ErpUserInfo erpUserInfo = erpUserInfoService.getOne(queryWrapper);
        if (erpUserInfo != null) {
            String accessToken = UUID.randomUUID().toString().replace("-", "");
            erpUserInfo.setExpireTime(LocalDateTime.now().plusHours(2))
                    .setAccessToken(accessToken).updateById();
            model.addAttribute("accessToken", accessToken);
            model.addAttribute("userName", erpUserInfo.getName());
            return new AjaxRsp(accessToken);
        }
        return new AjaxRsp(-1, "用户名密码错误");
    }

    @RequestMapping(value = "main")
    public String main(HttpServletRequest request, Model model) {
        model.addAttribute("username", getUserName(request));
        return "main/main.html";
    }


    private static String getUserName(HttpServletRequest request) {
        Cookie[] cookies = request.getCookies();
        if (cookies != null) {
            for (Cookie cookie : cookies) {
                if (cookie.getName() != null && cookie.getName().equals("accessToken")) {
                    String accessToken = cookie.getValue();
                    if (StringUtils.isNotBlank(accessToken)) {
                        QueryWrapper queryWrapper = new QueryWrapper();
                        queryWrapper.eq("access_token", accessToken);
                        ErpUserInfo erpUserInfo = new ErpUserInfo().selectOne(queryWrapper);
                        if (erpUserInfo != null) {
                            return erpUserInfo.getName();
                        }
                    }
                }
            }
        }
        return "未登录";
    }

}





