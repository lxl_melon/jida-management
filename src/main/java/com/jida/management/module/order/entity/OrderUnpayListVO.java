package com.jida.management.module.order.entity;

import java.time.LocalDateTime;

/**
 * @description:
 * @author: Circle
 * @date: 2020-06-13 5:23 PM
 */
public class OrderUnpayListVO {
    /**
     * 订单号
     */
    private Long id;
    /**
     * 用户ID
     */
    private Long userId;
    /**
     * 地区名字
     */
    private String cityName;
    /**
     * 订单创建时间
     */
    private LocalDateTime createTime;
}
