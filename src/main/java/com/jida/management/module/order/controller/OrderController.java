package com.jida.management.module.order.controller;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.jida.management.module.AjaxRsp;
import com.jida.management.mybatis.entity.*;
import com.jida.management.mybatis.service.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

@Controller
@RequestMapping(value = "/order")
public class OrderController {

    @Autowired
    JidaOrderService jidaOrderService;
    @Autowired
    JidaOrderSenderService jidaOrderSenderService;
    @Autowired
    JidaOrderReceiverService jidaOrderReceiverService;
    @Autowired
    JidaOrderDeliverService jidaOrderDeliverService;
    @Autowired
    JidaUserPaymentService jidaUserPaymentService;
    @Autowired
    JidaOrderTrackService jidaOrderTrackService;

    @RequestMapping("unpay/list")
    public String unpayList() {
        return "order/unpay/list.html";
    }

    @RequestMapping("unpay/listData")
    @ResponseBody
    public AjaxRsp<List<JidaOrder>> unpayListData() {
        QueryWrapper<JidaOrder> queryWrapper = new QueryWrapper<>();
        //订单状态 0:已查价 100:已下单 200:已支付 300:派单中 400:待取件 500:到店 600:已取件，配送中 700:配送完成 1000:订单已取消
        queryWrapper.in("status", new Integer[]{100});
        List<JidaOrder> jidaOrderList = jidaOrderService.list(queryWrapper);
        return new AjaxRsp<>(jidaOrderList);
    }

    @RequestMapping("unpay/get")
    public String unpayGet(Model model, Long id) {
        JidaOrder jidaOrder = jidaOrderService.getById(id);
        QueryWrapper queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("order_id", id);
        JidaOrderSender jidaOrderSender = jidaOrderSenderService.getOne(queryWrapper);
        JidaOrderReceiver jidaOrderReceiver = jidaOrderReceiverService.getOne(queryWrapper);
        JidaOrderDeliver jidaOrderDeliver = jidaOrderDeliverService.getOne(queryWrapper);
        JidaUserPayment jidaUserPayment = jidaUserPaymentService.getOne(queryWrapper);
        model.addAttribute("orderInfo", jidaOrder);
        model.addAttribute("orderSender", jidaOrderSender);
        model.addAttribute("orderReceiver", jidaOrderReceiver);
        model.addAttribute("orderDeliver", jidaOrderDeliver);
        model.addAttribute("userPayment", jidaUserPayment);

        return "order/unpay/info.html";
    }

    @RequestMapping(value = "/getOrderTrackListData")
    @ResponseBody
    public AjaxRsp<List<JidaOrderTrack>> getOrderTrackListData(String orderId) {
        QueryWrapper queryWrapperTrack = new QueryWrapper<>();
        queryWrapperTrack.eq("order_id", orderId);
        queryWrapperTrack.eq("type", 1);
        queryWrapperTrack.orderByDesc("create_time");
        List<JidaOrderTrack> jidaOrderTrackList = jidaOrderTrackService.list(queryWrapperTrack);
        return new AjaxRsp(jidaOrderTrackList);
    }


    @RequestMapping("unpickup/list")
    public String unpickupList() {
        return "order/unpickup/list.html";
    }

    @RequestMapping("unpickup/listData")
    @ResponseBody
    public AjaxRsp<List<JidaOrder>> unpickupListData() {
        QueryWrapper<JidaOrder> queryWrapper = new QueryWrapper<>();
        //订单状态 0:已查价 100:已下单 200:已支付 300:派单中 400:待取件 500:到店 600:已取件，配送中 700:配送完成 1000:订单已取消
        queryWrapper.in("status", new Integer[]{400, 500});
        List<JidaOrder> jidaOrderList = jidaOrderService.list(queryWrapper);
        return new AjaxRsp<>(jidaOrderList);
    }

    @RequestMapping("unpickup/get")
    public String unpickupGet(Model model, Long id) {
        JidaOrder jidaOrder = jidaOrderService.getById(id);
        QueryWrapper queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("order_id", id);
        JidaOrderSender jidaOrderSender = jidaOrderSenderService.getOne(queryWrapper);
        JidaOrderReceiver jidaOrderReceiver = jidaOrderReceiverService.getOne(queryWrapper);
        JidaOrderDeliver jidaOrderDeliver = jidaOrderDeliverService.getOne(queryWrapper);
        JidaUserPayment jidaUserPayment = jidaUserPaymentService.getOne(queryWrapper);
        model.addAttribute("orderInfo", jidaOrder);
        model.addAttribute("orderSender", jidaOrderSender);
        model.addAttribute("orderReceiver", jidaOrderReceiver);
        model.addAttribute("orderDeliver", jidaOrderDeliver);
        model.addAttribute("userPayment", jidaUserPayment);

        return "order/unpickup/info.html";
    }

    @RequestMapping("delivering/list")
    public String deliveringList() {
        return "order/delivering/list.html";
    }

    @RequestMapping("delivering/listData")
    @ResponseBody
    public AjaxRsp<List<JidaOrder>> deliveringListData() {
        QueryWrapper<JidaOrder> queryWrapper = new QueryWrapper<>();
        //订单状态 0:已查价 100:已下单 200:已支付 300:派单中 400:待取件 500:到店 600:已取件，配送中 700:配送完成 1000:订单已取消
        queryWrapper.in("status", new Integer[]{600});
        List<JidaOrder> jidaOrderList = jidaOrderService.list(queryWrapper);
        return new AjaxRsp<>(jidaOrderList);
    }

    @RequestMapping("delivering/get")
    public String deliveringGet(Model model, Long id) {
        JidaOrder jidaOrder = jidaOrderService.getById(id);
        QueryWrapper queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("order_id", id);
        JidaOrderSender jidaOrderSender = jidaOrderSenderService.getOne(queryWrapper);
        JidaOrderReceiver jidaOrderReceiver = jidaOrderReceiverService.getOne(queryWrapper);
        JidaOrderDeliver jidaOrderDeliver = jidaOrderDeliverService.getOne(queryWrapper);
        JidaUserPayment jidaUserPayment = jidaUserPaymentService.getOne(queryWrapper);
        model.addAttribute("orderInfo", jidaOrder);
        model.addAttribute("orderSender", jidaOrderSender);
        model.addAttribute("orderReceiver", jidaOrderReceiver);
        model.addAttribute("orderDeliver", jidaOrderDeliver);
        model.addAttribute("userPayment", jidaUserPayment);

        return "order/delivering/info.html";
    }

    @RequestMapping("finish/list")
    public String finishList() {
        return "order/finish/list.html";
    }

    @RequestMapping("finish/listData")
    @ResponseBody
    public AjaxRsp<List<JidaOrder>> finishListData() {
        QueryWrapper<JidaOrder> queryWrapper = new QueryWrapper<>();
        //订单状态 0:已查价 100:已下单 200:已支付 300:派单中 400:待取件 500:到店 600:已取件，配送中 700:配送完成 1000:订单已取消
        queryWrapper.in("status", new Integer[]{700});
        List<JidaOrder> jidaOrderList = jidaOrderService.list(queryWrapper);
        return new AjaxRsp<>(jidaOrderList);
    }

    @RequestMapping("finish/get")
    public String finishGet(Model model, Long id) {
        JidaOrder jidaOrder = jidaOrderService.getById(id);
        QueryWrapper queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("order_id", id);
        JidaOrderSender jidaOrderSender = jidaOrderSenderService.getOne(queryWrapper);
        JidaOrderReceiver jidaOrderReceiver = jidaOrderReceiverService.getOne(queryWrapper);
        JidaOrderDeliver jidaOrderDeliver = jidaOrderDeliverService.getOne(queryWrapper);
        JidaUserPayment jidaUserPayment = jidaUserPaymentService.getOne(queryWrapper);
        model.addAttribute("orderInfo", jidaOrder);
        model.addAttribute("orderSender", jidaOrderSender);
        model.addAttribute("orderReceiver", jidaOrderReceiver);
        model.addAttribute("orderDeliver", jidaOrderDeliver);
        model.addAttribute("userPayment", jidaUserPayment);

        return "order/finish/info.html";
    }

    @RequestMapping("cancel/list")
    public String cancelList() {
        return "order/cancel/list.html";
    }

    @RequestMapping("cancel/listData")
    @ResponseBody
    public AjaxRsp<List<JidaOrder>> cancelListData() {
        QueryWrapper<JidaOrder> queryWrapper = new QueryWrapper<>();
        //订单状态 0:已查价 100:已下单 200:已支付 300:派单中 400:待取件 500:到店 600:已取件，配送中 700:配送完成 1000:订单已取消
        queryWrapper.in("status", new Integer[]{1000});
        List<JidaOrder> jidaOrderList = jidaOrderService.list(queryWrapper);
        return new AjaxRsp<>(jidaOrderList);
    }


    @RequestMapping("cancel/get")
    public String cancelGet(Model model, Long id) {
        JidaOrder jidaOrder = jidaOrderService.getById(id);
        QueryWrapper queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("order_id", id);
        JidaOrderSender jidaOrderSender = jidaOrderSenderService.getOne(queryWrapper);
        JidaOrderReceiver jidaOrderReceiver = jidaOrderReceiverService.getOne(queryWrapper);
        JidaOrderDeliver jidaOrderDeliver = jidaOrderDeliverService.getOne(queryWrapper);
        JidaUserPayment jidaUserPayment = jidaUserPaymentService.getOne(queryWrapper);
        model.addAttribute("orderInfo", jidaOrder);
        model.addAttribute("orderSender", jidaOrderSender);
        model.addAttribute("orderReceiver", jidaOrderReceiver);
        model.addAttribute("orderDeliver", jidaOrderDeliver);
        model.addAttribute("userPayment", jidaUserPayment);

        return "order/cancel/info.html";
    }
}





