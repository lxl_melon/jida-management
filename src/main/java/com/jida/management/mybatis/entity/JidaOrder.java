package com.jida.management.mybatis.entity;

import com.baomidou.mybatisplus.extension.activerecord.Model;
import java.time.LocalDateTime;
import java.io.Serializable;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 订单信息表
 * </p>
 *
 * @author JiDa
 * @since 2020-06-13
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
public class JidaOrder extends Model<JidaOrder> {

    private static final long serialVersionUID = 1L;

    /**
     * 订单号
     */
    private Long id;

    /**
     * 用户ID
     */
    private Long userId;

    /**
     * 商户ID
     */
    private Long epId;

    /**
     * 门店ID
     */
    private Long storeId;

    /**
     * 服务商编码
     */
    private String spCode;

    /**
     * 服务商订单号
     */
    private String spOrderId;

    /**
     * 地区
     */
    private Integer cityId;

    /**
     * 地区名字
     */
    private String cityName;

    /**
     * 预约类型 0实时订单 1预约订单
     */
    private Integer subscribeType;

    /**
     * 预约订单的取件时间
     */
    private LocalDateTime pickupTime;

    /**
     * 货物类型
     */
    private Integer goodsType;
    /**
     * 货物重量（KG）
     */
    private Integer goodsWeight;

    /**
     * 0寄件 1取件
     */
    private Integer sendType;

    /**
     * 预估里程（米）
     */
    private Integer estimateDistance;

    /**
     * 预估配送时长（分钟）
     */
    private Integer estimateDuration;

    /**
     * 订单状态 0:已查价 100:已下单 200:已支付 300:派单中 400:待取件 500:到店 600:已取件，配送中 700:配送完成 1000:订单已取消
     */
    private Integer status;

    /**
     * 订单取消原因分类ID
     */
    private Integer cancelTypeId;

    /**
     * 取消原因具体描述
     */
    private String cancelReason;

    /**
     * 备注
     */
    private String remark;

    /**
     * 创建时间
     */
    private LocalDateTime createTime;

    /**
     * 最后修改时间
     */
    private LocalDateTime updateTime;


    @Override
    protected Serializable pkVal() {
        return this.id;
    }

}
