package com.jida.management.mybatis.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import com.baomidou.mybatisplus.annotation.TableId;
import java.time.LocalDateTime;
import java.io.Serializable;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 后台管理系统账号
 * </p>
 *
 * @author JiDa
 * @since 2020-06-20
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
public class ErpUserInfo extends Model<ErpUserInfo> {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    /**
     * 昵称
     */
    private String name;

    /**
     * 电话
     */
    private String cellphone;

    /**
     * 密码
     */
    private String password;

    private String accessToken;
    private LocalDateTime expireTime;

    /**
     * 状态 0:无效  1:有效
     */
    private Integer status;

    private LocalDateTime createTime;

    private LocalDateTime updateTime;

    /**
     * 用户角色：1:管理员
     */
    private Integer role;


    @Override
    protected Serializable pkVal() {
        return this.id;
    }

}
