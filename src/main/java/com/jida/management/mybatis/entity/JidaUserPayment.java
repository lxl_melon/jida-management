package com.jida.management.mybatis.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import com.baomidou.mybatisplus.annotation.TableId;
import java.time.LocalDateTime;
import java.io.Serializable;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 用户支付记录
 * </p>
 *
 * @author JiDa
 * @since 2020-06-26
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
public class JidaUserPayment extends Model<JidaUserPayment> {

    private static final long serialVersionUID = 1L;

    /**
     * 主键
     */
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    /**
     * 用户ID
     */
    private Long userId;

    /**
     * 交易流水号
     */
    private String serialId;

    /**
     * 订单号
     */
    private Long orderId;

    /**
     * 支付渠道 1:微信支付 2:支付宝支付 3:余额扣减
     */
    private Integer payChannel;

    /**
     * 支付详情
     */
    private String extraData;

    /**
     * 0:提交 1:成功 2:失败
     */
    private Integer state;

    /**
     * 支付金额
     */
    private Integer totalFee;

    /**
     * 1:充值 2:订单支付 3:退款
     */
    private Integer type;

    /**
     * 创建时间
     */
    private LocalDateTime createTime;

    /**
     * 最后修改时间
     */
    private LocalDateTime updateTime;


    @Override
    protected Serializable pkVal() {
        return this.id;
    }

}
