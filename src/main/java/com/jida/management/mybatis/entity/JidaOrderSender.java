package com.jida.management.mybatis.entity;

import java.math.BigDecimal;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import java.time.LocalDateTime;
import java.io.Serializable;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 订单寄件人信息
 * </p>
 *
 * @author JiDa
 * @since 2020-06-25
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
public class JidaOrderSender extends Model<JidaOrderSender> {

    private static final long serialVersionUID = 1L;

    /**
     * 订单号
     */
    private Long orderId;

    /**
     * 寄件人姓名
     */
    private String name;

    /**
     * 寄件人手机号
     */
    private String cellphone;

    /**
     * 抵达纬度
     */
    private BigDecimal latitude;

    /**
     * 抵达精度
     */
    private BigDecimal longitude;

    /**
     * 定位类型（0：高德坐标系 1：百度坐标系）
     */
    private Integer locationType;

    /**
     * 城市ID
     */
    private Integer cityId;

    /**
     * 地区名字
     */
    private String cityName;

    /**
     * 地址信息
     */
    private String address;

    /**
     * 地址详细信息
     */
    private String addressDetail;

    /**
     * 创建时间
     */
    private LocalDateTime createTime;

    /**
     * 最后修改时间
     */
    private LocalDateTime updateTime;


    @Override
    protected Serializable pkVal() {
        return this.orderId;
    }

}
