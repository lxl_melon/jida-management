package com.jida.management.mybatis.service;

import com.jida.management.mybatis.entity.JidaOrderDeliver;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 订单配送员信息 服务类
 * </p>
 *
 * @author JiDa
 * @since 2020-06-26
 */
public interface JidaOrderDeliverService extends IService<JidaOrderDeliver> {

}
