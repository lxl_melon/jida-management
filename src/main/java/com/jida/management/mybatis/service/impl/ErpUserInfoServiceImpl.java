package com.jida.management.mybatis.service.impl;

import com.jida.management.mybatis.entity.ErpUserInfo;
import com.jida.management.mybatis.mapper.ErpUserInfoMapper;
import com.jida.management.mybatis.service.ErpUserInfoService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 后台管理系统账号 服务实现类
 * </p>
 *
 * @author JiDa
 * @since 2020-06-20
 */
@Service
public class ErpUserInfoServiceImpl extends ServiceImpl<ErpUserInfoMapper, ErpUserInfo> implements ErpUserInfoService {

}
