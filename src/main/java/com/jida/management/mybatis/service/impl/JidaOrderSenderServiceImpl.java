package com.jida.management.mybatis.service.impl;

import com.jida.management.mybatis.entity.JidaOrderSender;
import com.jida.management.mybatis.mapper.JidaOrderSenderMapper;
import com.jida.management.mybatis.service.JidaOrderSenderService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 订单寄件人信息 服务实现类
 * </p>
 *
 * @author JiDa
 * @since 2020-06-25
 */
@Service
public class JidaOrderSenderServiceImpl extends ServiceImpl<JidaOrderSenderMapper, JidaOrderSender> implements JidaOrderSenderService {

}
