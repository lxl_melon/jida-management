package com.jida.management.mybatis.service.impl;

import com.jida.management.mybatis.entity.JidaOrderTrack;
import com.jida.management.mybatis.mapper.JidaOrderTrackMapper;
import com.jida.management.mybatis.service.JidaOrderTrackService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 订单跟踪记录 服务实现类
 * </p>
 *
 * @author JiDa
 * @since 2020-06-25
 */
@Service
public class JidaOrderTrackServiceImpl extends ServiceImpl<JidaOrderTrackMapper, JidaOrderTrack> implements JidaOrderTrackService {

}
