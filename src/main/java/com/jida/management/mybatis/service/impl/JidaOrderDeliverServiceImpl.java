package com.jida.management.mybatis.service.impl;

import com.jida.management.mybatis.entity.JidaOrderDeliver;
import com.jida.management.mybatis.mapper.JidaOrderDeliverMapper;
import com.jida.management.mybatis.service.JidaOrderDeliverService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 订单配送员信息 服务实现类
 * </p>
 *
 * @author JiDa
 * @since 2020-06-26
 */
@Service
public class JidaOrderDeliverServiceImpl extends ServiceImpl<JidaOrderDeliverMapper, JidaOrderDeliver> implements JidaOrderDeliverService {

}
