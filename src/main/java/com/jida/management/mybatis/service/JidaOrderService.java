package com.jida.management.mybatis.service;

import com.jida.management.mybatis.entity.JidaOrder;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 订单信息表 服务类
 * </p>
 *
 * @author JiDa
 * @since 2020-06-13
 */
public interface JidaOrderService extends IService<JidaOrder> {

}
