package com.jida.management.mybatis.service.impl;

import com.jida.management.mybatis.entity.JidaOrder;
import com.jida.management.mybatis.mapper.JidaOrderMapper;
import com.jida.management.mybatis.service.JidaOrderService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 订单信息表 服务实现类
 * </p>
 *
 * @author JiDa
 * @since 2020-06-13
 */
@Service
public class JidaOrderServiceImpl extends ServiceImpl<JidaOrderMapper, JidaOrder> implements JidaOrderService {

}
