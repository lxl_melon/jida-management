package com.jida.management.mybatis.service;

import com.jida.management.mybatis.entity.JidaUserPayment;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 用户支付记录 服务类
 * </p>
 *
 * @author JiDa
 * @since 2020-06-26
 */
public interface JidaUserPaymentService extends IService<JidaUserPayment> {

}
