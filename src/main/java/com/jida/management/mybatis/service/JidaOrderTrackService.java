package com.jida.management.mybatis.service;

import com.jida.management.mybatis.entity.JidaOrderTrack;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 订单跟踪记录 服务类
 * </p>
 *
 * @author JiDa
 * @since 2020-06-25
 */
public interface JidaOrderTrackService extends IService<JidaOrderTrack> {

}
