package com.jida.management.mybatis.service;

import com.jida.management.mybatis.entity.JidaOrderReceiver;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 订单收件人信息 服务类
 * </p>
 *
 * @author JiDa
 * @since 2020-06-25
 */
public interface JidaOrderReceiverService extends IService<JidaOrderReceiver> {

}
