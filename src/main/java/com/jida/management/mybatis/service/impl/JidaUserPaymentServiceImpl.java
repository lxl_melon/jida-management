package com.jida.management.mybatis.service.impl;

import com.jida.management.mybatis.entity.JidaUserPayment;
import com.jida.management.mybatis.mapper.JidaUserPaymentMapper;
import com.jida.management.mybatis.service.JidaUserPaymentService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 用户支付记录 服务实现类
 * </p>
 *
 * @author JiDa
 * @since 2020-06-26
 */
@Service
public class JidaUserPaymentServiceImpl extends ServiceImpl<JidaUserPaymentMapper, JidaUserPayment> implements JidaUserPaymentService {

}
