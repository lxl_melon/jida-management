package com.jida.management.mybatis.service.impl;

import com.jida.management.mybatis.entity.JidaOrderReceiver;
import com.jida.management.mybatis.mapper.JidaOrderReceiverMapper;
import com.jida.management.mybatis.service.JidaOrderReceiverService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 订单收件人信息 服务实现类
 * </p>
 *
 * @author JiDa
 * @since 2020-06-25
 */
@Service
public class JidaOrderReceiverServiceImpl extends ServiceImpl<JidaOrderReceiverMapper, JidaOrderReceiver> implements JidaOrderReceiverService {

}
