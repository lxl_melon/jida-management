package com.jida.management.mybatis.service;

import com.jida.management.mybatis.entity.JidaOrderSender;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 订单寄件人信息 服务类
 * </p>
 *
 * @author JiDa
 * @since 2020-06-25
 */
public interface JidaOrderSenderService extends IService<JidaOrderSender> {

}
