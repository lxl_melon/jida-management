package com.jida.management.mybatis.service;

import com.jida.management.mybatis.entity.ErpUserInfo;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 后台管理系统账号 服务类
 * </p>
 *
 * @author JiDa
 * @since 2020-06-20
 */
public interface ErpUserInfoService extends IService<ErpUserInfo> {

}
