package com.jida.management.mybatis.mapper;

import com.jida.management.mybatis.entity.JidaOrderDeliver;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 订单配送员信息 Mapper 接口
 * </p>
 *
 * @author JiDa
 * @since 2020-06-26
 */
public interface JidaOrderDeliverMapper extends BaseMapper<JidaOrderDeliver> {

}
