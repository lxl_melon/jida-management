package com.jida.management.mybatis.mapper;

import com.jida.management.mybatis.entity.ErpUserInfo;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 后台管理系统账号 Mapper 接口
 * </p>
 *
 * @author JiDa
 * @since 2020-06-20
 */
public interface ErpUserInfoMapper extends BaseMapper<ErpUserInfo> {

}
