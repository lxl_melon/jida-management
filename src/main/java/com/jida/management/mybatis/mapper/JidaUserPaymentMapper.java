package com.jida.management.mybatis.mapper;

import com.jida.management.mybatis.entity.JidaUserPayment;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 用户支付记录 Mapper 接口
 * </p>
 *
 * @author JiDa
 * @since 2020-06-26
 */
public interface JidaUserPaymentMapper extends BaseMapper<JidaUserPayment> {

}
