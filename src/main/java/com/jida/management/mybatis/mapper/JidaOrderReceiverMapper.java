package com.jida.management.mybatis.mapper;

import com.jida.management.mybatis.entity.JidaOrderReceiver;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 订单收件人信息 Mapper 接口
 * </p>
 *
 * @author JiDa
 * @since 2020-06-25
 */
public interface JidaOrderReceiverMapper extends BaseMapper<JidaOrderReceiver> {

}
