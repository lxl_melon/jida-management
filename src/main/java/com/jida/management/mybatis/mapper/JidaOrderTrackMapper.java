package com.jida.management.mybatis.mapper;

import com.jida.management.mybatis.entity.JidaOrderTrack;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 订单跟踪记录 Mapper 接口
 * </p>
 *
 * @author JiDa
 * @since 2020-06-25
 */
public interface JidaOrderTrackMapper extends BaseMapper<JidaOrderTrack> {

}
