package com.jida.management.mybatis.mapper;

import com.jida.management.mybatis.entity.JidaOrder;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 订单信息表 Mapper 接口
 * </p>
 *
 * @author JiDa
 * @since 2020-06-13
 */
public interface JidaOrderMapper extends BaseMapper<JidaOrder> {

}
