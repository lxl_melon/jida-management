package com.jida.management.mybatis.mapper;

import com.jida.management.mybatis.entity.JidaOrderSender;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 订单寄件人信息 Mapper 接口
 * </p>
 *
 * @author JiDa
 * @since 2020-06-25
 */
public interface JidaOrderSenderMapper extends BaseMapper<JidaOrderSender> {

}
