package com.jida.management.config;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Builder
@Data
@AllArgsConstructor
@NoArgsConstructor
public class LogMessage {
    private String appName;
    private String className;
    private String methodName;

    private String message;

    @Override
    public String toString() {
        return appName + ' ' + className + ' ' +
                methodName + ' ' + message;
    }
}
