package com.jida.management.config;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.jida.management.mybatis.entity.ErpUserInfo;
import org.apache.commons.lang3.StringUtils;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.context.annotation.EnableAspectJAutoProxy;
import org.springframework.stereotype.Component;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import java.time.LocalDateTime;
import java.util.Date;
import java.util.Optional;

@Component
@Aspect
@EnableAspectJAutoProxy(proxyTargetClass = true, exposeProxy = true)
public class AccessAspect {

    public static final String MODULE = "jidaManagement";

    @Pointcut("execution(* com.jida.management.module..*(..)) && " +
            "@annotation(org.springframework.web.bind.annotation.RequestMapping) && "
            + "!execution(* com.jida.management.module.main.MainController.loginData(..))")
    public void init() {
    }

    @Around("init()")
    public Object around(ProceedingJoinPoint pjp) throws Throwable {
        String className = pjp.getTarget().getClass().getSimpleName();
        String methodName = pjp.getSignature().getName();
        HttpServletRequest request = ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getRequest();
        String ipToken = ":" + request.getHeader("X-Real-IP");
        Object[] args = pjp.getArgs();
        try {
            if (loginStatusCheck()) {
                Object result = pjp.proceed();
                String s = Optional.ofNullable(result).map(Object::toString).orElse("");
                return result;
            } else {
                return "main/login";
            }
        } catch (Exception e) {
            throw e;
        }
    }

    private boolean loginStatusCheck() {
        ServletRequestAttributes attributes = (ServletRequestAttributes) RequestContextHolder.getRequestAttributes();
        HttpServletRequest request = attributes.getRequest();
        Cookie[] cookies = request.getCookies();
        if (cookies != null) {
            for (Cookie cookie : cookies) {
                if (cookie.getName() != null && cookie.getName().equals("accessToken")) {
                    String accessToken = cookie.getValue();
                    if (StringUtils.isNotBlank(accessToken)) {
                        QueryWrapper queryWrapper = new QueryWrapper();
                        queryWrapper.eq("access_token", accessToken);
                        queryWrapper.ge("expire_time", new Date());
                        ErpUserInfo erpUserInfo = new ErpUserInfo().selectOne(queryWrapper);
                        if (erpUserInfo != null) {
                            erpUserInfo.setExpireTime(LocalDateTime.now().plusHours(2)).updateById();
                            return true;
                        }
                    }
                }
            }
        }
        return false;
    }



}