package com.jida.management.constants;


import lombok.Getter;

import java.util.Arrays;
import java.util.Map;
import java.util.stream.Collectors;

@Getter
public enum TimeZoneConstants {
    UTC0("UTC", "零时区"),
    UTC1("UTC+1", "东一区"),
    UTC2("UTC+2", "东二区"),
    UTC3("UTC+3", "东三区"),
    UTC4("UTC+4", "东四区"),
    UTC5("UTC+5", "东五区"),
    UTC6("UTC+6", "东六区"),
    UTC7("UTC+7", "东七区"),
    UTC8("UTC+8", "东八区"),
    UTC9("UTC+9", "东九区"),
    UTC10("UTC+10", "东十区"),
    UTC11("UTC+11", "东十一区"),
    UTC12("UTC+12", "东西十二区"),
    UTC21("UTC-1", "西一区"),
    UTC22("UTC-2", "西二区"),
    UTC23("UTC-3", "西三区"),
    UTC24("UTC-4", "西四区"),
    UTC25("UTC-5", "西五区"),
    UTC26("UTC-6", "西六区"),
    UTC27("UTC-7", "西七区"),
    UTC28("UTC-8", "西八区"),
    UTC29("UTC-9", "西九区"),
    UTC30("UTC-10", "西十区"),
    UTC31("UTC-11", "西十一区");

    public String code;
    public String name;

    TimeZoneConstants(String code, String name) {
        this.code = code;
        this.name = name;
    }

    public static Map<String, String> getMap() {
        return Arrays.stream(values()).collect(Collectors.toMap(TimeZoneConstants::getCode, TimeZoneConstants::getName, (o1, o2) -> o2));
    }
}
