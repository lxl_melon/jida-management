package com.jida.management.constants;

public enum ProjectNameEnum {

    TRAIN_TICKET("1", "火车票"),
    PLANE_TICKET("2", "机票"),
    HOTEL("3", "酒店"),
    BUS_TICKET("4", "汽车票"),
    BOAT_TICKET("5", "船票"),
    SCENIC_SPOT("6", "景点"),
    CHENGJI("7", "城际"),
    YILONG_PLANE_TICKET("8", "艺龙机票"),
    YILONG_TRAIN_TICKET("9", "艺龙火车票"),
    MARKET_TICKET("10", "市场中心"),
    HUIXING("11", "慧行");

    public String code;
    public String name;

    ProjectNameEnum(String code, String name) {
        this.code = code;
        this.name = name;
    }
}
