package com.jida.management.constants;

public enum RspType {
    C_200(200, "success"),
    C_403(403, "请求需要强制执行,请确认"),
    C_500(500, "系统未知错误"),

    CACHE_ERROR(1100, "缓存系统出错"),
    TOKEN_FORMAT_FAILED(1101, "登陆已过期,请重新登录"),
    TOKEN_AUTH_NULL(1102, "登陆已失效,请重新登录"),
    TOKEN_AUTH_FAILED(1103, "登陆失败,请重新登录"),
    REQUEST_TOO_BUSY(1104, "您的请求过于频繁"),
    VALIDATE_PARAM(1105, "参数有误"),


    DSF_ERROR(1301, "服务调用错误"),;

    public int code;
    public String message;

    RspType(int code, String message) {
        this.code = code;
        this.message = message;
    }
}
