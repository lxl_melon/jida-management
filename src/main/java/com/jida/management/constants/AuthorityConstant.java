package com.jida.management.constants;

public interface AuthorityConstant {

    interface Role {
        String SUPER = "super-admin";
        String MERCHANT_ADMIN = "merchant-admin";
    }

    /**
     * 权限模块
     * 对应权限code
     **/
    interface Permmision {
        String AUTHORITY_ADMIN = "authority-admin";
        String MERCHANT_ADMIN = "merchant-admin";
        String PLATFORM_ADMIN = "platform-admin";
        String RESOURCE_ADMIN = "resource-admin";
        String MARKET_ADMIN = "market-admin";
        String FINANCE_ADMIN = "finance-admin";
        String SETTLEMENT_ADMIN = "settlement-admin";
        String RISK_ADMIN = "risk-admin";
        String USER_ADMIN = "user-admin";
        String WORK_ADMIN = "work-admin";
    }
}