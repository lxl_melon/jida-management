package com.jida.management.exception;

import com.jida.management.constants.RspType;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = true)
public class BaseException extends RuntimeException {
    private int code;

    public BaseException() {
        this(400, "操作失败,请重试");
    }

    public BaseException(String msg) {
        this(400, msg);
    }

    public BaseException(String msg,Exception e) {
        this(400, msg);
        addSuppressed(e);
    }

    public BaseException(RspType type) {
        this(type.code, type.message);
    }

    public BaseException(int code, String msg) {
        super(msg);
        this.code = code;
    }
}
