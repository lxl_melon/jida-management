package com.jida.management.exception;

import com.jida.management.constants.RspType;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = true)
public class ValidateException extends BaseException {
    private int code;

    public ValidateException() {
        this(400, "操作失败,请重试");
    }

    public ValidateException(String msg) {
        this(400, msg);
    }

    public ValidateException(RspType type) {
        this(type.code, type.message);
    }

    public ValidateException(int code, String msg) {
        super(msg);
        this.code = code;
    }
}
